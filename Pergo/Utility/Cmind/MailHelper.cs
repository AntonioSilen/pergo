﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Utility.Cmind
{
    public class MailHelper
    {
        /// <summary>
        /// 寄信
        /// </summary>
        /// <param name="title">主旨</param>
        /// <param name="mailbody">內文</param>
        /// <param name="mailList">收信人</param>
        /// <param name="ImgPath">圖片路徑</param>
        /// <param name="filename">檔案路徑</param>
        /// <returns></returns>
        public static bool SendEmail(string title, string mailbody, List<string> mailList, string ImgPath, string filename, List<string> attachment = null)
        {
            try
            {
                string SenderMail = ConfigurationManager.AppSettings["SenderMail"];
                int Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                string Host = ConfigurationManager.AppSettings["Host"];
                string CredentialsAcc = ConfigurationManager.AppSettings["CredentialsAcc"];
                string CredentialsPwd = ConfigurationManager.AppSettings["CredentialsPwd"];

                //SenderMail = "wmaxtime@gmail.com";
                //Host = "smtp-relay.sendinblue.com";
                //CredentialsAcc = "wmaxtime@gmail.com";
                //CredentialsPwd = "LfXNbJcn8Zvahs4C";

                SenderMail = "info@pergo.com.tw";
                //SenderMail = "cmind@green.pumo.com.tw";
                Host = "green.pumo.com.tw";
                CredentialsAcc = "cmind@green.pumo.com.tw";
                CredentialsPwd = "Hmu7#QBtW!TaW";
                Port = 25;

                MailAddress From = new MailAddress(SenderMail, "Pergo 百力地板");//填寫發信電子郵件地址，和顯示名稱
                //設置好發送地址，和接收地址，接收地址可以是多個
                MailMessage mail = new MailMessage();
                mail.From = From;
                foreach (var item in mailList)
                {
                    mail.To.Add(item);
                }
                mail.Subject = title;

                System.Text.StringBuilder strBody = new System.Text.StringBuilder();
                strBody.Append(mailbody);

                mail.Body = strBody.ToString();
                mailBody(mail, title, mailbody, ImgPath, filename);
                mail.IsBodyHtml = true;//設置顯示htmls

                if (attachment != null && attachment.Count() > 0)
                {
                    foreach (var att in attachment)
                    {
                        if (File.Exists(att))
                        {
                            //附件
                            Attachment data = new Attachment(att, MediaTypeNames.Application.Octet);
                            // Add time stamp information for the file.
                            ContentDisposition disposition = data.ContentDisposition;
                            disposition.CreationDate = System.IO.File.GetCreationTime(att);
                            disposition.ModificationDate = System.IO.File.GetLastWriteTime(att);
                            disposition.ReadDate = System.IO.File.GetLastAccessTime(att);
                            // Add the file attachment to this email message.
                            mail.Attachments.Add(data);
                        }
                    }                  
                }

                //設置好發送郵件服務地址
                using (SmtpClient client = new SmtpClient(Host, Port))
                {
                    client.Credentials = new NetworkCredential(CredentialsAcc, CredentialsPwd);
                    //client.EnableSsl = true;
                    client.EnableSsl = false;
                    client.Send(mail);
                }
                //SmtpClient client = new SmtpClient();
                ////client.Host = "smtp.163.com";
                //client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                //client.Host = ConfigurationManager.AppSettings["Host"];
                ////client.Host = "login.live.com";                

                ////填寫服務器地址相關的用户名和密碼信息
                //client.Credentials = new System.Net.NetworkCredential("a842695137@gmail.com", "A1100102250");
                //發送郵件
                //client.Send(mail);
            }
            catch (Exception ex)
            {
                using (StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath("/App_Data/maillog.txt")))   //小寫TXT     
                {
                    // Add some text to the file.                  
                    sw.WriteLine("-------------------");
                    // Arbitrary objects can also be written to the file.
                    sw.Write("Date : ");
                    sw.WriteLine(DateTime.Now);
                    sw.WriteLine(ex.Message);
                }
                return false;
            }
            return true;
        }

        public static void mailBody(MailMessage mail, string title, string mailBody, string ImgPath, string filename)
        {
            try
            {
                string palinBody = title;
                AlternateView plainView = AlternateView.CreateAlternateViewFromString(
                         palinBody, null, "text/plain");

                string htmlBody = mailBody;

                // add the views
                mail.AlternateViews.Add(plainView);
                AlternateView htmlView =
                       AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");

                //Add Image
                if (!string.IsNullOrEmpty(ImgPath) && !string.IsNullOrEmpty(filename))
                {
                    int l = filename.IndexOf(".");
                    int typel = filename.Length - l;
                    string mediaType = "image/" + filename.Substring(l + 1, typel - 1);

                    //AlternateView htmlView =
                    //   AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");
                    imgResource(htmlView, "eventimg.jpg", mediaType, ImgPath);

                    //// add the views
                    //mail.AlternateViews.Add(htmlView);
                }
                // add the views
                mail.AlternateViews.Add(htmlView);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static void imgResource(AlternateView htmlView, string imgName, string imgType, string ImgPath)
        {
            try
            {
                // create image resource from image path using LinkedResource class..   
                LinkedResource imageResource = new LinkedResource(ImgPath, imgType);
                string[] imgArr = imgName.Split('.');
                imageResource.ContentId = imgArr[0];
                imageResource.TransferEncoding = TransferEncoding.Base64;
                htmlView.LinkedResources.Add(imageResource);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}