﻿using Pergo.Models;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.FAQ
{
    public class FAQView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "問題")]
        public string Question { get; set; }

        [Display(Name = "解答")]
        public string Answer { get; set; }

        [Display(Name = "地板類型")]
        public int BrandType { get; set; }

        [Display(Name = "問題類型")]
        public int QuestionType { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public List<SelectListItem> BrandTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Text = "全部",
                    Value = "0"
                });
                result.Add(new SelectListItem()
                {
                    Text = "超耐磨木地板",
                    Value = "1"
                });
                result.Add(new SelectListItem()
                {
                    Text = "防水地板",
                    Value = "2"
                });

                return result;
            }
        }

        FAQTypeRepository faqTypeRepository = new FAQTypeRepository(new PergoDBEntities());
        public List<SelectListItem> QuestionTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Text = "全部",
                    Value = "0",
                });
                var values = faqTypeRepository.GetAll().Where(s => s.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Name,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }

        //public List<SelectListItem> QuestionTypeOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        result.Add(new SelectListItem()
        //        {
        //            Text = "全部",
        //            Value = "0"
        //        });
        //        result.Add(new SelectListItem()
        //        {
        //            Text = "保養相關",
        //            Value = "1"
        //        });
        //        result.Add(new SelectListItem()
        //        {
        //            Text = "收邊相關",
        //            Value = "2"
        //        });

        //        return result;
        //    }
        //}
    }
}