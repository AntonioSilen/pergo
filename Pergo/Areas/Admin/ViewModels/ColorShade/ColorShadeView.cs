﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.ColorShade
{
    public class ColorShadeView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "分類名稱")]
        public string Name { get; set; }

        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}