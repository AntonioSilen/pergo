﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.ColorShade
{
    public class ColorShadeIndexView : PageQuery
    {
        public ColorShadeIndexView()
        {
            this.Sorting = "Name";
            this.IsDescending = false;
        }

        public PageResult<ColorShadeView> PageResult { get; set; }

        [Display(Name = "分類名稱")]
        public string Name { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }
    }
}