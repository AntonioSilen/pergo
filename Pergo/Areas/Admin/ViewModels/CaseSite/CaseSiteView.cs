﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.CaseSite
{
    public class CaseSiteView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Icon原色")]
        public string Icon { get; set; }

        [Required]
        [Display(Name = "Icon黃色")]
        public string IconHover { get; set; }

        [Required]
        [Display(Name = "分類名稱")]
        public string Name { get; set; }

        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}