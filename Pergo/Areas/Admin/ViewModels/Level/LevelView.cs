﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Level
{
    public class LevelView
    {
        public LevelView() { }
        public LevelView(int level)
        {
            this.Level = level;
        }

        public string SeriesStr { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "系列編號")]
        public int SeriesID { get; set; }

        [Required]
        [Display(Name = "等級")]
        public int Level { get; set; }

        [Display(Name = "長度")]
        public string Length { get; set; }

        [Display(Name = "厚度")]
        public string Thickness { get; set; }

        [Display(Name = "密度")]
        public string Density { get; set; }

        [Display(Name = "倒角")]
        public string Chamfer { get; set; }

        [Display(Name = "工藝技術")]
        public string Tech { get; set; }

        [Display(Name = "包裝")]
        public string Package { get; set; }

        [Display(Name = "基材標準")]
        public string Standard { get; set; }

        [Display(Name = "表層壓紋")]
        public string Texture { get; set; }

        [Display(Name = "亮澤")]
        public string Luster { get; set; }

        [Display(Name = "耐熱性")]
        public string HeatResistance { get; set; }

        [Display(Name = "鎖扣")]
        public string Lock { get; set; }

        [Display(Name = "環保認證")]
        public string Certification { get; set; }

        [Display(Name = "耐磨等級")]
        public string WearRating { get; set; }

        [Display(Name = "使用等級")]
        public string UseLevel { get; set; }

        [Display(Name = "產品特性")]
        public string Features { get; set; }

        [Display(Name = "產品花色")]
        public string Color { get; set; }

        [Required]
        [Display(Name = "保固與服務")]
        public string Guarantee { get; set; }

        [Display(Name = "規格顯示欄位")]
        public string SpecDisplay { get; set; }

        [Display(Name = "等級顯示欄位")]
        public string LevelDisplay { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public List<SelectListItem> ProductTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<LevelType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ChamferOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Chamfer>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> TechOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Tech>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> TextureOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Texture>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> LusterOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Luster>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> SpecDisplayOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Field>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> LevelDisplayOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Field>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}