﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Level
{
    public class LevelIndexView : PageQuery
    {
        public LevelIndexView()
        {
            this.Sorting = "SeriesID";
            this.IsDescending = false;
        }
        public string SeriesStr { get; set; }

        public int SeriesID { get; set; }

        public PageResult<LevelView> PageResult { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }        
    }
}