﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Case
{
    public class CaseView : CaseOptionsView
    {
        public CaseView()
        {
            this.ColorTypeStr = "";
            this.SiteTypeStr = "";
            this.StyleTypeStr = "";
        }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "花色編號")]
        public int ProductID { get; set; }
        public string ProductStr { get; set; }

        [Display(Name = "產品類別")]
        public int BrandType { get; set; }

        //public List<SelectListItem> BrandOptions { get; set; }

        [Display(Name = "產品系列")]
        public int SeriesType { get; set; }
        public string SeriesTypeStr { get; set; }

        //public List<SelectListItem> SeriesOptions { get; set; }

        [Display(Name = "顏色深淺")]//多值
        public string ColorType { get; set; }
        public string ColorTypeStr { get; set; }

        //public List<SelectListItem> ColorOptions { get; set; }

        [Display(Name = "案場類型")]//多值
        public string SiteType { get; set; }
        public string SiteTypeStr { get; set; }

        //public List<SelectListItem> SiteOptions { get; set; }

        [Display(Name = "風格樣式")]//多值
        public string StyleType { get; set; }
        public string StyleTypeStr { get; set; }

        //public List<SelectListItem> StyleOptions { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "地板類型")]
        public string Floor { get; set; }

        [Display(Name = "描述")]
        public string Description { get; set; }

        [Display(Name = "位置")]
        public string Location { get; set; }

        [Display(Name = "地區")]
        public string Area { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}