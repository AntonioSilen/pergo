﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Case
{
    public class CaseIndexView : PageQuery
    {
        public CaseIndexView()
        {
            CaseOptionsView caseOptionsView = new CaseOptionsView();
            this.Sorting = "Sort";
            this.IsDescending = false;
            this.BrandOptions = caseOptionsView.BrandOptions;
            this.SeriesOptions = caseOptionsView.SeriesOptions;
            this.ColorOptions = caseOptionsView.ColorOptions;
            this.SiteOptions = caseOptionsView.SiteOptions;
            this.StyleOptions = caseOptionsView.StyleOptions;
        }

        public PageResult<CaseView> PageResult { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "產品類別")]
        public int BrandType { get; set; }

        public List<SelectListItem> BrandOptions { get; set; }

        [Display(Name = "產品系列")]
        public int SeriesType { get; set; }

        public List<SelectListItem> SeriesOptions { get; set; }

        [Display(Name = "顏色深淺")]
        public string ColorType { get; set; }

        public List<SelectListItem> ColorOptions { get; set; }

        [Display(Name = "案場類型")]
        public string SiteType { get; set; }

        public List<SelectListItem> SiteOptions { get; set; }

        [Display(Name = "風格樣式")]
        public string StyleType { get; set; }

        public List<SelectListItem> StyleOptions { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }

        //public CaseOptionsView Options { get; set; }
    }
}