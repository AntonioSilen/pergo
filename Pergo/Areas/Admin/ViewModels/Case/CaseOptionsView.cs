﻿using Pergo.Models;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Case
{
    public class CaseOptionsView
    {
        private SeriesRepository seriesRepository;
        private ColorShadeRepository colorShadeRepository;
        private CaseSiteRepository caseSiteRepository;
        private StyleRepository styleRepository;

        public CaseOptionsView() : this(null, null, null, null) { }

        public CaseOptionsView(SeriesRepository repo, ColorShadeRepository repo2, CaseSiteRepository repo3, StyleRepository repo4)
        {
            seriesRepository = repo ?? new SeriesRepository(new PergoDBEntities());
            colorShadeRepository = repo2 ?? new ColorShadeRepository(new PergoDBEntities());
            caseSiteRepository = repo3 ?? new CaseSiteRepository(new PergoDBEntities());
            styleRepository = repo4 ?? new StyleRepository(new PergoDBEntities());
        }
        
        public List<SelectListItem> BrandOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Text = "Laminate",
                    Value = "1"
                });
                result.Add(new SelectListItem()
                {
                    Text = "Vinyl",
                    Value = "2"
                });
                return result;
            }
        }
        public List<SelectListItem> SeriesOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.SeriesName,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ColorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = colorShadeRepository.GetAll().Where(c => c.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Name,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> SiteOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = caseSiteRepository.GetAll().Where(s => s.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Name,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> StyleOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = styleRepository.GetAll().Where(s => s.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Name,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
    }
}