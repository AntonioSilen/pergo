﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.AccessoriesUsage
{
    public class AccessoriesUsageIndexView : PageQuery
    {
        public AccessoriesUsageIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<AccessoriesUsageView> PageResult { get; set; }

        [Display(Name = "配件項目")]
        public int AccessoriesID { get; set; }
        public string AccessoriesStr { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }
      
        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }
    }
}