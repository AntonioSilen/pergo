﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.Store
{
    public class StoreIndexView : PageQuery
    {
        public StoreIndexView()
        {
            this.Sorting = "StoreName";
            this.IsDescending = false;
        }

        public PageResult<StoreView> PageResult { get; set; }

        [Display(Name = "門市名稱")]
        public string StoreName { get; set; }

        [Display(Name = "門市地址")]
        public string Address { get; set; }

        [Display(Name = "門市照片")]
        public string StorePic { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }
    }
}