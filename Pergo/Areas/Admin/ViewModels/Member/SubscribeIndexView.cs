﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.Member
{
    public class SubscribeIndexView
    {
        public List<string> EmailList { get; set; }

        public string SearchValue { get; set; }
    }
}