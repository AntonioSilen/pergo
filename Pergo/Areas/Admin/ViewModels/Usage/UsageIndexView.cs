﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Usage
{
    public class UsageIndexView : PageQuery
    {
        public UsageIndexView()
        {
            this.Sorting = "SeriesID";
            this.IsDescending = false;
        }
        public string SeriesStr { get; set; }

        public int SeriesID { get; set; }

        public PageResult<UsageView> PageResult { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }

        [Display(Name = "案場類別")]
        public string SiteType { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        public List<SelectListItem> SiteOptions { get; set; }
    }
}