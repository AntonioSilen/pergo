﻿using Pergo.Models;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Usage
{
    public class UsageView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "系列")]
        public int SeriesID { get; set; }
        public string SeriesTypeStr { get; set; }

        [Required]
        [Display(Name = "花色")]
        public int ProductID { get; set; }
        public string ProductStr { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }
        
        [Display(Name = "圖片")]
        public string MainPic { get; set; }
        
        [Display(Name = "案場類別")]
        public string SiteType { get; set; }
        public string SiteTypeInt { get; set; }
        
        [Display(Name = "排序")]
        public int Sort { get; set; }
        
        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }
        
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }
        
        [Display(Name = "更新者")]
        public int Updater { get; set; }
        
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
        
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        SeriesRepository seriesRepository = new SeriesRepository(new PergoDBEntities());
        CaseSiteRepository caseSiteRepository = new CaseSiteRepository(new PergoDBEntities());

        public List<SelectListItem> SiteOptions {get; set;}

        public List<SelectListItem> ColorOptions { get; set; }
    }
}