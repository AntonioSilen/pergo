﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.CaseImage
{
    public class CaseImageView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "案例")]
        public int CaseID { get; set; }
        public string CaseStr { get; set; }

        [Display(Name = "圖片")]
        public string CarousePic { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}