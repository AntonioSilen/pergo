﻿using Pergo.Models;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.SeriesBanner
{
    public class BannerView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "系列")]
        public int SeriesID { get; set; }
        public string SeriesStr { get; set; }

        [Display(Name = "標題二")]
        public string Title { get; set; }

        [Display(Name = "標題一")]
        public string EngTitle { get; set; }

        [Display(Name = "副標題 (空白視為換行)")]
        public string SubTitle { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "手機圖片")]
        public string MobileMainPic { get; set; }

        [Display(Name = "連結文字")]
        public string LinkText { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Display(Name = "頁面")]
        public int Page { get; set; }

        [Display(Name = "類別")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "白字")]
        public bool TextChange { get; set; }

        [Required]
        [Display(Name = "左右留白")]
        public bool WhiteSpace { get; set; }

        [Display(Name = "文字位置")]
        public int Position { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "錨點連結")]
        public string Anchor { get; set; }

        public List<SelectListItem> SeriesOptions { get; set; }

        public List<SelectListItem> BannerPageOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<PageType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> BannerTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<SeriesBannerType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }


        public List<SelectListItem> PositionOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<BannerTextPosition>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}