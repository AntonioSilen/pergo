﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Accessories
{
    public class AccessoriesIndexView : PageQuery
    {
        public AccessoriesIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<AccessoriesView> PageResult { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "品牌材質")]
        public int Brand { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }

        [Display(Name = "Laminate標題")]
        public string LaminateTitle { get; set; }

        [Display(Name = "Laminate描述")]
        public string LaminateDesc { get; set; }

        [Display(Name = "Vinyl標題")]
        public string VinylTitle { get; set; }

        [Display(Name = "Vinyl描述")]
        public string VinylDesc { get; set; }

        public List<SelectListItem> BrandOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<BrandValue>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}