﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.SEO
{
    public class SEOView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "描述")]
        public string Description { get; set; }

        [Display(Name = "網址連結")]
        public string Url { get; set; }

        [Display(Name = "關鍵字")]
        public string Keywords { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "控制器")]
        public string Controller { get; set; }

        [Display(Name = "動作")]
        public string Action { get; set; }

        [Display(Name = "參數")]
        public string Parameter { get; set; }

        [Display(Name = "頁面")]
        public string SEOPage { get; set; }
    }
}