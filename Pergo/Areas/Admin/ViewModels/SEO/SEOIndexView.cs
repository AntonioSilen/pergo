﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.SEO
{
    public class SEOIndexView
    {
        public List<SEOView> SEOList { get; set; }
    }
}