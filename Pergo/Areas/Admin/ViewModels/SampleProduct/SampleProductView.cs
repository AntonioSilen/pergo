﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.SampleProduct
{
    public class SampleProductView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "樣冊樣盒申請編號")]
        public int SampleID { get; set; }

        [Required]
        [Display(Name = "系列編號")]
        public int SeriesID { get; set; }

        [Required]
        [Display(Name = "產品編號")]
        public int ProductID { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
    }
}