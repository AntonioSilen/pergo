﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.SampleProduct
{
    public class SampleProductIndexView : PageQuery
    {
        public SampleProductIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<SampleProductView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
    }
}