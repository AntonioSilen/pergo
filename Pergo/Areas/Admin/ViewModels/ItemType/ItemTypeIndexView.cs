﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.ItemType
{
    public class ItemTypeIndexView : PageQuery
    {
        public ItemTypeIndexView()
        {
            this.Sorting = "SeriesID";
            this.IsDescending = false;
        }

        public PageResult<ItemTypeView> PageResult { get; set; }

        [Display(Name = "項目")]
        public string Item { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }

        public List<SelectListItem> ItemTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<SeriesItem>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}