﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Documentation
{
    public class DocumentationIndexView : PageQuery
    {
        public DocumentationIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<DocumentationView> PageResult { get; set; }

        [Display(Name = "是否上線")]
        public bool? IsOnline { get; set; }

        [Display(Name = "標題名稱")]
        public string Title { get; set; }

        //[Display(Name = "類別")]
        //public int Type { get; set; }

        //public List<SelectListItem> DocumentationTypeOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        var values = EnumHelper.GetValues<DocumentationType>();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Text = EnumHelper.GetDescription(v),
        //                Value = ((int)v).ToString()
        //            });
        //        }
        //        return result;
        //    }
        //}
    }
}