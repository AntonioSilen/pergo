﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.FAQType
{
    public class FAQTypeView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "分類名稱")]
        public string Name { get; set; }

        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }
    }
}