﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.DocumentationMailLog
{
    public class DocumentationMailLogIndexView : PageQuery
    {
        public DocumentationMailLogIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<DocumentationMailLogView> PageResult { get; set; }

        public int DocID { get; set; }
        public DocumentationRepository documentationRepository = new DocumentationRepository(new PergoDBEntities());
        public string DocTitle
        {
            get
            {
                var docInfo = documentationRepository.GetById(this.DocID);
                if (docInfo != null)
                {
                    return docInfo.Title;
                }
                return "";
            }
        }

        [Display(Name = "文件標題")]
        public string SearchTitle { get; set; }

        [Display(Name = "信箱")]
        public string Email { get; set; }
    }

    public class DocumentationMailLogView
    {
        [Display(Name = "編號")]
        public string ID { get; set; }

        [Display(Name = "文件")]
        public int DocID { get; set; }
        public DocumentationRepository documentationRepository = new DocumentationRepository(new PergoDBEntities());
        public string DocTitle 
        {
            get
            {
                var docInfo = documentationRepository.GetById(this.DocID);
                if (docInfo != null)
                {
                    return docInfo.Title;
                }
                return "";
            }
        }

        [Display(Name = "信箱")]
        public string Email { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }
    }
}