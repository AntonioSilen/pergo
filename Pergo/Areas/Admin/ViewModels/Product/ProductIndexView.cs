﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.Product
{
    public class ProductIndexView : PageQuery
    {
        public ProductIndexView()
        {
            this.Sorting = "Sort";
            this.IsDescending = false;
        }

        public PageResult<ProductView> PageResult { get; set; }

        public string SeriesStr { get; set; }

        //Name
        [Display(Name = "產品名稱")]
        public string SearchName { get; set; }

        [Display(Name = "系列")]
        public int SeriesID { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }
    }
}