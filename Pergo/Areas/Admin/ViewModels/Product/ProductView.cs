﻿using Pergo.Areas.Admin.ViewModels.Level;
using Pergo.Models;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Product
{
    public class ProductView
    {
        public int brand { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "系列")]
        public int SeriesID { get; set; }

        [Required]
        [Display(Name = "紋理")]
        public int Texture { get; set; }

        [Required]
        [Display(Name = "亮澤")]
        public int Luster { get; set; }

        [Display(Name = "色系")]
        public string Color { get; set; }

        [Display(Name = "風格")]
        public string Shade { get; set; }

        [Required]
        [Display(Name = "產品名稱")]
        public string Name { get; set; }

        [Display(Name = "產品英文名稱")]
        public string EngName { get; set; }

        [Required]
        [Display(Name = "型號")]
        public string Model { get; set; }

        [Display(Name = "型號2")]
        public string ModelTwo { get; set; }

        [Display(Name = "型號3")]
        public string ModelThree { get; set; }

        [Required]
        [Display(Name = "概念")]
        public string Concept { get; set; }

        [Required]
        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "點閱數")]
        public int Views { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public List<SelectListItem> SeriesOptions { get; set; }

        public List<SelectListItem> TextureOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ColorTexture>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> LusterOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Luster>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ColorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ColorColor>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ShadeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Shade>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}