﻿using Pergo.Models;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Reservation
{
    public class ReservationView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "分類")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Display(Name = "地址")]
        public string Address { get; set; }

        [Display(Name = "門市")]
        public string Store { get; set; }
        public string StoreName { get; set; }

        [Required]
        [Display(Name = "信箱")]
        public string Email { get; set; }

        [Display(Name = "詢問內容")]
        public string InquiryContent { get; set; }

        [Required]
        [Display(Name = "回覆狀態")]
        public bool IsReply { get; set; }

        [Display(Name = "預約時間")]
        public DateTime ReservationTime { get; set; }
        public string ReservationTimeStr { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        public List<SelectListItem> ReservationTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ReservationType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}