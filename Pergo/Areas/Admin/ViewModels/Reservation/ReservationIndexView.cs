﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.Reservation
{
    public class ReservationIndexView : PageQuery
    {
        public ReservationIndexView()
        {
            this.Sorting = "ReservationTime";
            this.IsDescending = false;
        }

        public PageResult<ReservationView> PageResult { get; set; }

        //Name, Phone, Email
        [Display(Name = "姓名/電話/信箱")]
        public string SearchValue { get; set; }

        [Display(Name = "預約時間")]
        public DateTime ReservationTime { get; set; }

        [Display(Name = "回覆狀態")]
        public bool? IsReply { get; set; }
    }
}