﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.ProductImage
{
    public class ProductImageView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "產品編號")]
        public int ProductID { get; set; }
        public string ProductStr { get; set; }

        [Required]
        [Display(Name = "圖片")]
        public string CarousePic { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}