﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.ProductImage
{
    public class ProductImageIndexView/* : PageQuery*/
    {
        //public ProductImageIndexView()
        //{
        //    this.Sorting = "Sort";
        //    this.IsDescending = false;
        //}

        public int ProductID { get; set; }
        public string ProductStr { get; set; }

        //public PageResult<ProductImageView> PageResult { get; set; }
        public List<ProductImageView> ImageList { get; set; }

        [Display(Name = "圖片")]
        public string CarousePictures { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }
    }
}