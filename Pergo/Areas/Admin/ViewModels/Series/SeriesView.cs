﻿using Pergo.Areas.Admin.ViewModels.Level;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Series
{
    public class SeriesView
    {
        //public SeriesView()
        //{
        //    LivingLevel = new LevelView(1);
        //    OriginalLevel = new LevelView(2);
        //    PublicLevel = new LevelView(3);
        //}

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "系列名稱")]
        public string SeriesName { get; set; }

        [Required]
        [Display(Name = "系列英文名稱")]
        public string SeriesEngName { get; set; }

        [Display(Name = "系列標題")]
        public string SeriesTitle { get; set; }

        [Required]
        [Display(Name = "品牌")]
        public int Brand { get; set; }

        [Required]
        [Display(Name = "Icon原色")]
        public string Icon { get; set; }

        [Required]
        [Display(Name = "Icon黃色")]
        public string IconHover { get; set; }

        [Display(Name = "樣圖")]
        public string SamplePic { get; set; }

        [Display(Name = "案例圖")]
        public string CasePic { get; set; }

        [Display(Name = "應用案場")]
        public string UsageSite { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public List<SelectListItem> BrandOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<BrandValue>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        CaseSiteRepository caseSiteRepository = new CaseSiteRepository(new PergoDBEntities());
        public List<SelectListItem> SiteOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = caseSiteRepository.GetAll().Where(s => s.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Name,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
    }
}