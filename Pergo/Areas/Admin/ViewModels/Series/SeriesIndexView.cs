﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.Series
{
    public class SeriesIndexView : PageQuery
    {
        public SeriesIndexView()
        {
            this.Sorting = "SeriesName";
            this.IsDescending = false;
        }

        public PageResult<SeriesView> PageResult { get; set; }

        [Display(Name = "系列名稱")]
        public string SeriesName { get; set; }

        [Display(Name = "品牌")]
        public int Brand { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }

        public List<SelectListItem> BrandOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<BrandValue>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
        
    }
}