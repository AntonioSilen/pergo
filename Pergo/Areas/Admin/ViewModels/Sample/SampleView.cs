﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.Sample
{
    public class SampleView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Display(Name = "地址")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "郵件")]
        public string Email { get; set; }

        [Display(Name = "公司")]
        public string Company { get; set; }

        [Display(Name = "統一編號")]
        public string GUI { get; set; }

        [Required]
        [Display(Name = "回覆狀態")]
        public bool IsReply { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
    }
}