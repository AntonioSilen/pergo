﻿using Pergo.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.Areas.Admin.ViewModels.Sample
{
    public class SampleIndexView : PageQuery
    {
        public SampleIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = false;
        }

        public PageResult<SampleView> PageResult { get; set; }

        //Name, Phone, Email
        [Display(Name = "姓名/電話/信箱")]
        public string SearchValue { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "回覆狀態")]
        public bool? IsReply { get; set; }
    }
}