﻿using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.ViewModels.FixedBanner
{
    public class BannerIndexView : PageQuery
    {
        public BannerIndexView()
        {
            this.Sorting = "Sort";
            this.IsDescending = false;
        }

        public PageResult<BannerView> PageResult { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "類別")]
        public int Type { get; set; }

        [Display(Name = "上線狀態")]
        public bool? IsOnline { get; set; }

        public List<SelectListItem> BannerTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<BannerType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}