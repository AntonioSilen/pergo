﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Accessories;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class AccessoriesAdminController : BaseAdminController
    {
        private AccessoriesRepository accessoriesRepository;
        private AccessoriesUsageRepository accessoriesUsageRepository;

        public AccessoriesAdminController() : this(null, null) { }

        public AccessoriesAdminController(AccessoriesRepository repo, AccessoriesUsageRepository repo2)
        {
            accessoriesRepository = repo ?? new AccessoriesRepository(new PergoDBEntities());
            accessoriesUsageRepository = repo2 ?? new AccessoriesUsageRepository(new PergoDBEntities());
        }

        // GET: Admin/AccessoriesAdmin
        public ActionResult Index(AccessoriesIndexView model)
        {
            try
            { 
                using (StreamReader sr = new StreamReader(Server.MapPath("/Models/AccessoryTitle.txt"))) 
                {
                    String line;
                    int l = 1;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (l == 1)
                        {
                            model.LaminateTitle = line;
                        }
                        else if (l == 2)
                        {
                            model.LaminateDesc = line;
                        }
                        else if (l == 3)
                        {
                            model.VinylTitle = line;
                        }
                        else if (l == 4)
                        {
                            model.VinylDesc = line;
                            l = 0;
                        }
                        l++;
                    }
                }
            }
            catch (Exception e)
            {
                ShowMessage(false, "儲存失敗");
            }
            var query = accessoriesRepository.Query(model.IsOnline, model.Brand, model.Title);
            var pageResult = query.ToPageResult<Accessories>(model);
            model.PageResult = Mapper.Map<PageResult<AccessoriesView>>(pageResult);
            return View(model);
        }

        public ActionResult AccessoriesTitle(AccessoriesIndexView model)
        {
            using (StreamWriter sw = new StreamWriter(Server.MapPath("/Models/AccessoryTitle.txt")))
            {
                sw.WriteLine(model.LaminateTitle.Replace("\r\n", ""));
                sw.WriteLine(model.LaminateDesc.Replace("\r\n", ""));
                sw.WriteLine(model.VinylTitle.Replace("\r\n", ""));
                sw.WriteLine(model.VinylDesc.Replace("\r\n", ""));
            }
            ShowMessage(true, "儲存成功");

            return RedirectToAction(nameof(AccessoriesAdminController.Index), new { Title = model.Title, Brand = model.Brand, IsOnline = model.IsOnline});
        }

        public ActionResult Edit(int id = 0)
        {
            AccessoriesView model;
            if (id == 0)
            {
                model = new AccessoriesView();
            }
            else
            {
                var query = accessoriesRepository.GetById(id);
                model = Mapper.Map<AccessoriesView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AccessoriesView accessoriesmodel, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);       

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, accessoriesmodel.MainPic);
                    accessoriesmodel.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }               

                Accessories accessories = Mapper.Map<Accessories>(accessoriesmodel);
                accessories.UpdateDate = DateTime.Now;
                accessories.Updater = 1;
                if (accessoriesmodel.ID == 0)
                {
                    accessories.CreateDate = DateTime.Now;
                    accessories.Creater = 1;
                    accessoriesmodel.ID = accessoriesRepository.Insert(accessories);                    
                }
                else
                {
                    accessoriesRepository.Update(accessories);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(AccessoriesAdminController.Edit), new { id = accessoriesmodel.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(accessoriesmodel);
        }

        public ActionResult Delete(int id)
        {
            Accessories store = accessoriesRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, store.MainPic);
            accessoriesRepository.Delete(store.ID);
            var usagelist = accessoriesRepository.Query(null, id, "");
            foreach (var item in usagelist)
            {
                AccessoriesUsageAdminController accessoriesUsageAdminController = new AccessoriesUsageAdminController();
                accessoriesUsageAdminController.Delete(item.ID);
            }
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(AccessoriesAdminController.Index));
        }
    }
}