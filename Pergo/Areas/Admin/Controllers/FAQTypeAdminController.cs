﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.FAQType;
using Pergo.Models;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class FAQTypeAdminController : BaseAdminController
    {
        private FAQTypeRepository faqTypeRepository;

        public FAQTypeAdminController() : this(null) { }

        public FAQTypeAdminController(FAQTypeRepository repo)
        {
            faqTypeRepository = repo ?? new FAQTypeRepository(new PergoDBEntities());
        }

        // GET: Admin/FAQTypeAdmin
        public ActionResult Index(FAQTypeIndexView model)
        {
            var query = faqTypeRepository.Query(model.IsOnline);
            model.TypeList = Mapper.Map<List<FAQTypeView>>(query);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            FAQTypeView model;
            if (id == 0)
            {
                model = new FAQTypeView();
            }
            else
            {
                var query = faqTypeRepository.GetById(id);
                model = Mapper.Map<FAQTypeView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(FAQTypeView model)
        {
            if (ModelState.IsValid)
            {
                FAQType style = Mapper.Map<FAQType>(model);
                if (model.ID == 0)
                {
                    model.ID = faqTypeRepository.Insert(style);
                }
                else
                {
                    faqTypeRepository.Update(style);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(FAQTypeAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            FAQType style = faqTypeRepository.GetById(id);
            faqTypeRepository.Delete(style.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(FAQTypeAdminController.Index));
        }
    }
}