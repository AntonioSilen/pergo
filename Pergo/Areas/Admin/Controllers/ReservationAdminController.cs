﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Reservation;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ReservationAdminController : BaseAdminController
    {
        private ReservationRepository reservationRepository;
        private StoreRepository storeRepository;

        public ReservationAdminController() : this(null, null) { }

        public ReservationAdminController(ReservationRepository repo, StoreRepository repo2)
        {
            reservationRepository = repo ?? new ReservationRepository(new PergoDBEntities());
            storeRepository = repo2 ?? new StoreRepository(new PergoDBEntities());
        }

        // GET: Admin/ReservationAdmin
        public ActionResult Index(ReservationIndexView model)
        {
            var query = reservationRepository.Query(model.IsReply).OrderByDescending(r => r.ReservationTime);
            var pageResult = query.ToPageResult<Reservation>(model);
            model.PageResult = Mapper.Map<PageResult<ReservationView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            ReservationView model;
            if (id == 0)
            {
                model = new ReservationView();
                model.ReservationTime = DateTime.Now;
                model.ReservationTimeStr = model.ReservationTime.ToString("yyyy/MM/dd HH:mm");
            }
            else
            {
                var query = reservationRepository.GetById(id);
                model = Mapper.Map<ReservationView>(query);
                model.ReservationTimeStr = model.ReservationTime.ToString("yyyy/MM/dd HH:mm");
                model.StoreName = storeRepository.GetById(Convert.ToInt32(model.Store)).StoreName;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ReservationView model)
        {
            model.ReservationTime = Convert.ToDateTime(model.ReservationTimeStr);
            if (ModelState.IsValid)
            {   
                Reservation reservation = Mapper.Map<Reservation>(model);
                if (model.ID == 0)
                {
                    reservation.CreateDate = DateTime.Now;
                    model.ID = reservationRepository.Insert(reservation);
                }
                else
                {
                    reservationRepository.Update(reservation);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ReservationAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Reservation reservation = reservationRepository.GetById(id);
            reservationRepository.Delete(reservation.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(ReservationAdminController.Index));
        }
    }
}