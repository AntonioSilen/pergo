﻿using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Member;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class MemberAdminController : BaseAdminController
    {
        // GET: Admin/MemberAdmin
        public ActionResult Subscribe(SubscribeIndexView model)
        {
            List<string> mailList = new List<string>();

            // 建立檔案串流（@ 可取消跳脫字元 escape sequence）
            StreamReader sr = new StreamReader(Server.MapPath(@"\App_Data\subscribeemail.txt"));
            while (!sr.EndOfStream)
            {               // 每次讀取一行，直到檔尾
                string line = sr.ReadLine();            // 讀取文字到 line 變數
                mailList.Add(line);
            }
            sr.Close();						// 關閉串流

            if (!string.IsNullOrEmpty(model.SearchValue))
            {
                mailList = mailList.Where(m => m.Contains(model.SearchValue)).ToList();
            }
            model.EmailList = mailList;

            return View(model);
        }

        /// <summary>
        /// 刪除管理者
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string DelManager(string email)
        {        
            List<string> newList = new List<string>();

            StreamReader sr = new StreamReader(Server.MapPath(@"\App_Data\subscribeemail.txt"));
            while (!sr.EndOfStream)
            {               // 每次讀取一行，直到檔尾
                string line = sr.ReadLine();            // 讀取文字到 line 變數

                if (line != email)
                {
                    newList.Add(line);
                }
            }
            sr.Close();                     // 關閉串流

            try
            {
                using (StreamWriter outputFile = new StreamWriter(Path.Combine(Server.MapPath(@"\App_Data\"), "subscribeemail.txt"), false))
                {
                    foreach (var item in newList)
                    {
                        outputFile.WriteLine(item);
                    }
                    outputFile.Close();
                    return $" {email} 已取消訂閱 !";
                }
            }
            catch (Exception ex)
            {
                return $"操作失敗，請再試一次。 Message : {ex.Message}";
            }
        }
    }
}