﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Level;
using Pergo.Areas.Admin.ViewModels.Product;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProductAdminController : BaseAdminController
    {
        private ProductRepository productRepository;
        //private LevelRepository levelRepository;
        private SeriesRepository seriesRepository;

        public ProductAdminController() : this(null, null/*, null*/) { }

        public ProductAdminController(ProductRepository repo, SeriesRepository repo2/*, LevelRepository repo3*/)
        {
            productRepository = repo ?? new ProductRepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
            //levelRepository = repo3 ?? new LevelRepository(new PergoDBEntities());
        }

        // GET: Admin/ProductAdmin
        public ActionResult Index(ProductIndexView model, int sid = 0)
        {
            var query = productRepository.Query(model.IsOnline);
            if (sid != 0)
            {
                model.SeriesID = sid;
            }
            if (model.SeriesID != 0)
            {
                model.SeriesStr = seriesRepository.GetById(model.SeriesID).SeriesName;
                query = query.Where(p => p.SeriesID == model.SeriesID);
            }
            var pageResult = query.ToPageResult<Product>(model);
            model.PageResult = Mapper.Map<PageResult<ProductView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0, int sid = 0)
        {
            ProductView model;
            if (id == 0)
            {
                model = new ProductView();
            }
            else
            {
                var query = productRepository.GetById(id);
                model = Mapper.Map<ProductView>(query);
                //model.LivingLevel = Mapper.Map<LevelView>(levelRepository.GetAll().Where(l => l.ProductID == id && l.Level == 1).FirstOrDefault());
                //model.OriginalLevel = Mapper.Map<LevelView>(levelRepository.GetAll().Where(l => l.ProductID == id && l.Level == 2).FirstOrDefault());
            }

            var seriesData = seriesRepository.GetById(sid);            
            model.brand = seriesData.Brand;
            model.SeriesID = sid;

            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            model.SeriesOptions = result;

            string cdata = "";
            if (!string.IsNullOrEmpty(model.Color))
            {
                var colorList = model.Color.Split(',');
                int cc = 1;
                foreach (var item in colorList)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        cdata += EnumHelper.GetDescription((ColorColor)Convert.ToInt32(item));
                        if (cc < colorList.Count())
                        {
                            cdata += ",";
                        }
                    }
                    cc++;
                }
            }           
            model.Color = cdata;

            string sdata = "";
            if (!string.IsNullOrEmpty(model.Shade))
            {
                var shadeList = model.Shade.Split(',');
                int sc = 1;
                foreach (var item in shadeList)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        sdata += EnumHelper.GetDescription((Shade)Convert.ToInt32(item));
                        if (sc < shadeList.Count())
                        {
                            sdata += ",";
                        }
                    }
                    sc++;
                }
            }
            model.Shade = sdata;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ProductView productmodel, HttpPostedFileBase pic_file)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            productmodel.SeriesOptions = result;

            bool hasFile = ImageHelper.CheckFileExists(pic_file);
            //bool hasFile_LC = ImageHelper.CheckFileExists(LivingLevel_Certification);
            //bool hasFile_LF = ImageHelper.CheckFileExists(LivingLevel_Features);
            //bool hasFile_OC = ImageHelper.CheckFileExists(OriginalLevel_Certification);
            //bool hasFile_OF = ImageHelper.CheckFileExists(OriginalLevel__Features);
            bool imageValid = !(productmodel.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(productmodel.MainPic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, productmodel.MainPic);
                    productmodel.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                //if (hasFile_LC)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, productmodel.LivingLevel.Certification);
                //    productmodel.LivingLevel.Certification = ImageHelper.SaveFile(LivingLevel_Certification, PhotoFolder);
                //}
                //if (hasFile_LF)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, productmodel.LivingLevel.Features);
                //    productmodel.LivingLevel.Features = ImageHelper.SaveFile(LivingLevel_Features, PhotoFolder);
                //}
                //if (hasFile_OC)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, productmodel.OriginalLevel.Certification);
                //    productmodel.OriginalLevel.Certification = ImageHelper.SaveFile(OriginalLevel_Certification, PhotoFolder);
                //}
                //if (hasFile_OF)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, productmodel.OriginalLevel.Features);
                //    productmodel.OriginalLevel.Features = ImageHelper.SaveFile(OriginalLevel__Features, PhotoFolder);
                //}

                //var livingLevel = Mapper.Map<ProductLevel>(productmodel.LivingLevel);
                //var originalLevel = Mapper.Map<ProductLevel>(productmodel.OriginalLevel);

                Product product = Mapper.Map<Product>(productmodel);
                product.UpdateDate = DateTime.Now;
                product.Updater = 1;

                string cdata = "";
                if (!string.IsNullOrEmpty(product.Shade))
                {
                    var colorList = product.Color.Split(',');
                    int cc = 1;
                    foreach (var item in colorList)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            cdata += (int)EnumHelper.GetValueFromDescription<ColorColor>(item);
                            if (cc < colorList.Count())
                            {
                                cdata += ",";
                            }
                        }
                        cc++;
                    }
                }
                product.Color = cdata;

                string sdata = "";
                if (!string.IsNullOrEmpty(product.Shade))
                {
                    var shadeList = product.Shade.Split(',');
                    int sc = 1;
                    foreach (var item in shadeList)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            sdata += (int)EnumHelper.GetValueFromDescription<Shade>(item);
                            if (sc < shadeList.Count())
                            {
                                sdata += ",";
                            }
                        }
                        sc++;
                    }
                }
                product.Shade = sdata;
                //livingLevel.UpdateDate = product.UpdateDate;
                //livingLevel.Updater = product.Updater;
                //originalLevel.UpdateDate = product.UpdateDate;
                //originalLevel.Updater = product.Updater;
                if (productmodel.ID == 0)
                {
                    //Product data
                    product.CreateDate = DateTime.Now;
                    product.Creater = 1;
                    productmodel.ID = productRepository.Insert(product);
                    ////Living level data
                    //livingLevel.CreateDate = product.CreateDate;
                    //livingLevel.Creater = product.Creater;
                    //livingLevel.ProductID = productmodel.ID;
                    //levelRepository.Insert(livingLevel);
                    ////Original level data
                    //originalLevel.CreateDate = product.CreateDate;
                    //originalLevel.Creater = product.Creater;
                    //originalLevel.ProductID = productmodel.ID;
                    //levelRepository.Insert(originalLevel);
                }
                else
                {
                    productRepository.Update(product);
                    //levelRepository.Update(livingLevel);
                    //levelRepository.Update(originalLevel);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ProductAdminController.Edit), new { id = productmodel.ID, sid = productmodel.SeriesID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(productmodel);
        }

        public ActionResult Delete(int id)
        {
            Product product = productRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, product.MainPic);
            productRepository.Delete(product.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(ProductAdminController.Index));
        }
    }
}