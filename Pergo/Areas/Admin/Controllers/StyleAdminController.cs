﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Style;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class StyleAdminController : BaseAdminController
    {
        private StyleRepository styleRepository;

        public StyleAdminController() : this(null) { }

        public StyleAdminController(StyleRepository repo)
        {
            styleRepository = repo ?? new StyleRepository(new PergoDBEntities());
        }

        // GET: Admin/StyleAdmin
        public ActionResult Index(StyleIndexView model)
        {
            var query = styleRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<Style>(model);
            model.PageResult = Mapper.Map<PageResult<StyleView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            StyleView model;
            if (id == 0)
            {
                model = new StyleView();
            }
            else
            {
                var query = styleRepository.GetById(id);
                model = Mapper.Map<StyleView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(StyleView model)
        {
            if (ModelState.IsValid)
            {
                Style style = Mapper.Map<Style>(model);
                style.UpdateDate = DateTime.Now;
                style.Updater = 1;
                if (model.ID == 0)
                {
                    style.CreateDate = DateTime.Now;
                    style.Creater = 1;
                    model.ID = styleRepository.Insert(style);
                }
                else
                {
                    styleRepository.Update(style);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(StyleAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Style style = styleRepository.GetById(id);
            styleRepository.Delete(style.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(StyleAdminController.Index));
        }
    }
}