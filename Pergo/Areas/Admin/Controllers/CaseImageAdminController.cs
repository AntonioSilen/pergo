﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.CaseImage;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class CaseImageAdminController : BaseAdminController
    {
        private CaseImageRepository caseImageRepository;

        public CaseImageAdminController() : this(null) { }

        public CaseImageAdminController(CaseImageRepository repo)
        {
            caseImageRepository = repo ?? new CaseImageRepository(new PergoDBEntities());
        }

        // GET: Admin/CaseImageAdmin
        public ActionResult Index(CaseImageIndexView model)
        {
            var query = caseImageRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<CaseImage>(model);
            model.PageResult = Mapper.Map<PageResult<CaseImageView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            CaseImageView model;
            if (id == 0)
            {
                model = new CaseImageView();
            }
            else
            {
                var query = caseImageRepository.GetById(id);
                model = Mapper.Map<CaseImageView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(CaseImageView model, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);
            bool imageValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.CarousePic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.CarousePic);
                    model.CarousePic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                CaseImage caseImage = Mapper.Map<CaseImage>(model);
                caseImage.UpdateDate = DateTime.Now;
                caseImage.Updater = 1;
                if (model.ID == 0)
                {
                    caseImage.CreateDate = DateTime.Now;
                    caseImage.Creater = 1;
                    model.ID = caseImageRepository.Insert(caseImage);
                }
                else
                {
                    caseImageRepository.Update(caseImage);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(CaseImageAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CaseImage caseImage = caseImageRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, caseImage.CarousePic);
            caseImageRepository.Delete(caseImage.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(CaseImageAdminController.Index));
        }
    }
}