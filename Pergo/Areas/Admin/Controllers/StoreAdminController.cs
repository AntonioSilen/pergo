﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Store;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class StoreAdminController : BaseAdminController
    {
        private StoreRepository storeRepository;

        public StoreAdminController() : this(null) { }

        public StoreAdminController(StoreRepository repo)
        {
            storeRepository = repo ?? new StoreRepository(new PergoDBEntities());
        }

        // GET: Admin/StoreAdmin
        public ActionResult Index(StoreIndexView model)
        {
            var query = storeRepository.Query(model.IsOnline);
            if (!string.IsNullOrEmpty(model.StoreName))
            {
                query = query.Where(s => s.StoreName.Contains(model.StoreName));
            }
            var pageResult = query.OrderByDescending(s => s.UpdateDate).ToPageResult<Store>(model);
            model.PageResult = Mapper.Map<PageResult<StoreView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            StoreView model;
            if (id == 0)
            {
                model = new StoreView();
            }
            else
            {
                var query = storeRepository.GetById(id);
                model = Mapper.Map<StoreView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(StoreView model, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);
            bool imageValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.StorePic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.StorePic);
                    model.StorePic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                Store store = Mapper.Map<Store>(model);
                store.UpdateDate = DateTime.Now;
                store.Updater = 1;
                if (model.ID == 0)
                {
                    store.CreateDate = DateTime.Now;
                    store.Creater = 1;
                    model.ID = storeRepository.Insert(store);
                }
                else
                {
                    storeRepository.Update(store);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(StoreAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Store store = storeRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, store.StorePic);
            storeRepository.Delete(store.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(StoreAdminController.Index));
        }
    }
}