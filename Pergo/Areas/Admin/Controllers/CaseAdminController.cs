﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Case;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class CaseAdminController : BaseAdminController
    {
        private CaseRepository caseRepository;
        private ColorShadeRepository colorShadeRepository;
        private CaseSiteRepository caseSiteRepository;
        private StyleRepository styleRepository;
        private SeriesRepository seriesRepository;

        public CaseAdminController() : this(null, null, null, null, null) { }

        public CaseAdminController(CaseRepository repo, ColorShadeRepository repo2, CaseSiteRepository repo3, StyleRepository repo4, SeriesRepository repo5)
        {
            caseRepository = repo ?? new CaseRepository(new PergoDBEntities());
            colorShadeRepository = repo2 ?? new ColorShadeRepository(new PergoDBEntities());
            caseSiteRepository = repo3 ?? new CaseSiteRepository(new PergoDBEntities());
            styleRepository = repo4 ?? new StyleRepository(new PergoDBEntities());
            seriesRepository = repo5 ?? new SeriesRepository(new PergoDBEntities());
        }

        // GET: Admin/CaseAdmin
        public ActionResult Index(CaseIndexView model)
        {
            var query = caseRepository.Query(model.IsOnline);
            #region 搜尋條件            
            if (model.BrandType != 0)
                query = query.Where(c => c.BrandType == model.BrandType);
            if (model.SeriesType != 0)
                query = query.Where(c => c.SeriesType == model.SeriesType);
            if (!string.IsNullOrEmpty(model.ColorType))
            {
                var typedata = model.ColorType.Split(',');
                for (int i = 0; i < typedata.Count(); i++)
                {
                    var val = typedata[i];
                    query = query.Where(d => d.ColorType.Contains(val));
                }
            }
            if (!string.IsNullOrEmpty(model.SiteType))
            {
                var typedata = model.SiteType.Split(',');
                for (int i = 0; i < typedata.Count(); i++)
                {
                    var val = typedata[i];
                    query = query.Where(d => d.SiteType.Contains(val));
                }            
            }
            if (!string.IsNullOrEmpty(model.StyleType))
            {
                var typedata = model.StyleType.Split(',');
                for (int i = 0; i < typedata.Count(); i++)
                {
                    var val = typedata[i];
                    query = query.Where(d => d.StyleType.Contains(val));
                } 
            }
            #endregion

            var pageResult = query.OrderBy(q => q.ID).ToPageResult<Case>(model);
            model.PageResult = Mapper.Map<PageResult<CaseView>>(pageResult);

            #region 分類顯示文字
            foreach (var item in model.PageResult.Data)
            {
                item.SeriesTypeStr = seriesRepository.GetById(item.SeriesType).SeriesName;
            //    var colorTypeList = item.ColorType.Split(',');
            //    for (int i = 0; i < colorTypeList.Count() -1; i++)
            //    {
            //        item.ColorTypeStr += $"{ colorShadeRepository.GetById(colorTypeList[i]).Name } , ";
            //    }
            //    var siteTypeList = item.SiteType.Split(',');
            //    for (int i = 0; i < siteTypeList.Count() - 1; i++)
            //    {
            //        item.SiteTypeStr += $"{ caseSiteRepository.GetById(siteTypeList[i]).Name } , ";
            //    }
            //    var styleTypeList = item.StyleType.Split(',');
            //    for (int i = 0; i < styleTypeList.Count() - 1; i++)
            //    {
            //        item.StyleTypeStr += $"{ styleRepository.GetById(styleTypeList[i]).Name } , ";
            //    }
            }           
            #endregion

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            CaseView model;
            if (id == 0)
            {
                model = new CaseView();
            }
            else
            {
                var query = caseRepository.GetById(id);
                model = Mapper.Map<CaseView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(CaseView model, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);
            bool imageValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    model.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                Case casedata = Mapper.Map<Case>(model);
                casedata.UpdateDate = DateTime.Now;
                casedata.Updater = 1;
                if (model.ID == 0)
                {
                    casedata.CreateDate = DateTime.Now;
                    casedata.Creater = 1;
                    model.ID = caseRepository.Insert(casedata);
                }
                else
                {
                    caseRepository.Update(casedata);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(CaseAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Case casedata = caseRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, casedata.MainPic);
            caseRepository.Delete(casedata.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(CaseAdminController.Index));
        }
    }
}