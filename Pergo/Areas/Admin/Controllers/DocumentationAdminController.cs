﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Documentation;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class DocumentationAdminController : BaseAdminController
    {
        private DocumentationRepository documentationRepository;          

        public DocumentationAdminController() : this(null) { }

        public DocumentationAdminController(DocumentationRepository repo)
        {
            documentationRepository = repo ?? new DocumentationRepository(new PergoDBEntities());
        }

        // GET: Admin/DocumentationAdmin
        public ActionResult Index(DocumentationIndexView model)
        {
            var query = documentationRepository.Query(model.IsOnline);
            #region 搜尋條件            
            if (!string.IsNullOrEmpty(model.Title))
            {
                query = query.Where(d => d.Title.Contains(model.Title));
            }
            #endregion

            var pageResult = query.OrderBy(q => q.ID).ToPageResult<Documentation>(model);
            model.PageResult = Mapper.Map<PageResult<DocumentationView>>(pageResult);          

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            DocumentationView model;
            if (id == 0)
            {
                model = new DocumentationView();
            }
            else
            {
                var query = documentationRepository.GetById(id);
                model = Mapper.Map<DocumentationView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(DocumentationView model, HttpPostedFileBase pdf_file, HttpPostedFileBase pic_file)
        {
            bool hasPhoto = ImageHelper.CheckFileExists(pic_file);
            bool hasFile = ImageHelper.CheckFileExists(pdf_file);
            bool imageValid = !(model.ID == 0 && !hasPhoto);
            bool fileValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳圖片");
            }
            if (!fileValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳文件");
            }

            if (ModelState.IsValid)
            {
                if (hasPhoto)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    model.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                if (hasFile)
                {
                    string FileFolder = Path.Combine(Server.MapPath("~/FileUploads"), "DocumentationFile");
                    ImageHelper.DeleteFile(FileFolder, model.FileName);
                    model.FileName = ImageHelper.SaveFile(pdf_file, FileFolder, string.IsNullOrEmpty(model.FileName) ? "" : model.FileName.Replace(Path.GetExtension(pdf_file.FileName), ""));
                }

                Documentation documentationdata = Mapper.Map<Documentation>(model);
                documentationdata.UpdateDate = DateTime.Now;
                documentationdata.Updater = 1;
                if (model.ID == 0)
                {
                    documentationdata.CreateDate = DateTime.Now;
                    documentationdata.Creater = 1;
                    model.ID = documentationRepository.Insert(documentationdata);
                }
                else
                {
                    documentationRepository.Update(documentationdata);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(DocumentationAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Documentation documentationdata = documentationRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, documentationdata.MainPic);
            documentationRepository.Delete(documentationdata.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(DocumentationAdminController.Index));
        }
    }
}