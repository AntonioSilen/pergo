﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.CarouselBanner;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class CarouselBannerAdminController : BaseAdminController
    {
        private CarouselBannerRepository carouselBannerRepository;

        public CarouselBannerAdminController() : this(null) { }

        public CarouselBannerAdminController(CarouselBannerRepository repo)
        {
            carouselBannerRepository = repo ?? new CarouselBannerRepository(new PergoDBEntities());
        }

        // GET: Admin/CarouselAdmin
        public ActionResult Index(BannerIndexView model)
        {
            var query = carouselBannerRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<CarouselBanner>(model);
            model.PageResult = Mapper.Map<PageResult<BannerView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                item.Anchor = GetAnchor(item.ID);
            }
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            BannerView model;
            if (id == 0)
            {
                model = new BannerView();
            }
            else
            {
                var query = carouselBannerRepository.GetById(id);
                model = Mapper.Map<BannerView>(query);
                model.Anchor = GetAnchor(model.ID);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(BannerView model, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);
            bool imageValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    model.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }              

                CarouselBanner banner = Mapper.Map<CarouselBanner>(model);
                banner.UpdateDate = DateTime.Now;
                banner.Updater = 1;
                if (model.ID == 0)
                {
                    banner.CreateDate = DateTime.Now;
                    banner.Creater = 1;
                    model.ID = carouselBannerRepository.Insert(banner);
                }
                else
                {
                    banner.Anchor = GetAnchor(model.ID);

                    carouselBannerRepository.Update(banner);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(CarouselBannerAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CarouselBanner banner = carouselBannerRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, banner.MainPic);
            carouselBannerRepository.Delete(banner.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(CarouselBannerAdminController.Index));
        }

        public string GetAnchor(int id)
        {
            string hostStr = Request.Url.Scheme + "://" + Request.Url.Authority;
            string folderStr = "/Home/Index#";          
            return hostStr + folderStr + "fixed_" + id;
        }
    }
}
