﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Sample;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class SampleAdminController : BaseAdminController
    {      
        private SampleRepository sampleRepository;

        public SampleAdminController() : this(null) { }

        public SampleAdminController(SampleRepository repo)
        {
            sampleRepository = repo ?? new SampleRepository(new PergoDBEntities());
        }

        // GET: Admin/SampleAdmin
        public ActionResult Index(SampleIndexView model)
        {
            var query = sampleRepository.Query(model.IsReply);
            var pageResult = query.ToPageResult<Sample>(model);
            model.PageResult = Mapper.Map<PageResult<SampleView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            SampleView model;
            if (id == 0)
            {
                model = new SampleView();
            }
            else
            {
                var query = sampleRepository.GetById(id);
                model = Mapper.Map<SampleView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(SampleView model, HttpPostedFileBase MainPic)
        {            
            if (ModelState.IsValid)
            {
                Sample sample = Mapper.Map<Sample>(model);
                if (model.ID == 0)
                {
                    sample.CreateDate = DateTime.Now;
                    model.ID = sampleRepository.Insert(sample);
                }
                else
                {
                    sampleRepository.Update(sample);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(SampleAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Sample sample = sampleRepository.GetById(id);
            sampleRepository.Delete(sample.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(SampleAdminController.Index));
        }
    }
}