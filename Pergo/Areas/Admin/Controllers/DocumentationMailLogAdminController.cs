﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.DocumentationMailLog;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class DocumentationMailLogAdminController : BaseAdminController
    {
        private DocumentationMailLogRepository documentationMailLogRepository;

        public DocumentationMailLogAdminController() : this(null) { }

        public DocumentationMailLogAdminController(DocumentationMailLogRepository repo)
        {
            documentationMailLogRepository = repo ?? new DocumentationMailLogRepository(new PergoDBEntities());
        }

        // GET: Admin/DocumentationAdmin
        public ActionResult Index(DocumentationMailLogIndexView model, int docId = 0)
        {
            model.DocID = docId;
            var query = documentationMailLogRepository.Query(model.DocID, model.SearchTitle, model.Email);           

            var pageResult = query.OrderBy(q => q.ID).ToPageResult<DocumentationMailLog>(model);
            model.PageResult = Mapper.Map<PageResult<DocumentationMailLogView>>(pageResult);

            return View(model);
        }
    }
}