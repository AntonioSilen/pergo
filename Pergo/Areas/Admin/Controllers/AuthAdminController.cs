﻿using Pergo.Areas.Admin.ViewModels.Auth;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Pergo.Areas.Admin.Controllers
{
    public class AuthAdminController : BaseAdminController
    {
        private AdminRepository adminRepository;

        public AuthAdminController() : this(null) { }

        public AuthAdminController(AdminRepository repo)
        {
            adminRepository = repo ?? new AdminRepository();
        }

        // GET: Admin/AccountAdmin
        public ActionResult Login()
        {
#if DEBUG
            //if (System.Diagnostics.Debugger.IsAttached)
            //{
            //    var controller = DependencyResolver.Current.GetService<AuthAdminController>();
            //    controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //    return controller.Login(new LoginView() { Account = "sysadmin", Password = "123456" });
            //}
#endif
            var login = new LoginView();
            //login.Account = "sysadmin";
            //login.Password = "123456";
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                var admin = adminRepository.Login(login.Account, login.Password);
                if (admin != null)
                {
                    AdminInfo adminInfo = new AdminInfo();
                    adminInfo.ID = admin.ID;
                    adminInfo.Account = admin.Account;
                    AdminInfoHelper.Login(adminInfo, login.RememberMe);

                    return Redirect(FormsAuthentication.GetRedirectUrl(login.Account, false));
                }
            }
            return View(login);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AdminInfoHelper.Logout();
            return RedirectToAction("Login");
        }
    }
}