﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.SeriesBanner;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class SeriesBannerAdminController : BaseAdminController
    {       
        private SeriesBannerRepository seriesBannerRepository;
        private SeriesRepository seriesRepository;

        public SeriesBannerAdminController() : this(null, null) { }

        public SeriesBannerAdminController(SeriesBannerRepository repo, SeriesRepository repo2)
        {
            seriesBannerRepository = repo ?? new SeriesBannerRepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
        }

        // GET: Admin/SeriesBannerAdmin
        public ActionResult Index(BannerIndexView model, int page = 0)
        {
            var query = seriesBannerRepository.Query(model.IsOnline, model.SeriesID, model.Type);
            if (page != 0)
            {
                query = query.Where(s => s.Page == page);
            }
            if (model.Page != 0)
            {
                query = query.Where(s => s.Page == model.Page);
            }
            var pageResult = query.ToPageResult<SeriesBanner>(model);
            model.PageResult = Mapper.Map<PageResult<BannerView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                item.SeriesStr = seriesRepository.GetById(item.SeriesID).SeriesName;
                item.Anchor = GetAnchor(item.Page, item.SeriesID, item.ID);
            }

            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            model.SeriesOptions = result;
            model.Page = page;
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            BannerView model;
            if (id == 0)
            {
                model = new BannerView();
            }
            else
            {
                var query = seriesBannerRepository.GetById(id);
                model = Mapper.Map<BannerView>(query);
                model.Anchor = GetAnchor(model.Page, model.SeriesID, model.ID);
            }

            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            model.SeriesOptions = result;
            model.Type = 1;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(BannerView model, HttpPostedFileBase main_photo, HttpPostedFileBase mobile_photo)
        {

            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            model.SeriesOptions = result;
            bool hasFile = ImageHelper.CheckFileExists(main_photo);
            bool hasMobileFile = ImageHelper.CheckFileExists(mobile_photo);
            bool imageValid = !(model.ID == 0 && !hasFile);
            bool imageMobileValid = !(model.ID == 0 && !hasMobileFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳圖片");
            }
            if (!imageMobileValid)
            {
                ModelState.AddModelError(nameof(model.MobileMainPic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    model.MainPic = ImageHelper.SaveFile(main_photo, PhotoFolder, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "_p");
                }
                if (hasMobileFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MobileMainPic);
                    model.MobileMainPic = ImageHelper.SaveFile(mobile_photo, PhotoFolder, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "_m");
                }

                SeriesBanner banner = Mapper.Map<SeriesBanner>(model);
                banner.UpdateDate = DateTime.Now;
                banner.Updater = 1;
                if (model.ID == 0)
                {
                    banner.CreateDate = DateTime.Now;
                    banner.Creater = 1;
                    model.ID = seriesBannerRepository.Insert(banner);
                }
                else
                {
                    banner.Anchor = GetAnchor(model.Page, model.SeriesID, model.ID);

                    seriesBannerRepository.Update(banner);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(SeriesBannerAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            SeriesBanner banner = seriesBannerRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, banner.MainPic);
            seriesBannerRepository.Delete(banner.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(SeriesBannerAdminController.Index));
        }

        public string GetAnchor(int page, int sid, int id)
        {
            string hostStr = Request.Url.Scheme + "://" + Request.Url.Authority;
            string folderStr = "";

            switch (page)
            {
                case 1:
                    folderStr = "/Series/Overview/" + sid + "#";
                    break;
                case 2:
                    folderStr = "/Series/Feature/" + sid + "#";
                    break;
                case 3:
                    folderStr = "/Series/Tech/" + sid + "#";
                    break;
                case 4:
                    folderStr = "/Series/Color?sid=" + sid + "#";
                    break;
                case 5:
                    folderStr = "/Series/Spec/" + sid + "#";
                    break;
                default:
                    folderStr = "/Series/Overview/" + sid + "#";
                    break;
            }            
            
            return hostStr + folderStr + "series_" + id;
        }
    }
}