﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.SampleProduct;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class SampleProductAdminController : BaseAdminController
    {
        private SampleProductRepository sampleProductRepository;

        public SampleProductAdminController() : this(null) { }

        public SampleProductAdminController(SampleProductRepository repo)
        {
            sampleProductRepository = repo ?? new SampleProductRepository(new PergoDBEntities());
        }

        // GET: Admin/SampleProductAdmin
        public ActionResult Index(SampleProductIndexView model, int sid = 0)
        {
            var query = sampleProductRepository.Query().Where(s => s.SampleID == sid);
            var pageResult = query.ToPageResult<SampleProduct>(model);
            model.PageResult = Mapper.Map<PageResult<SampleProductView>>(pageResult);
            return View(model);
        }

        //public ActionResult Edit(int id = 0)
        //{
        //    BannerView model;
        //    if (id == 0)
        //    {
        //        model = new BannerView();
        //    }
        //    else
        //    {
        //        var query = sampleProductRepository.GetById(id);
        //        model = Mapper.Map<BannerView>(query);
        //    }
        //    return View(model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        //public ActionResult Edit(BannerView model, HttpPostedFileBase MainPic)
        //{
        //    if (ModelState.IsValid)
        //    {   
        //        SampleProduct sampleProduct = Mapper.Map<SampleProduct>(model);
        //        if (model.ID == 0)
        //        {
        //            model.ID = sampleProductRepository.Insert(sampleProduct);
        //        }
        //        else
        //        {
        //            sampleProductRepository.Update(sampleProduct);
        //        }
        //        ShowMessage(true, "");
        //        return RedirectToAction(nameof(SampleProductAdminController.Edit), new { id = model.ID });
        //    }

        //    ShowMessage(false, "輸入資料有誤");
        //    return View(model);
        //}

        public ActionResult Delete(int id)
        {
            SampleProduct sampleProduct = sampleProductRepository.GetById(id);
            sampleProductRepository.Delete(sampleProduct.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(SampleProductAdminController.Index));
        }
    }
}