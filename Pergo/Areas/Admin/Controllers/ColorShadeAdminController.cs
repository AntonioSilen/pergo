﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.ColorShade;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ColorShadeAdminController : BaseAdminController
    {
        private ColorShadeRepository colorShadeRepository;

        public ColorShadeAdminController() : this(null) { }

        public ColorShadeAdminController(ColorShadeRepository repo)
        {
            colorShadeRepository = repo ?? new ColorShadeRepository(new PergoDBEntities());
        }

        // GET: Admin/ColorShadeAdmin
        public ActionResult Index(ColorShadeIndexView model)
        {
            var query = colorShadeRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<ColorShade>(model);
            model.PageResult = Mapper.Map<PageResult<ColorShadeView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            ColorShadeView model;
            if (id == 0)
            {
                model = new ColorShadeView();
            }
            else
            {
                var query = colorShadeRepository.GetById(id);
                model = Mapper.Map<ColorShadeView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ColorShadeView model)
        {      
            if (ModelState.IsValid)
            {     
                ColorShade colorShade = Mapper.Map<ColorShade>(model);
                colorShade.UpdateDate = DateTime.Now;
                colorShade.Updater = 1;
                if (model.ID == 0)
                {
                    colorShade.CreateDate = DateTime.Now;
                    colorShade.Creater = 1;
                    model.ID = colorShadeRepository.Insert(colorShade);
                }
                else
                {
                    colorShadeRepository.Update(colorShade);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ColorShadeAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            ColorShade colorShade = colorShadeRepository.GetById(id);          
            colorShadeRepository.Delete(colorShade.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(ColorShadeAdminController.Index));
        }
    }
}