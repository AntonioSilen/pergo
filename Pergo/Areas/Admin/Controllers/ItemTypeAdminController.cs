﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.ItemType;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ItemTypeAdminController : BaseAdminController
    {       
        private ItemTypeRepository itemTypeRepository;
        private SeriesRepository seriesRepository;

        public ItemTypeAdminController() : this(null, null) { }

        public ItemTypeAdminController(ItemTypeRepository repo, SeriesRepository repo2)
        {
            itemTypeRepository = repo ?? new ItemTypeRepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
        }

        // GET: Admin/ItemTypeAdmin
        public ActionResult Index(ItemTypeIndexView model)
        {
            var query = itemTypeRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<ItemType>(model);
            model.PageResult = Mapper.Map<PageResult<ItemTypeView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                item.SeriesStr = seriesRepository.GetById(Convert.ToInt32(item.SeriesID)).SeriesName;
            }
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            ItemTypeView model;              
            if (id == 0)
            {
                model = new ItemTypeView();
            }
            else
            {
                var query = itemTypeRepository.GetById(id);
                model = Mapper.Map<ItemTypeView>(query);
            }
            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            model.SeriesList = result;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ItemTypeView model)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            var values = seriesRepository.GetAll().Where(s => s.IsOnline == true).ToList();
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Text = v.SeriesName,
                    Value = v.ID.ToString()
                });
            }
            model.SeriesList = result;
            if (ModelState.IsValid)
            {          
                ItemType itemType = Mapper.Map<ItemType>(model);
                itemType.UpdateDate = DateTime.Now;
                itemType.Updater = 1;
                if (model.ID == 0)
                {
                    itemType.CreateDate = DateTime.Now;
                    itemType.Creater = 1;
                    model.ID = itemTypeRepository.Insert(itemType);
                }
                else
                {
                    itemTypeRepository.Update(itemType);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ItemTypeAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            ItemType itemType = itemTypeRepository.GetById(id);
            itemTypeRepository.Delete(itemType.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(ItemTypeAdminController.Index));
        }
    }
}