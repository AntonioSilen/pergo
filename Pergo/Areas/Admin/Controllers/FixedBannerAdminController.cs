﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.FixedBanner;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class FixedBannerAdminController : BaseAdminController
    {        
        private FixedBannerRepository fixedBannerRepository;

        public FixedBannerAdminController() : this(null) { }

        public FixedBannerAdminController(FixedBannerRepository repo)
        {
            fixedBannerRepository = repo ?? new FixedBannerRepository(new PergoDBEntities());
        }

        // GET: Admin/FixedAdmin
        public ActionResult Index(BannerIndexView model)
        {
            var query = fixedBannerRepository.Query(model.IsOnline, model.Type);
            var pageResult = query.ToPageResult<FixedBanner>(model);
            model.PageResult = Mapper.Map<PageResult<BannerView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                item.Anchor = GetAnchor(item.Type, item.ID);
            }
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            BannerView model;
            if (id == 0)
            {
                model = new BannerView();
            }
            else
            {
                var query = fixedBannerRepository.GetById(id);
                model = Mapper.Map<BannerView>(query);               
                model.Anchor = GetAnchor(model.Type, model.ID);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(BannerView model, HttpPostedFileBase main_photo, HttpPostedFileBase mobile_photo)
        {
            bool hasFile = ImageHelper.CheckFileExists(main_photo);
            bool hasMobileFile = ImageHelper.CheckFileExists(mobile_photo);
            bool imageValid = !(model.ID == 0 && !hasFile);
            bool imageMobileValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳圖片");
            }
            if (!imageMobileValid)
            {
                ModelState.AddModelError(nameof(model.MobileMainPic), "請上傳手機圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    model.MainPic = ImageHelper.SaveFile(main_photo, PhotoFolder, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "_p");
                }
                if (hasMobileFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MobileMainPic);
                    model.MobileMainPic = ImageHelper.SaveFile(mobile_photo, PhotoFolder, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "_m");
                }

                FixedBanner banner = Mapper.Map<FixedBanner>(model);
                banner.UpdateDate = DateTime.Now;
                banner.Updater = 1;
                if (model.ID == 0)
                {
                    banner.CreateDate = DateTime.Now;
                    banner.Creater = 1;
                    model.ID = fixedBannerRepository.Insert(banner);
                }
                else
                {                   
                    banner.Anchor = GetAnchor(model.Type, model.ID);

                    fixedBannerRepository.Update(banner);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(FixedBannerAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            FixedBanner banner = fixedBannerRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, banner.MainPic);
            fixedBannerRepository.Delete(banner.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(FixedBannerAdminController.Index));
        }

        public string GetAnchor(int type, int id)
        {
            string hostStr = Request.Url.Scheme + "://" + Request.Url.Authority;
            string folderStr = "";
            if (type >= 1 && type <= 3)
            {
                folderStr = "/Home/Index#";
            }
            else if (type >= 4 && type <= 6)
            {
                folderStr = "/Characteristic/Index/Laminate#";
            }
            else if (type >= 7 && type <= 9)
            {
                folderStr = "/Characteristic/Index/Vinyl#";
            }
            return hostStr + folderStr + "fixed_" + type + "_" + id;
        }
    }
}