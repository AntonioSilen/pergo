﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.SEO;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class SEOAdminController : BaseAdminController
    {
        private SEORepository seoRepository;
        private SeriesRepository seriesRepository;

        public SEOAdminController() : this(null, null) { }

        public SEOAdminController(SEORepository repo, SeriesRepository repo2)
        {
            seoRepository = repo ?? new SEORepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
        }

        // GET: Admin/SEOAdmin
        public ActionResult Index(SEOIndexView model)
        {
            var query = seoRepository.Query();
            model.SEOList = Mapper.Map<List<SEOView>>(query);
            foreach (var item in model.SEOList)
            {
                item.SEOPage = SEOPage(item);
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            SEOView model;
            if (id == 0)
            {
                return RedirectToAction("Index", "HomeAdmin");
            }
            else
            {
                var query = seoRepository.GetById(id);
                model = Mapper.Map<SEOView>(query);
                model.SEOPage = SEOPage(model);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(SEOView seomodel, HttpPostedFileBase Image)
        {
            bool hasFile = ImageHelper.CheckFileExists(Image);

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, seomodel.Image);
                    seomodel.Image = ImageHelper.SaveFile(Image, PhotoFolder);
                }

                SEO seo = Mapper.Map<SEO>(seomodel);
                if (seomodel.ID == 0)
                {
                    seomodel.ID = seoRepository.Insert(seo);
                }
                else
                {
                    seoRepository.Update(seo);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(SEOAdminController.Edit));
            }

            ShowMessage(false, "輸入資料有誤");
            return View(seomodel);
        }
        
        public string SEOPage(SEOView model)
        {
            string SEOPage = "";
            switch (model.Controller)
            {
                case "Home":
                    if (model.Action == "Accessories")
                    {
                        if (model.Parameter == "Laminate")
                        {
                            SEOPage = "超耐磨配件頁";
                        }
                        else
                        {
                            SEOPage = "防水配件頁";
                        }
                    }
                    else
                    {
                        SEOPage = "首頁";
                    }
                    break;
                case "Characteristic":
                    if (model.Parameter == "Laminate")
                    {
                        SEOPage = "超耐磨頁";
                    }
                    else
                    {
                        SEOPage = "防水頁";
                    }
                    break;
                case "Comparison":
                    if (!string.IsNullOrEmpty(model.Parameter))
                    {
                        if (model.Parameter == "Laminate")
                        {
                            SEOPage = "超耐磨比較";
                        }
                        else
                        {
                            SEOPage = "防水比較";
                        }
                    }
                    else
                    {
                        SEOPage = "綜合產品比較";
                    }
                    break;
                case "Series":
                    var data = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == model.Parameter).FirstOrDefault();
                    SEOPage = data == null ? "" : data.SeriesName;
                    break;
                case "Support":
                    switch (model.Action)
                    {
                        case "Index":
                            SEOPage = "支援服務總頁";
                            break;
                        case "ContactUs":
                            SEOPage = "線上預約";
                            break;
                        case "FAQ":
                            SEOPage = "FAQ";
                            break;
                        case "Documentation":
                            SEOPage = "文件下載";
                            break;
                        case "Store":
                            SEOPage = "尋找門市";
                            break;
                        default:
                            SEOPage = "支援服務總頁";
                            break;
                    }
                    break;
                default:
                    break;
            }
            return SEOPage;
        }
    }
}