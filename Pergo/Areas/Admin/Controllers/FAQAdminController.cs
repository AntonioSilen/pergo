﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.FAQ;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class FAQAdminController : BaseAdminController
    {       
        private FAQRepository faqRepository;

        public FAQAdminController() : this(null) { }

        public FAQAdminController(FAQRepository repo)
        {
            faqRepository = repo ?? new FAQRepository(new PergoDBEntities());
        }

        // GET: Admin/FAQAdmin
        public ActionResult Index(FAQIndexView model)
        {
            var query = faqRepository.Query(model.IsOnline);
            if (model.BrandType != 0)
            {
                query = query.Where(q => q.BrandType == model.BrandType);
            }
            if (model.QuestionType != 0)
            {
                query = query.Where(q => q.QuestionType == model.QuestionType);
            }
            var pageResult = query.ToPageResult<FAQ>(model);
            model.PageResult = Mapper.Map<PageResult<FAQView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            FAQView model;
            if (id == 0)
            {
                model = new FAQView();
            }
            else
            {
                var query = faqRepository.GetById(id);
                model = Mapper.Map<FAQView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(FAQView model)
        {
            if (ModelState.IsValid)
            {             
                FAQ FAQ = Mapper.Map<FAQ>(model);
                FAQ.UpdateDate = DateTime.Now;
                FAQ.Updater = 1;
                if (model.ID == 0)
                {
                    FAQ.CreateDate = DateTime.Now;
                    FAQ.Creater = 1;
                    model.ID = faqRepository.Insert(FAQ);
                }
                else
                {
                    faqRepository.Update(FAQ);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(FAQAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            FAQ FAQ = faqRepository.GetById(id);
            faqRepository.Delete(FAQ.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(FAQAdminController.Index));
        }
    }
}