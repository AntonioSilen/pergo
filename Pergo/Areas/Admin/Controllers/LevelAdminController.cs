﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Level;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class LevelAdminController : BaseAdminController
    {       
        private LevelRepository levelRepository;
        private SeriesRepository seriesRepository;

        public LevelAdminController() : this(null, null) { }

        public LevelAdminController(LevelRepository repo, SeriesRepository repo2)
        {
            levelRepository = repo ?? new LevelRepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
        }

        // GET: Admin/LevelAdmin
        public ActionResult Index(LevelIndexView model, int sid)
        {
            var query = levelRepository.Query(model.IsOnline).Where(l => l.SeriesID == sid);
            var pageResult = query.ToPageResult<ProductLevel>(model);
            model.SeriesID = sid;
            model.PageResult = Mapper.Map<PageResult<LevelView>>(pageResult);
            var seriesInfo = seriesRepository.GetById(model.PageResult.Data.FirstOrDefault().SeriesID);
            if (seriesInfo.Brand == 1)
            {
                model.PageResult.Data = model.PageResult.Data.Where(l => l.Level <= 3);
            }
            else
            {
                model.PageResult.Data = model.PageResult.Data.Where(l => l.Level >= 4);
            }
            model.SeriesStr = seriesInfo.SeriesName;
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            LevelView model;
            if (id == 0)
            {
                model = new LevelView();
            }
            else
            {
                var query = levelRepository.GetById(id);
                model = Mapper.Map<LevelView>(query);
            }
            model.SeriesStr = seriesRepository.GetById(model.SeriesID).SeriesName;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(LevelView model)
        {
            bool imageValid = !(model.ID == 0);

            if (ModelState.IsValid)
            {
                ProductLevel level = Mapper.Map<ProductLevel>(model);
                level.UpdateDate = DateTime.Now;
                level.Updater = 1;
                levelRepository.Update(level);

                ShowMessage(true, "");
                return View(model);
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }      
    }
}