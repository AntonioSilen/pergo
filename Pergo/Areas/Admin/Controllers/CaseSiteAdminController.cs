﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.CaseSite;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class CaseSiteAdminController : BaseAdminController
    {
        private CaseSiteRepository caseSiteRepository;

        public CaseSiteAdminController() : this(null) { }

        public CaseSiteAdminController(CaseSiteRepository repo)
        {
            caseSiteRepository = repo ?? new CaseSiteRepository(new PergoDBEntities());
        }

        // GET: Admin/CaseSiteAdmin
        public ActionResult Index(CaseSiteIndexView model)
        {
            var query = caseSiteRepository.Query(model.IsOnline, model.Name);
            var pageResult = query.ToPageResult<CaseSite>(model);
            model.PageResult = Mapper.Map<PageResult<CaseSiteView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            CaseSiteView model;
            if (id == 0)
            {
                model = new CaseSiteView();
            }
            else
            {
                var query = caseSiteRepository.GetById(id);
                model = Mapper.Map<CaseSiteView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(CaseSiteView model, HttpPostedFileBase Icon, HttpPostedFileBase IconHover)
        {
            bool hasFile_IC = ImageHelper.CheckFileExists(Icon);
            bool hasFile_ICH = ImageHelper.CheckFileExists(IconHover);

            if (ModelState.IsValid)
            {
                if (hasFile_IC)
                {
                    string filename = $"icon-{ model.ID }";
                    ImageHelper.DeleteFile(PhotoFolder, model.Icon);
                    model.Icon = ImageHelper.SaveFile(Icon, PhotoFolder, filename);
                }
                if (hasFile_ICH)
                {
                    string filename = $"icon-{ model.ID }-hover";
                    ImageHelper.DeleteFile(PhotoFolder, model.IconHover);
                    model.IconHover = ImageHelper.SaveFile(IconHover, PhotoFolder, filename);
                }

                CaseSite caseSite = Mapper.Map<CaseSite>(model);
                caseSite.UpdateDate = DateTime.Now;
                caseSite.Updater = 1;
                if (model.ID == 0)
                {
                    caseSite.CreateDate = DateTime.Now;
                    caseSite.Creater = 1;
                    model.ID = caseSiteRepository.Insert(caseSite);
                }
                else
                {
                    caseSiteRepository.Update(caseSite);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(CaseSiteAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CaseSite caseSite = caseSiteRepository.GetById(id);
            caseSiteRepository.Delete(caseSite.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(CaseSiteAdminController.Index));
        }
    }
}