﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.ProductImage;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProductImageAdminController : BaseAdminController
    {        
        private ProductImageRepository productImageRepository;
        private ProductRepository productRepository;

        public ProductImageAdminController() : this(null, null) { }

        public ProductImageAdminController(ProductImageRepository repo, ProductRepository repo2)
        {
            productImageRepository = repo ?? new ProductImageRepository(new PergoDBEntities());
            productRepository = repo2 ?? new ProductRepository(new PergoDBEntities());
        }

        // GET: Admin/ProductImageAdmin
        public ActionResult Index(ProductImageIndexView model, int pid = 0)
        {
            //var query = productImageRepository.Query(model.IsOnline, pid);
            //var pageResult = query.ToPageResult<ProductImage>(model);
            //model.ProductID = pid;
            //model.ProductStr = productRepository.GetById(pid).Name;
            //model.PageResult = Mapper.Map<PageResult<ProductImageView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(/*int id = 0, */int pid = 0)
        {
            //ProductImageView model;
            //if (id == 0)
            //{
            //    model = new ProductImageView();
            //}
            //else
            //{
            //    var query = productImageRepository.GetById(id);
            //    model = Mapper.Map<ProductImageView>(query);                     
            //}
            ProductImageIndexView model = new ProductImageIndexView();

            model.ProductID = pid;
            model.ProductStr = productRepository.GetById(model.ProductID).Name;
            model.ImageList = Mapper.Map<List<ProductImageView>>(productImageRepository.Query(null, pid).ToList());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ProductImageIndexView model, HttpPostedFileBase[] CarousePictures)
        {
            model.ProductStr = productRepository.GetById(model.ProductID).Name;
            bool hasFile = false;
            if (CarousePictures.Count() <= 0)
            {
                ModelState.AddModelError(nameof(model.CarousePictures), "請上傳至少一張圖片。");
            }
            else
            {
                foreach (var carousePic in CarousePictures)
                {
                    hasFile = ImageHelper.CheckFileExists(carousePic);
                }
            }

            //bool imageValid = !(!hasFile);
            if (hasFile == false)
            {
                ModelState.AddModelError(nameof(model.CarousePictures), "所選檔案不存在，請重新選擇檔案。");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    try
                    {
                        foreach (var carousePic in CarousePictures)
                        {
                            ProductImageView image = new ProductImageView();
                            image.CarousePic = ImageHelper.SaveFile(carousePic, PhotoFolder);
                            image.ProductID = model.ProductID;
                            image.IsOnline = hasFile;

                            productImageRepository.Insert(Mapper.Map<ProductImage>(image));
                        }
                        ShowMessage(true, "");
                        return RedirectToAction(nameof(ProductImageAdminController.Edit), new { pid = model.ProductID });
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(false, "輸入資料有誤 : " + ex.Message);
                        return View(model);
                    }
                }
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    ProductImage image = productImageRepository.GetById(id);
        //    ImageHelper.DeleteFile(PhotoFolder, image.CarousePic);
        //    productImageRepository.Delete(image.ID);
        //    ShowMessage(true, "Data deleted.");
        //    return RedirectToAction(nameof(ProductImageAdminController.Index));
        //}

        public string DeletePhoto(int photoId)
        {
            try
            {
                ProductImage image = productImageRepository.GetById(photoId);
                ImageHelper.DeleteFile(PhotoFolder, image.CarousePic);
                productImageRepository.Delete(photoId);

                return "Success";
            }
            catch (Exception ex)
            {
                return "Failed";
            }
        }
    }
}