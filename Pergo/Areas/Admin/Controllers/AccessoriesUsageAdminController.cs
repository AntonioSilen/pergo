﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.AccessoriesUsage;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class AccessoriesUsageAdminController : BaseAdminController
    {
        private AccessoriesRepository accessoriesRepository;
        private AccessoriesUsageRepository accessoriesUsageRepository;

        public AccessoriesUsageAdminController() : this(null, null) { }

        public AccessoriesUsageAdminController(AccessoriesRepository repo, AccessoriesUsageRepository repo2)
        {
            accessoriesRepository = repo ?? new AccessoriesRepository(new PergoDBEntities());
            accessoriesUsageRepository = repo2 ?? new AccessoriesUsageRepository(new PergoDBEntities());
        }
        // GET: Admin/AccessoriesUsageAdmin
        public ActionResult Index(AccessoriesUsageIndexView model, int aid = 0)
        {
            if (aid == 0)
            {
                return RedirectToAction("Index", "AccessoriesAdmin");
            }
            var query = accessoriesUsageRepository.Query(model.IsOnline, aid, model.Title);
            var pageResult = query.ToPageResult<AccessoriesUsage>(model);
            model.PageResult = Mapper.Map<PageResult<AccessoriesUsageView>>(pageResult);
            model.AccessoriesID = aid;
            model.AccessoriesStr = accessoriesRepository.GetById(aid).Title;
            return View(model);
        }

        public ActionResult Edit(int id = 0, int aid = 0)
        {
            AccessoriesUsageView model;
            if (id == 0)
            {
                model = new AccessoriesUsageView();
            }
            else
            {
                var query = accessoriesUsageRepository.GetById(id);
                model = Mapper.Map<AccessoriesUsageView>(query);
            }
            model.AccessoriesID = aid;
            model.AccessoriesStr = accessoriesRepository.GetById(aid).Title;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AccessoriesUsageView accessoriesusagemodel, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, accessoriesusagemodel.MainPic);
                    accessoriesusagemodel.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                AccessoriesUsage accessoriesusage = Mapper.Map<AccessoriesUsage>(accessoriesusagemodel);
                accessoriesusage.UpdateDate = DateTime.Now;
                accessoriesusage.Updater = 1;
                if (accessoriesusagemodel.ID == 0)
                {
                    accessoriesusage.CreateDate = DateTime.Now;
                    accessoriesusage.Creater = 1;
                    accessoriesusagemodel.ID = accessoriesUsageRepository.Insert(accessoriesusage);
                }
                else
                {
                    accessoriesUsageRepository.Update(accessoriesusage);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(AccessoriesUsageAdminController.Edit), new { id = accessoriesusagemodel.ID, aid = accessoriesusagemodel.AccessoriesID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(accessoriesusagemodel);
        }

        public ActionResult Delete(int id)
        {
            AccessoriesUsage store = accessoriesUsageRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, store.MainPic);
            accessoriesUsageRepository.Delete(store.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(AccessoriesUsageAdminController.Index));
        }
    }
}