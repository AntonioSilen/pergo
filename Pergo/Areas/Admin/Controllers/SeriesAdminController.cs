﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Level;
using Pergo.Areas.Admin.ViewModels.Series;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class SeriesAdminController : BaseAdminController
    {        
        private SeriesRepository seriesRepository;
        private LevelRepository levelRepository;
        private ProductRepository productRepository;
        private ProductImageRepository productImageRepository;
        private SEORepository seoRepository;

        public SeriesAdminController() : this(null, null, null, null, null) { }

        public SeriesAdminController(SeriesRepository repo, LevelRepository repo2, ProductRepository repo3, ProductImageRepository repo4, SEORepository repo5)
        {
            seriesRepository = repo ?? new SeriesRepository(new PergoDBEntities());
            levelRepository = repo2 ?? new LevelRepository(new PergoDBEntities());
            productRepository = repo3 ?? new ProductRepository(new PergoDBEntities());
            productImageRepository = repo4 ?? new ProductImageRepository(new PergoDBEntities());
            seoRepository = repo5 ?? new SEORepository(new PergoDBEntities());
        }

        // GET: Admin/ProductSeriesAdmin
        public ActionResult Index(SeriesIndexView model)
        {
            var query = seriesRepository.Query(model.IsOnline, model.Brand, model.SeriesName);
            var pageResult = query.ToPageResult<ProductSeries>(model);
            model.PageResult = Mapper.Map<PageResult<SeriesView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            SeriesView model;
            if (id == 0)
            {
                model = new SeriesView();
            }
            else
            {
                var query = seriesRepository.GetById(id);
                model = Mapper.Map<SeriesView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(SeriesView seriesmodel, 
            HttpPostedFileBase Icon, HttpPostedFileBase IconHover, HttpPostedFileBase sample_pic_file, HttpPostedFileBase case_pic_file
            )
        {
            bool hasFile_IC = ImageHelper.CheckFileExists(Icon);
            bool hasFile_ICH = ImageHelper.CheckFileExists(IconHover);
            bool hasSamplePic = ImageHelper.CheckFileExists(sample_pic_file);
            bool hasCasePic = ImageHelper.CheckFileExists(case_pic_file);

            if (ModelState.IsValid)
            {
                if (hasFile_IC)
                {
                    string filename = $"icon-{ seriesmodel.SeriesEngName.ToLower().Replace(" ", "") }-{seriesmodel.CreateDate.ToString("yyyyMMddHHmms")}";
                    ImageHelper.DeleteFile(PhotoFolder, seriesmodel.Icon);
                    seriesmodel.Icon = ImageHelper.SaveFile(Icon, PhotoFolder, filename);
                }
                if (hasFile_ICH)
                {
                    string filename = $"icon-{ seriesmodel.SeriesEngName.ToLower().Replace(" ", "") }-{seriesmodel.CreateDate.ToString("yyyyMMddHHmms") }-hover";
                    ImageHelper.DeleteFile(PhotoFolder, seriesmodel.IconHover);
                    seriesmodel.IconHover = ImageHelper.SaveFile(IconHover, PhotoFolder, filename);
                }
                if (hasSamplePic)
                {
                    string filename = $"sample-{ seriesmodel.SeriesEngName.ToLower().Replace(" ", "") }-{seriesmodel.CreateDate.ToString("yyyyMMddHHmms")}";
                    ImageHelper.DeleteFile(PhotoFolder, seriesmodel.SamplePic);
                    seriesmodel.SamplePic = ImageHelper.SaveFile(sample_pic_file, PhotoFolder, filename);
                }
                if (hasCasePic)
                {
                    string filename = $"case-{ seriesmodel.SeriesEngName.ToLower().Replace(" ", "") }-{seriesmodel.CreateDate.ToString("yyyyMMddHHmms")}";
                    ImageHelper.DeleteFile(PhotoFolder, seriesmodel.CasePic);
                    seriesmodel.CasePic = ImageHelper.SaveFile(case_pic_file, PhotoFolder, filename);
                }

                ProductSeries series = Mapper.Map<ProductSeries>(seriesmodel);
                series.UpdateDate = DateTime.Now;
                series.Updater = 1;
                if (seriesmodel.ID == 0)
                {
                    series.CreateDate = DateTime.Now;
                    series.Creater = 1;
                    seriesmodel.ID = seriesRepository.Insert(series);
                    if (seriesmodel.ID == 0)
                    {
                        ShowMessage(false, "新增失敗，請聯繫工程師");
                        return View(seriesmodel);
                    }

                    //新增等級
                    if (series.Brand == 1)
                    {
                        var livingLevel = new ProductLevel();
                        var originalLevel = new ProductLevel();
                        var publicLevel = new ProductLevel();

                        livingLevel.SeriesID = originalLevel.SeriesID = publicLevel.SeriesID = seriesmodel.ID;
                        livingLevel.CreateDate = originalLevel.CreateDate = publicLevel.CreateDate = series.CreateDate;
                        livingLevel.Creater = originalLevel.Creater = publicLevel.Creater = 1;
                        livingLevel.UpdateDate = originalLevel.UpdateDate = publicLevel.UpdateDate = series.UpdateDate;
                        livingLevel.Updater = originalLevel.Updater = publicLevel.Updater = 1;
                        livingLevel.Guarantee = originalLevel.Guarantee = publicLevel.Guarantee = "";
                        livingLevel.Level = 1;
                        originalLevel.Level = 2;
                        publicLevel.Level = 3;
                        livingLevel.IsOnline = true;
                        originalLevel.IsOnline = publicLevel.IsOnline = false;

                        //新增等級
                        levelRepository.Insert(livingLevel);
                        levelRepository.Insert(originalLevel);
                        levelRepository.Insert(publicLevel);
                    }
                    else if(series.Brand == 2)
                    {
                        var optimumLevel = new ProductLevel();
                        var premiumLevel = new ProductLevel();

                        optimumLevel.SeriesID = premiumLevel.SeriesID = seriesmodel.ID;
                        optimumLevel.CreateDate = premiumLevel.CreateDate = series.CreateDate;
                        optimumLevel.Creater = premiumLevel.Creater = 1;
                        optimumLevel.UpdateDate = premiumLevel.UpdateDate = series.UpdateDate;
                        optimumLevel.Updater = premiumLevel.Updater = 1;
                        optimumLevel.Guarantee = premiumLevel.Guarantee = "";
                        optimumLevel.Level = 4;
                        premiumLevel.Level = 5;
                        optimumLevel.IsOnline = premiumLevel.IsOnline = true;

                        //新增等級
                        levelRepository.Insert(optimumLevel);
                        levelRepository.Insert(premiumLevel);
                    }

                    //新增SEO
                    SEO seoData = new SEO()
                        {
                        Title = "",
                        Description = "",
                        Url = "",
                        Keywords = "",
                        Image = "",
                         Controller = "Series",
                         Action = "Overview",
                         Parameter = seriesmodel.SeriesEngName.Replace(" ", ""),
                        };
                    var result = seoRepository.Insert(seoData);
                }
                else
                {
                    seriesRepository.Update(series);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(SeriesAdminController.Edit), new { id = seriesmodel.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(seriesmodel);
        }

        //public ActionResult Delete(int id)
        //{
        //    ProductSeries series = seriesRepository.GetById(id);
        //    ImageHelper.DeleteFile(PhotoFolder, series.Icon);
        //    ImageHelper.DeleteFile(PhotoFolder, series.IconHover);
        //    ImageHelper.DeleteFile(PhotoFolder, series.SamplePic);
        //    ImageHelper.DeleteFile(PhotoFolder, series.CasePic);
        //    seriesRepository.Delete(series.ID);
        //    var levelList = levelRepository.GetAll().Where(l => l.SeriesID == id);
        //    if (levelList != null)
        //    {
        //        foreach (var item in levelList)
        //        {
        //            levelRepository.Delete(item.ID);
        //        }
        //    }
        //    var colorList = productRepository.GetAll().Where(c => c.SeriesID == id);
        //    if (colorList != null)
        //    {
        //        foreach (var item in colorList)
        //        {
        //            var imageList = productImageRepository.GetAll().Where(c => c.ProductID == item.ID);
        //            if (imageList != null)
        //            {
        //                foreach (var image in imageList)
        //                {
        //                    productImageRepository.Delete(image.ID);
        //                }
        //            }
        //            productRepository.Delete(item.ID);
        //        }
        //    }          

        //    ShowMessage(true, "Data deleted.");
        //    return RedirectToAction(nameof(SeriesAdminController.Index));
        //}
    }
}