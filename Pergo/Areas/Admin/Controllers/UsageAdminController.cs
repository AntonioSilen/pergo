﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Areas.Admin.ViewModels.Usage;
using Pergo.Models;
using Pergo.Models.Cmind;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class UsageAdminController : BaseAdminController
    {
        private SeriesRepository seriesRepository;
        private UsageRepository usageRepository;
        private CaseSiteRepository caseSiteRepository;
        private ProductRepository productRepository;

        public UsageAdminController() : this(null, null, null, null) { }

        public UsageAdminController(SeriesRepository repo, UsageRepository repo2, CaseSiteRepository repo3, ProductRepository repo4)
        {
            seriesRepository = repo ?? new SeriesRepository(new PergoDBEntities());
            usageRepository = repo2 ?? new UsageRepository(new PergoDBEntities());
            caseSiteRepository = repo3 ?? new CaseSiteRepository(new PergoDBEntities());
            productRepository = repo4 ?? new ProductRepository(new PergoDBEntities());         
        }

        // GET: Admin/UsageAdmin
        public ActionResult Index(UsageIndexView model, int sid = 0)
        {
            if (sid != 0)
            {
                model.SeriesID = sid;
            }
            if (model.SeriesID != 0)
            {
                //model.SeriesID = sid;
                model.SeriesStr = seriesRepository.GetById(model.SeriesID).SeriesName;                
                model.SiteOptions = GetUsageSite(model.SeriesID);

                var query = usageRepository.Query(model.IsOnline).Where(u => u.SeriesID == model.SeriesID);
                #region 搜尋條件           
                if (!string.IsNullOrEmpty(model.Title))
                {
                    query = query.Where(d => d.Title.Contains(model.Title));
                }
                if (!string.IsNullOrEmpty(model.SiteType))
                {
                    var typedata = model.SiteType.Split(',');
                    for (int i = 0; i < typedata.Count(); i++)
                    {
                        var val = typedata[i];
                        query = query.Where(d => d.SiteType.Contains(val));
                    }
                }

                #endregion

                var pageResult = query.OrderBy(q => q.ID).ToPageResult<SeriesUsage>(model);
                model.PageResult = Mapper.Map<PageResult<UsageView>>(pageResult);

                #region 分類顯示文字
                if (model.PageResult.Data.Count() > 0)
                {
                    foreach (var item in model.PageResult.Data)
                    {
                        item.SeriesTypeStr = seriesRepository.GetById(item.SeriesID).SeriesName;
                    }
                }
                #endregion

                return View(model);
            }
            return RedirectToAction("Index", "SeriesAdmin");
        }

        public ActionResult Edit(int id = 0, int sid = 0)
        {
            UsageView model;
            if (id == 0)
            {
                model = new UsageView();
            }
            else
            {
                var query = usageRepository.GetById(id);
                model = Mapper.Map<UsageView>(query);
                sid = model.SeriesID;
            }
            model.SeriesID = sid;
            model.SeriesTypeStr = seriesRepository.GetById(sid).SeriesName;
            model.SiteOptions = GetUsageSite(sid);

            List<SelectListItem> result = new List<SelectListItem>();
            var colorList = productRepository.GetAll().Where(c => c.SeriesID == sid).ToList();
            if (colorList.Count() > 0)
            {
                foreach (var c in colorList)
                {
                    result.Add(new SelectListItem()
                     {
                         Text = c.Name,
                         Value = c.ID.ToString()
                     });
                }
            }
            model.ColorOptions = result;


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(UsageView model, HttpPostedFileBase pic_file)
        {
            bool hasFile = ImageHelper.CheckFileExists(pic_file);
            bool imageValid = !(model.ID == 0 && !hasFile);
            if (!imageValid)
            {
                ModelState.AddModelError(nameof(model.MainPic), "請上傳圖片");
            }

            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    model.MainPic = ImageHelper.SaveFile(pic_file, PhotoFolder);
                }

                SeriesUsage usagedata = Mapper.Map<SeriesUsage>(model);
                usagedata.UpdateDate = DateTime.Now;
                usagedata.Updater = 1;
                if (model.ID == 0)
                {
                    usagedata.CreateDate = DateTime.Now;
                    usagedata.Creater = 1;
                    model.ID = usageRepository.Insert(usagedata);
                }
                else
                {
                    usageRepository.Update(usagedata);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(UsageAdminController.Edit), new { id = model.ID });
            }
            model.SiteOptions = GetUsageSite(model.SeriesID);

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            SeriesUsage usagedata = usageRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, usagedata.MainPic);
            usageRepository.Delete(usagedata.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(CaseAdminController.Index));
        }

        public List<SelectListItem> GetUsageSite(int sid)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            var seriesUsageSite = seriesRepository.GetById(sid).UsageSite;

            if(seriesUsageSite != null)
            { 
            var usagetypedata = seriesUsageSite.Split(',');
            var values = caseSiteRepository.GetAll().Where(s => s.IsOnline == true).ToList();//???
            foreach (var v in values)
            {
                foreach (var t in usagetypedata)
                {
                    if (v.Name == t)
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = v.Name,
                            Value = v.ID.ToString()
                        });
                    }
                }
            }
        }
            return result;
        }
    }
}