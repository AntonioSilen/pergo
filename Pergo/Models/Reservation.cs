//------------------------------------------------------------------------------
// <auto-generated>
//    這個程式碼是由範本產生。
//
//    對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//    如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pergo.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Reservation
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Store { get; set; }
        public string Email { get; set; }
        public string InquiryContent { get; set; }
        public bool IsReply { get; set; }
        public System.DateTime ReservationTime { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
