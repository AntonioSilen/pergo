﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Models.Cmind
{
    public class PageResult<T> : Page
    {
        public IEnumerable<T> Data { get; set; }
    }
}