﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Models.Cmind
{
    public class AdminInfo
    {
        public int ID { get; set; }

        public string Account { get; set; }
    }
}