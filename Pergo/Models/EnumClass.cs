﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

/// <summary>
/// 專門放列舉值的Class
/// </summary>    
namespace Pergo.Models
{
    /// <summary>
    /// 1: 首頁-上方固定橫幅 2: 首頁-中間 3: 首頁-底部 4.系列分類-中間 5.系列分類-底部訂製生活
    /// </summary>
    public enum BannerType //橫幅區塊
    {
        /// <summary>
        /// 首頁-上方固定橫幅
        /// </summary>
        [Description("首頁-上方固定Banner")]
        TopFixed = 1,

        /// <summary>
        /// 首頁-中間
        /// </summary>
        [Description("首頁-中間")]
        CenterArea = 2,

        /// <summary>
        /// 首頁-底部
        /// </summary>
        [Description("首頁-底部")]
        BottomArea = 3,

        /// <summary>
        /// Laminate-上方固定Banner
        /// </summary>
        [Description("Laminate-上方固定Banner")]
        LaminateTypeTop = 4,
        
        /// <summary>
        /// Laminate-中間
        /// </summary>
        [Description("Laminate-中間")]
        LaminateTypeMid = 5,

        /// <summary>
        /// Laminate-底部訂製生活
        /// </summary>
        [Description("Laminate-底部訂製生活")]
        LaminateCustom = 6,

        /// <summary>
        /// Vinyl-上方固定Banner
        /// </summary>
        [Description("Vinyl-上方固定Banner")]
        VinylTypeTop = 7,

        /// <summary>
        /// Vinyl-中間
        /// </summary>
        [Description("Vinyl-中間")]
        VinylTypeMid = 8,

        /// <summary>
        /// Vinyl-底部訂製生活
        /// </summary>
        [Description("Vinyl-底部訂製生活")]
        VinylCustom = 9,
    }

    /// <summary>
    /// 1: 概覽 2: 功能特色 3: 專利技術 4.花色展示 
    /// </summary>
    public enum PageType //系列Banner頁面
    {
        /// <summary>
        /// 概覽
        /// </summary>
        [Description("概覽")]
        Overview = 1,

        /// <summary>
        /// 功能特色
        /// </summary>
        [Description("功能特色")]
        Features = 2,

        ///// <summary>
        ///// 專利技術
        ///// </summary>
        //[Description("專利技術")]
        //Tech = 3,

        ///// <summary>
        ///// 花色展示
        ///// </summary>
        //[Description("花色展示")]
        //Color = 4,

        /// <summary>
        /// 產品規格
        /// </summary>
        [Description("產品規格")]
        Tech = 5,
    }

    /// <summary>
    /// 1: Banner 2: 影片
    /// </summary>
    public enum SeriesBannerType //系列Banner類型
    {
        /// <summary>
        /// Banner
        /// </summary>
        [Description("Banner")]
        Banner = 1,

        /// <summary>
        /// 影片
        /// </summary>
        [Description("影片")]
        Video = 2,        
    }

    /// <summary>
    /// 1.左上 2.中上 3.右上 4.左中 5.中間 6.右中 7.左下 8.中下 9.右下
    /// </summary>
    public enum BannerTextPosition //Banner文字位置
    {
        /// <summary>
        /// 左上
        /// </summary>
        [Description("左上")]
        TopLeft = 1,

        /// <summary>
        /// 中上
        /// </summary>
        [Description("中上")]
        TopCenter = 2,

        /// <summary>
        /// 右上
        /// </summary>
        [Description("右上")]
        TopRight = 3,

        /// <summary>
        /// 左中
        /// </summary>
        [Description("左中")]
        MidLeft = 4,

        /// <summary>
        /// 中間
        /// </summary>
        [Description("中間")]
        MidCenter = 5,

        /// <summary>
        /// 右中
        /// </summary>
        [Description("右中")]
        MidRight = 6,

        /// <summary>
        /// 左下
        /// </summary>
        [Description("左下")]
        BtmLeft = 7,

        /// <summary>
        /// 中下
        /// </summary>
        [Description("中下")]
        BtmCenter = 8,

        /// <summary>
        /// 右下
        /// </summary>
        [Description("右下")]
        BtmRight = 9,
    }

    /// <summary>
    /// 1: Laminate  2: Vinyl 
    /// </summary>
    public enum BrandValue //品牌分類
    {
        /// <summary>
        /// 耐磨
        /// </summary>
        [Description("Laminate")]
        Laminate = 1,

        /// <summary>
        /// 防水
        /// </summary>
        [Description("Vinyl")]
        Vinyl = 2,
    }

    /// <summary>
    /// 1: 丈量   2: 門市 
    /// </summary>
    public enum ReservationType //預約類型
    {
        /// <summary>
        /// 聯絡
        /// </summary>
        [Description("聯絡")]
        Contact = 0, 
        
        /// <summary>
        /// 丈量
        /// </summary>
        [Description("丈量")]
        Measuring = 1,

        /// <summary>
        /// 門市
        /// </summary>
        [Description("門市")]
        Store = 2,
    }

    /// <summary>
    /// 1: 紋理   2: 亮澤   3: 顏色   4: 
    /// </summary>
    public enum SeriesItem //分類項目
    {
        /// <summary>
        /// 紋理
        /// </summary>
        [Description("紋理")]
        Texture = 1,

        /// <summary>
        /// 亮澤
        /// </summary>
        [Description("亮澤")]
        Luster = 2,

        /// <summary>
        /// 顏色
        /// </summary>
        [Description("顏色")]
        Color = 3,
    }

    /// <summary>
    /// 1: Living  2: Original  3: Public  4: Optimum  5: Premium
    /// </summary>
    public enum LevelType //等級
    {
        /// <summary>
        /// Living Expression
        /// </summary>
        [Description("Living Expression")]
        Living = 1,

        /// <summary>
        /// Original Excellence
        /// </summary>
        [Description("Original Excellence")]
        Original = 2,

        /// <summary>
        /// Public Extreme
        /// </summary>
        [Description("Public Extreme")]
        Public = 3,

        /// <summary>
        /// Optimum
        /// </summary>
        [Description("Optimum 卓越版")]
        Optimum = 4,

        /// <summary>
        /// Premium
        /// </summary>
        [Description("Premium 優適版")]
        Premium = 5,
    }

    /// <summary>
    /// 1: 四邊延伸倒角   2: 
    /// </summary>
    public enum Chamfer //倒角
    {
        /// <summary>
        /// 四邊延伸倒角
        /// </summary>
        [Description("四邊延伸倒角")]
        FourWay = 1,
        
    }

    /// <summary>
    /// 1: AquaSafe 防護塗料層   
    /// 2: TitanX™ 表面耐磨專利技術   
    /// 3: PerfectFold™ 3.0 鎖扣系統
    /// </summary>
    public enum Tech //工藝技術
    {
        /// <summary>
        /// AquaSafe 防護塗料層
        /// </summary>
        [Description("AquaSafe 防護塗料層")]
        AquaSafe = 1,

        /// <summary>
        /// TitanX™ 表面耐磨專利技術
        /// </summary>
        [Description("TitanX™ 表面耐磨專利技術")]
        TitanX = 2,

        /// <summary>
        /// PerfectFold™ 3.0 鎖扣系統
        /// </summary>
        [Description("PerfectFold™ 3.0 鎖扣系統")]
        PerfectFold = 3,
    }

    /// <summary>
    /// 1: Genuine™ rustic 粗糙   
    /// 2: Genuine™ wood 原木
    /// 3: Antique wood 仿古木
    /// </summary>
    public enum Texture //表層壓紋
    {
        /// <summary>
        /// Genuine™ rustic 粗糙
        /// </summary>
        [Description("Genuine™ rustic 粗糙")]
        GenuineRustic = 1,

        /// <summary>
        /// Genuine™ wood 原木
        /// </summary>
        [Description("Genuine™ wood 原木")]
        GenuineWood = 2,

        /// <summary>
        /// Antique wood 仿古木
        /// </summary>
        [Description("Antique wood 仿古木")]
        AntiqueWood = 3,
    }

    /// <summary>
    /// 1: SP 半拋光   
    /// 2: SM 絲質亞光   
    /// 3: MA 亞光
    /// 4: EM 超亞光
    /// </summary>
    public enum Luster //亮澤
    {
        /// <summary>
        /// SP 半拋光
        /// </summary>
        [Description("SP 半拋光")]
        SP = 1,

        /// <summary>
        /// SM 絲質亞光
        /// </summary>
        [Description("SM 絲質亞光")]
        SM = 2,

        /// <summary>
        /// MA 亞光
        /// </summary>
        [Description("MA 亞光")]
        MA = 3,

        /// <summary>
        /// EM 超亞光
        /// </summary>
        [Description("EM 超亞光")]
        EM = 4,
    }

    /// <summary>
    /// 1: 長度   2: 厚度   3: 密度   4: 倒角   5: 工藝技術
    /// 6: 包裝   7: 基材標準   8: 表層壓紋   9: 亮澤   10: 耐熱性
    /// 11: 鎖扣   12: 耐磨等級   13: 使用等級 
    /// </summary>
    public enum Field
    {
        /// <summary>
        /// 長度
        /// </summary>
        [Description("長度")]
        Length = 1,

        /// <summary>
        /// 厚度
        /// </summary>
        [Description("厚度")]
        Thickness = 2,

        /// <summary>
        /// 密度
        /// </summary>
        [Description("密度")]
        Density = 3,

        /// <summary>
        /// 倒角
        /// </summary>
        [Description("倒角")]
        Chamfer = 4,

        /// <summary>
        /// 工藝技術
        /// </summary>
        [Description("工藝技術")]
        Tech = 5,

        /// <summary>
        /// 包裝
        /// </summary>
        [Description("包裝")]
        Package = 6,

        /// <summary>
        /// 基材標準
        /// </summary>
        [Description("基材標準")]
        Standard = 7,

        /// <summary>
        /// 表層壓紋
        /// </summary>
        [Description("表層壓紋")]
        Texture = 8,

        /// <summary>
        /// 亮澤
        /// </summary>
        [Description("亮澤")]
        Luster = 9,

        /// <summary>
        /// 耐熱性
        /// </summary>
        [Description("耐熱性")]
        HeatResistance = 10,

        /// <summary>
        /// 鎖扣
        /// </summary>
        [Description("鎖扣")]
        Lock = 11,

        /// <summary>
        /// 耐磨等級
        /// </summary>
        [Description("耐磨等級")]
        WearRating = 12,

        /// <summary>
        /// 使用等級
        /// </summary>
        [Description("使用等級")]
        UseLevel = 13,
    }

    /// <summary>
    /// 1: GR  2: GW  3: AW
    /// </summary>
    public enum ColorTexture //花色壓紋
    {
        /// <summary>
        /// GR
        /// </summary>
        [Description("GR Genuine™ 粗糙")]
        GR = 1,

        /// <summary>
        /// GW
        /// </summary>
        [Description("GW Genuine™ 原木")]
        GW = 2,

        /// <summary>
        /// AW
        /// </summary>
        [Description("AW 仿古木")]
        AW = 3,

        /// <summary>
        /// GS
        /// </summary>
        [Description("GS Genuine™ 石頭")]
        GS = 4,

        /// <summary>
        /// SO
        /// </summary>
        [Description("SO 光滑")]
        SO = 5,

        /// <summary>
        /// WO
        /// </summary>
        [Description("WO 木製")]
        WO = 6,

        /// <summary>
        /// GC
        /// </summary>
        [Description("GC Genuine™ 鋸痕")]
        GC = 7,        
    }

    /// <summary>
    /// 1: GR  2: GW  3: AW
    /// </summary>
    public enum ColorTextureWeb //花色壓紋
    {
        /// <summary>
        /// GR
        /// </summary>
        [Description("GR")]
        GR = 1,

        /// <summary>
        /// GW
        /// </summary>
        [Description("GW")]
        GW = 2,

        /// <summary>
        /// AW
        /// </summary>
        [Description("AW")]
        AW = 3,

        /// <summary>
        /// GS
        /// </summary>
        [Description("GS")]
        GS = 4,

        /// <summary>
        /// SO
        /// </summary>
        [Description("SO")]
        SO = 5,

        /// <summary>
        /// WO
        /// </summary>
        [Description("WO")]
        WO = 6,

        /// <summary>
        /// GC
        /// </summary>
        [Description("GC")]
        GC = 7,
    }

    /// <summary>
    /// 1: 光澤1  2: 光澤2  3: 光澤3  4: 光澤4  5: 光澤5 
    /// </summary>
    public enum ColorLuster //花色光澤
    {
        /// <summary>
        /// 光澤1
        /// </summary>
        [Description("光澤1")]
        LusterOne = 1,

        /// <summary>
        /// 光澤2
        /// </summary>
        [Description("光澤2")]
        LusterTwo = 2,

        /// <summary>
        /// 光澤3
        /// </summary>
        [Description("光澤3")]
        LusterThree = 3,

        /// <summary>
        /// 光澤4
        /// </summary>
        [Description("光澤4")]
        LusterFour = 4,

        /// <summary>
        /// 光澤5
        /// </summary>
        [Description("光澤5")]
        LusterFive = 5,
    }

    /// <summary>
    /// 1: 淺色系  2: 深色系  3: 冷色調  4: 暖色調  5: 灰色系 
    /// </summary>
    public enum ColorColor //花色顏色
    {
        /// <summary>
        /// 淺色系
        /// </summary>
        [Description("淺色系")]
        Light = 1,

        /// <summary>
        /// 深色系
        /// </summary>
        [Description("深色系")]
        Dark = 2,

        /// <summary>
        /// 冷色調
        /// </summary>
        [Description("冷色調")]
        Cold = 3,

        /// <summary>
        /// 暖色調
        /// </summary>
        [Description("暖色調")]
        Warm = 4,

        /// <summary>
        /// 灰色系
        /// </summary>
        [Description("灰色系")]
        Gray = 5,

        /// <summary>
        /// 原木色系
        /// </summary>
        [Description("原木色系")]
        Wood = 6,
    }

    /// <summary>
    /// 1: 鄉村風  2: 復古風  3: 北歐風  4: ⼯業風  5: 無印風 
    /// </summary>
    public enum Shade //花色深淺or風格
    {
        /// <summary>
        /// 鄉村風
        /// </summary>
        [Description("鄉村風")]
        CountryStyle = 1,

        /// <summary>
        /// 復古風
        /// </summary>
        [Description("復古風")]
        Classical = 2,

        /// <summary>
        /// 北歐風
        /// </summary>
        [Description("北歐風")]
        NordicStyle = 3,

        /// <summary>
        /// 工業風
        /// </summary>
        [Description("工業風")]
        IndustrialStyle = 4,

        /// <summary>
        /// 無印風
        /// </summary>
        [Description("無印風")]
        UnprintedStyle = 5,
    }

    /// <summary>
    /// 1: 產品目錄  2: 技術手冊 
    /// </summary>
    public enum DocumentationType //文件類型
    {
        /// <summary>
        /// 產品目錄
        /// </summary>
        [Description("產品目錄")]
        ProductCatalog = 1,

        /// <summary>
        /// 技術手冊
        /// </summary>
        [Description("技術手冊")]
        TechnicalManual = 2,        
    }
}