﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class AdminRepository
    {
        private PergoDBEntities db;

        public AdminRepository() : this(null) { }

        public AdminRepository(PergoDBEntities context)
        {
            db = context ?? new PergoDBEntities();
        }

        public Admin Login(string account, string password)
        {
            Admin admin = db.Admin.Where(p => p.Account == account && p.Password == password && p.Status).FirstOrDefault();
            return admin;
        }
    }
}