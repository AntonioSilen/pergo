﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class DocumentationRepository : GenericRepository<Documentation>
    {
        public DocumentationRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<Documentation> Query(bool? status)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            return query;
        }
    }
}