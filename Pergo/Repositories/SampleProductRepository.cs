﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class SampleProductRepository : GenericRepository<SampleProduct>
    {
        public SampleProductRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<SampleProduct> Query()
        {
            var query = GetAll();

            return query;
        }
    }
}