﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class SeriesBannerRepository : GenericRepository<SeriesBanner>
    {
        public SeriesBannerRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<SeriesBanner> Query(bool? status, int sid = 0, int type = 0)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (sid != 0)
            {
                query = query.Where(p => p.SeriesID == sid);
            }
            if (type != 0)
            {
                query = query.Where(p => p.Type == type);
            }
            return query;
        }
    }
}