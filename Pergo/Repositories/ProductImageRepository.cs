﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class ProductImageRepository : GenericRepository<ProductImage>
    {
        public ProductImageRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<ProductImage> Query(bool? status, int pid)
        {
            int statusInt = (status != null && status != false) ? 1 : 0;        
            var query = GetAll().Where(i => i.ProductID == pid);
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == statusInt);
            }
            return query;
        }
    }
}