﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class AccessoriesUsageRepository : GenericRepository<AccessoriesUsage>
    {
        public AccessoriesUsageRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<AccessoriesUsage> Query(bool? status, int aid, string title)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (aid != 0)
            {
                query = query.Where(p => p.AccessoriesID == aid);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            return query;
        }
    }
}