﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class SEORepository : GenericRepository<SEO>
    {
        public SEORepository(PergoDBEntities context) : base(context) { }

        public IQueryable<SEO> Query(string controller = "", string action = "", string parameter = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(controller))
            {
                query = query.Where(q => q.Controller == controller);
            }
            if (!string.IsNullOrEmpty(action))
            {
                query = query.Where(q => q.Action == action);
            }
            if (!string.IsNullOrEmpty(parameter))
            {
                query = query.Where(q => q.Parameter == parameter);
            }

            return query;
        }
    }
}