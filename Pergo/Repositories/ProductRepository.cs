﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class ProductRepository : GenericRepository<Product>
    {
        public ProductRepository(PergoDBEntities context) : base(context) { }
        private PergoDBEntities db = new PergoDBEntities();

        public IQueryable<Product> Query(bool? status)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            return query;
        }

        public int? UpdateViews(int id)
        {
            try
            {
                var query = db.Product.Where(p => p.ID == id).FirstOrDefault();
                query.Views = query.Views == null ? 1 : query.Views +1;

                db.SaveChanges();

                return query.Views;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}