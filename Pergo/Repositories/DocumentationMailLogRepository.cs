﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class DocumentationMailLogRepository : GenericRepository<DocumentationMailLog>
    {
        public DocumentationMailLogRepository(PergoDBEntities context) : base(context) { }
        DocumentationRepository documentationRepository = new DocumentationRepository(new PergoDBEntities());

        public IQueryable<DocumentationMailLog> Query(int docId = 0, string searchTitle = "", string email = "")
        {
            var query = GetAll();
            if (docId != 0)
            {
                query = query.Where(q => q.DocID == docId);
            }
            if (!string.IsNullOrEmpty(searchTitle))
            {
                var docQuery = documentationRepository.GetAll().Where(q => q.Title.Contains(searchTitle)).Select(q => q.ID).ToList();
                query = query.Where(q => docQuery.Contains(q.DocID));
            }
            if (!string.IsNullOrEmpty(email))
            {
                query = query.Where(q => q.Email == email);
            }
            return query;
        }
    }
}