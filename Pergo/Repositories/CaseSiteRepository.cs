﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class CaseSiteRepository : GenericRepository<CaseSite>
    {
        public CaseSiteRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<CaseSite> Query(bool? status, string searchName)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                query = query.Where(p => p.Name.Contains(searchName));
            }
            return query;
        }
    }
}