﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class SeriesRepository : GenericRepository<ProductSeries>
    {
        public SeriesRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<ProductSeries> Query(bool? status, int brand, string seriesName)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (brand != 0)
            {
                query = query.Where(p => p.Brand == brand);
            }
            if (!string.IsNullOrEmpty(seriesName))
            {
                query = query.Where(p => p.SeriesName.Contains(seriesName) || p.SeriesEngName.Contains(seriesName));
            }
            return query;
        }
    }
}