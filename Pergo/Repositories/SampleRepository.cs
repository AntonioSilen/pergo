﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class SampleRepository : GenericRepository<Sample>
    {
        public SampleRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<Sample> Query(bool? status)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsReply == status);
            }
            return query;
        }
    }
}