﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class ItemTypeRepository : GenericRepository<ItemType>
    {
        public ItemTypeRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<ItemType> Query(bool? status)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            return query;
        }
    }
}