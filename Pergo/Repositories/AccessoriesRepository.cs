﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class AccessoriesRepository : GenericRepository<Accessories>
    {
        public AccessoriesRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<Accessories> Query(bool? status, int brand, string title)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (brand != 0)
            {
                query = query.Where(p => p.Brand == brand);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            return query;
        }

        public PergoDBEntities db = new PergoDBEntities();

        public IQueryable<Object> GetJoin()
        {
            var Lambdajoin = db.AccessoriesUsage.Join(db.Accessories, //第一個參數為 要加入的資料來源
            u => u.AccessoriesID,//主表要join的值
            a => a.ID,//次表要join的值
            (u, a) => new  // (c,s)代表將資料集合起來
            {
                ID = u.ID,
                AccessoriesID = u.AccessoriesID,
                MainPic = u.MainPic,
                IsOnline = a.IsOnline,
            }).OrderBy(ua => ua.AccessoriesID).Where(ua => ua.IsOnline == true);

            return Lambdajoin;
        }
    }
}