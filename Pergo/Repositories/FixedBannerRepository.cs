﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class FixedBannerRepository : GenericRepository<FixedBanner>
    {
        public FixedBannerRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<FixedBanner> Query(bool? status, int type)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (type != 0)
            {
                query = query.Where(p => p.Type == type);
            }
            return query;
        }
    }
}