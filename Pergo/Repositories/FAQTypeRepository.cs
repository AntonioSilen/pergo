﻿using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.Repositories
{
    public class FAQTypeRepository : GenericRepository<FAQType>
    {
        public FAQTypeRepository(PergoDBEntities context) : base(context) { }

        public IQueryable<FAQType> Query(bool? status, string value = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(value))
            {
                query = query.Where(p => p.Name.Contains(value));
            }
            return query;
        }
    }
}