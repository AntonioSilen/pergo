"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CustomizedSelect =
/*#__PURE__*/
function () {
  function CustomizedSelect(select) {
    _classCallCheck(this, CustomizedSelect);

    this.select = select;
    this.createSelect(this.select);
  }

_createClass(CustomizedSelect, [{
    key: "createSelect",
    value: function createSelect(select) {
        this.holder = select.attr("placeholder").split(" ");
        this.isExpression = this.holder.indexOf("Expression") > 0;
        this.isOptimum = this.holder.indexOf("Optimum") > 0;

        var _this = this;

        var tpl = "<div class=".concat(select.attr("class"), ">");
        tpl += this.isExpression || this.isOptimum ? "<div class=cmz-sel-trig>".concat(this.holder[0] || "", "\n          <span class=\"font-weight-bold text-").concat(this.isExpression ? 'leaf' : 'gold', "\"> ").concat(this.holder[1], "</span>\n        </div>") : "<span class=cmz-sel-trig>".concat(this.holder, "</span>");
        tpl += "<div class=cmz-opts>";
        select.find("option").each(function () {
            tpl += _this.createOptions($(this));
        });
        select.wrap("<div class=cmz-sel-wrapper></div>");
        select.after("".concat(tpl, "</div></div>"));
        this.createThis();
    }
}
      , {
    key: "createOptions",
    value: function createOptions(option) {
      var text = option.text().split(" ");
      var outer = "<div class=cmz-opt data-value=".concat(option.attr("value"), ">").concat(text[0], "\n                  <span class=\"font-weight-bold text-");
      var inner = '">' + " " + text[1] + "</span></div>";
      return text.indexOf("Excellence") > 0 ? "".concat(outer, "wood").concat(inner) : text.indexOf("Expression") > 0 ? "".concat(outer, "leaf").concat(inner) : text.indexOf("Extreme") > 0 ? "".concat(outer, "dark").concat(inner) : text.indexOf("Optimum") > 0 ? "".concat(outer, "gold").concat(inner) : text.indexOf("Premium") > 0 ? "".concat(outer, "silver").concat(inner) : "<div class=cmz-opt\n              data-value=".concat(option.attr("value"), ">\n              ").concat(option.html(), "</div>");
    }
  }, {
    key: "trigger",
    value: function trigger(select) {
      select.on("click", function (event) {
        event.stopPropagation();
        $("html").one("click", function (_) {
          return select.find(".cmz-sel").removeClass("opened");
        });
        var trig = select.find(".cmz-sel-trig");
        var allTrig = $(".cmz-sel-trig");
        var index = allTrig.index(trig);
        allTrig.each(function () {
          var parent = $(this).parent();
          allTrig.index($(this)) === index ? parent.toggleClass("opened") : parent.removeClass("opened");
        });
      });
    }
  }, {
    key: "selected",
    value: function selected(option) {
      option.on("click", function (event) {
        event.stopPropagation();
        $(this).parents(".cmz-sel-wrapper").find("select").val($(this).data("value")).change();
        $(this).addClass("selection").siblings().removeClass("selection").parents(".cmz-sel").removeClass("opened").find(".cmz-sel-trig").html($(this).html());
      });
    }
  }, {
    key: "createThis",
    value: function createThis() {
      this.trigger(this.select.parent());
      this.selected(this.select.next().find(".cmz-opt"));
    }
  }]);

  return CustomizedSelect;
}();
"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

$(function () {
  var getExplorer = function () {
    var agent = window.navigator.userAgent;

    var compare = function compare(browser) {
      return agent.indexOf(browser) >= 0;
    };

    var ie11 = "ActiveXObject" in window;
    return compare("MSIE") || ie11 ? "ie" : compare("Firefox") && !ie11 ? "Firefox" : compare("Chrome") && !ie11 ? "Chrome" : compare("Safari") && !ie11 ? "Safari" : compare("Opera") && !ie11 ? "Opera" : "other";
  }();

  var headerH = Math.floor($("header").height());
  console.log(headerH);
  var windowScroll = 0;

  function collapseBlock(collapse, controller) {
    return $(collapse).collapse(controller).find(".triangle").removeClass(["tri-left", "tri-center", "tri-right"]);
  }

  $(".navbar-toggler").on("click", function () {
    $("#pad-category-series").prop("checked", true); // $(".mobile-menu-buttom").addClass("invisible");

    if ($(window).height() < 630) {// $(".menu-series").addClass("invisible");
    }
  });
  $("#navbarSupportedContent").on("hide.bs.collapse", function () {
    $("#pad-category-series").prop("checked", false); // $(".mobile-menu-buttom,.menu-series").removeClass("invisible");
  });

  if (getExplorer === "ie") {
    var style = '<link rel="stylesheet" href="./css/IE.css">';
    $("head").append(style);
  }

  $(window).on("load", function () {
    if (window.location.search.split("cid").length > 1) {
      var cid = window.location.search.split("cid=")[1];
      var productID = "#product-" + cid.split("-")[1];
      $(productID).click();

      if (cid.split("-")[0] === "2") {
        $(".infoCollapse").on("shown.bs.collapse", function () {
          $(this).find(".tab-1").click();
        });
      }
    }

    if (windowScroll < headerH) {
      $(".sub-nav").removeClass("fixed-top");
    } else {
      $(".sub-nav").addClass("fixed-top");
    } // $(".mt-header").css("margin-top", $("header").height());

  });
  $(window).on("click", function () {
    $(".accessory,.nav-form,.searchbarParent").removeClass("active");
    $('[type="search"]').val("");
  });
  $(window).on("scroll", function () {
    $("#pad-category-series").prop("checked", false);

    if ($(window).width() < 1080) {
      if (windowScroll < 100) {
        $(".pad-category-btn").removeClass("fixed-top");
        $("#pad-category-series").removeClass("scrolled"); //     $(".mobile-menu-buttom").addClass(["active"]);
        //     $("header").removeClass("d-none");
      } else {
        $(".pad-category-btn").addClass("fixed-top");
        $("#pad-category-series").addClass("scrolled"); //     $(".mobile-menu-buttom").removeClass(["active"]);
        //     $("header").addClass("d-none");
      }
    }
    windowScroll = $(this).scrollTop();
      console.clear();
      console.log(windowScroll, headerH);
      if (windowScroll < headerH + 10) {
          console.log(true, windowScroll, headerH);
      $(".sub-nav").removeClass("fixed-top");
      } else {
          console.log(false, windowScroll, headerH)
      $(".sub-nav").addClass("fixed-top");
    }

    $(".navbar-collapse").collapse("hide");
  });
  $(".navbar-collapse").on("hidden.bs.collapse", function () {
    $(".category-series,.pad-category-btn").removeClass("d-none");
  });
  $(window).on("resize", function () {
    $(".collapse.show").collapse("hide");
    $(".showProductInfo").find(".active").removeClass("active");
    $(".info").removeClass("active");

    if (windowScroll < 100) {
      $(".pad-category-btn").removeClass("fixed-top");
    } else {
      $(".pad-category-btn").addClass("fixed-top");
    }
  }); // header

  $(".nav-form-close").on("click", function (e) {
    e.preventDefault();
    $(this).parent().removeClass("active");
    $('[type="search"]').val("");
  });
  $("#showInput").on("click", function (e) {
    e.stopPropagation();
    e.preventDefault();
    $(".nav-form").addClass("active");
  });
  $(".nav-form").on("click", function (e) {
    e.stopPropagation();
  });
  $("#carousel-prev,#carousel-next").on("mouseover", function (e) {
    var carousel_ctr = $(this).attr("id").slice(-4);
    $("#carousel").carousel(carousel_ctr);
  });
  $(".navbar-toggler,.subNav").on("click", function () {
    $(".navbar-collapse.collapse").collapse("hide");
    $(".pad-category-btn,.category-series").addClass("d-none");
  }); // $(".accessory").on("click", function(e) {
  //   e.stopPropagation();
  //   let collapse = $(this)
  //     .parents("section")
  //     .next();
  //   $(this)
  //     .toggleClass("active")
  //     .parent()
  //     .siblings()
  //     .find(".accessory")
  //     .removeClass("active");
  //   if (
  //     collapse.hasClass("show") &&
  //     collapse.find(".triangle").hasClass($(this).data("triangle"))
  //   ) {
  //     collapseBlock(collapse, "hide");
  //   } else {
  //     collapseBlock(collapse, "show").addClass($(this).data("triangle"));
  //   }
  // });

  $(".bg-accessory-collapse").on("show.bs.collapse", function () {
    $(".bg-accessory-collapse.show").collapse("hide");
  }); //* comparison寫進 .net*//
  // let compareItems = []
  // $('#showTable').on('click', function(e) {
  //   if (compareItems.length < 2) {
  //     e.preventDefault()
  //   }
  //   alert(compareItems)
  // })
  // $('input[name="compareSample"]').on('change', function() {
  //   let exist = compareItems.indexOf($(this).val()) === -1 ? false : true
  //   let arrayIndex = compareItems.indexOf($(this).val())
  //   //確認比較項目以點選 ? 刪除 : (新增條件只能兩項 ? 刪除陣列[0] → 新增 : 直接新增)
  //   if (exist) {
  //     compareItems.splice(arrayIndex, 1)
  //   } else {
  //     if (compareItems.length === 2) {
  //       $(`input[value=${compareItems[0]}]`).prop('checked', false)
  //       compareItems.splice(0, 1)
  //     }
  //     compareItems.push($(this).val())
  //     console.log(compareItems)
  //   }
  // })

  $(".heart-s,.heart-o").on("click", function (e) {
    e.stopPropagation();
    e.preventDefault();
    var icon = $(this).hasClass("heart-o") ? $(this).next() : $(this).prev();
    $(this).toggleClass("d-none");
    icon.toggleClass("d-none");
  });
  $(".add-fav").on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).toggleClass("active");
  }); //comparisonTable

  function modalCarousel(trigger, _this, img) {
    var name = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
    var index = $(trigger).index($(_this));
    var imgStr = "";
    $(trigger).each(function (i, e) {
      imgStr += "<div class=\"carousel-item text-center ".concat(i == index ? "active" : "", "\" >\n        <a class=\"goto\" href=").concat($(this).data("href") || "#", "> <img src=").concat($(this).data("img"), " class=\"img-fluid\"/></a><br>\n          <span class=\"font-xs text-white\">").concat($(this).data("name"), "</span>\n          </div>");
    });
    $(".modal .carousel-inner").html(imgStr);
  }

  $(".show-left,.show-right").on("click", function () {
    var cls = ".show-";
    cls += +$(this).is(".show-left") ? "left" : "right";
    modalCarousel(cls, this, $(this).data("img"), $(this).data("name"));
  }); //usage

  $(".usage-img").on("click", function () {
    modalCarousel(".usage-img", this, $(this).data("img"), $(this).data("name"));
  });
  var filter = $(".filterd-list").isotope({
    itemSelector: ".el-item" // layoutMode: 'fitRows'

  });
  $(".filter-buttons").on("click", "a", function () {
    filter.isotope({
      filter: $(this).attr("data-filter")
    });
    $(".filter-buttons").find("a").removeClass("active");
    $(this).addClass("active");
  }); //support

  $(".seachbar").on("click", function (e) {
    e.stopPropagation();
  });
  $(".seachbar").on("focus", function (e) {
    $(this).parents(".searchbarParent").addClass("active");
  }); // $(".seachbar").on("blur", function() {
  //   $(this)
  //     .parents(".searchbarParent")
  //     .removeClass("active");
  // });

  $(".getDocument").on("change", function () {
    var checkboxex = _toConsumableArray($(".getDocument"));

    var checked = checkboxex.some(function (e) {
      return $(e).prop("checked");
    });
    $(this).parents("section")[(checked ? "add" : "remove") + "Class"]("active");
  });
  $(".cmz-sel").each(function () {
    new CustomizedSelect($(this));
  }); //*woe.js

  var wow = new WOW({
    boxClass: "wow",
    // default
    // animateClass: 'animated', // default
    offset: 50,
    // default
    mobile: false,
    // default
    live: true // default

  });
  wow.init();
});
"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

$(function () {
  var urlval;
  var planks;
  var totalCollapse;

  if (location.pathname.indexOf("/Color") > -1) {
    urlval = getUrlParameter("sid");
    planks = [];
    totalCollapse = planks.length;
    $.ajax({
        type: "POST",
        url: location.origin + "/Series/GetColorList",
      async: false,
      data: {
        sid: urlval
      },
      dataType: "json",
      success: function success(data) {
        planks = data;
        totalCollapse = planks.length;
        $(".display-planks").append(appendPlanks(planks));
        evtOn();
      }
    });
  } // $(".display-planks").append(appendPlanks(planks));
  // evtOn();


  function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split("&");
    var sParameterName;

    for (var i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split("=");

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
    }
  }

  function evtOn() {
    $(".heart-s,.heart-o").on("click", function (e) {
      e.stopPropagation();
      e.preventDefault();
      var icon = $(this).hasClass("heart-o") ? $(this).next() : $(this).prev();
      $(this).toggleClass("d-none");
      icon.toggleClass("d-none");
    });
    $(".add-fav").on("click", function (e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).toggleClass("active");
    });
    $(".shareLinks").on("click", function (e) {
      e.preventDefault();
      $(this).toggleClass("active").parents(".collapse-link").siblings(".social-link-icons").toggleClass("active");
    });
    var lastRow = "";
    var lastItem = "";
    var lastBlock = "";
    //$(".showProductInfo").on("click", function () {
    //  var isLg = $(window).width() < 768;
    //  var breakpoint = isLg ? "lgcol" : "xlcol";
    //  var index = $(".showProductInfo").index($(this));
    //  var mod = isLg ? index % 2 : index % 4;
    //  var assignID = isLg ? Math.floor(index / 2) : Math.floor(index / 4) * 2 + 1;
    //  var targetBlock = "#block-" + assignID;
    //  var activeRow = $(this).data(breakpoint);
    //  var dataset = $(this).data();
    //  var slideGroup = $(this).find(".slide-group");
    //  var info = $(this).next();
    //  var showMark = $(this).find(".show-mark");

    //  if (slideGroup.hasClass("active")) {
    //    slideGroup.toggleClass("active");
    //    info.toggleClass("active");
    //    showMark.toggleClass("active");
    //  } else {
    //    $(".slide-group.active,.show-mark.active").removeClass("active");
    //    $(".showProductInfo").next().removeClass("active");
    //    slideGroup.addClass("active");
    //    info.addClass("active");
    //    showMark.addClass("active");
    //  }

    //  if (activeRow === lastRow || lastRow === "") {
    //    index === lastItem ? $(lastBlock).collapse("toggle") : $(targetBlock).collapse("show");
    //  } else {
    //    $(lastBlock).collapse("hide");
    //    $(targetBlock).collapse("show");
    //  }

    //  sendData(targetBlock, dataset, assignID);
    //  $(targetBlock).removeClass(["tri-lg-left", "tri-lg-right", "tri-xl-first", "tri-xl-second", "tri-xl-third", "tri-xl-forth"]);
    //  var triangle = mod ? "tri-lg-right" : "tri-lg-left";

    //  if (!isLg) {
    //    switch (mod) {
    //      case 0:
    //        triangle = "tri-xl-first";
    //        break;

    //      case 1:
    //        triangle = "tri-xl-second";
    //        break;

    //      case 2:
    //        triangle = "tri-xl-third";
    //        break;

    //      case 3:
    //        triangle = "tri-xl-forth";
    //        break;
    //    }
    //  }

    //  $(targetBlock).addClass(triangle);
    //  lastRow = activeRow;
    //  lastItem = index;
    //  lastBlock = targetBlock;
    //});

    $(".showProductInfo").on("click", function () {
        let isLg = $(window).width() < 768;
        let breakpoint = isLg ? "lgcol" : "xlcol";
        let index = $(".showProductInfo").index($(this));
        let mod = isLg ? index % 2 : index % 4;
        let assignID = isLg
            ? Math.floor(index / 2)
            : Math.floor(index / 4) * 2 + 1;
        let targetBlock = "#block-" + assignID;
        let activeRow = $(this).data(breakpoint);
        let dataset = $(this).data();
        
        if (activeRow === lastRow || lastRow === "") {
            index === lastItem
                ? $(lastBlock).collapse("toggle")
                : $(targetBlock).collapse("show");
        } else {
            $(lastBlock).collapse("hide");
            $(targetBlock).collapse("show");
        }
        $(".showProductInfo").each(function () {
            let slideGroup = $(this).find(".slide-group");
            let showMark = $(this).find(".show-mark");
            let isActRow = $(this).data(breakpoint) !== activeRow;
            let eachIndex = $(".showProductInfo").index($(this)); //!初次點選
            let info = $(this).next();

            if (lastRow === "") {
                isActRow || slideGroup.addClass("active");
                isActRow || info.addClass("active");
                eachIndex !== index || showMark.addClass("active"); //!再次點選與上次同行
            } else if (activeRow === lastRow) {
                //*同行同物件
                if (index === lastItem) {
                    isActRow || slideGroup.toggleClass("active");
                    isActRow || info.toggleClass("active");
                    eachIndex !== index || showMark.toggleClass("active");
                } else {
                    //*同行不同物件
                    isActRow || slideGroup.addClass("active"); //每項 index 比對後與觸發 index 相同
                    isActRow || info.addClass("active");
                    eachIndex === index
                        ? showMark.addClass("active")
                        : showMark.removeClass("active");
                } //!再次點選與上次不同行
            } else {
                $(this).data(breakpoint) === activeRow
                    ? slideGroup.addClass("active")
                    : slideGroup.removeClass("active"); //每項 index 比對後與觸發 index 相同
                $(this).data(breakpoint) === activeRow
                    ? info.addClass("active")
                    : info.removeClass("active"); //每項 index 比對後與觸發 index 相同

                eachIndex === index
                    ? showMark.addClass("active")
                    : showMark.removeClass("active");
            }
        });

        sendData(targetBlock, dataset, assignID);
        $(targetBlock).removeClass([
            "tri-lg-left",
            "tri-lg-right",
            "tri-xl-first",
            "tri-xl-second",
            "tri-xl-third",
            "tri-xl-forth"
        ]);
        let triangle = mod ? "tri-lg-right" : "tri-lg-left";
        if (!isLg) {
            switch (mod) {
                case 0:
                    triangle = "tri-xl-first";
                    break;
                case 1:
                    triangle = "tri-xl-second";
                    break;
                case 2:
                    triangle = "tri-xl-third";
                    break;
                case 3:
                    triangle = "tri-xl-forth";
                    break;
            }
        }
        $(targetBlock).addClass(triangle);
        lastRow = activeRow;
        lastItem = index;
        lastBlock = targetBlock;
    });

    $(".infoCollapse").on("shown.bs.collapse", function () {
      $("html,body").animate({
        scrollTop: $(this).offset().top - $(window).height() / 5
      }, 1000);
    });
    $(".cart").on("click", function (e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).toggleClass("active");
    });
    $(".content-group").find(".content:not(:first-child)").slideUp();
    $(".tab-radio").on("change", function () {
      var contentGroup = $(this).siblings(".content-group");
      contentGroup.find($(this).val()).slideDown().delay(300).siblings(".content").slideUp();
    });
    $(".clickBoard").on("click", function () {
      event.preventDefault();
      var temp = $('<input type="text"/>');
      $("body").append(temp);
      temp.val($(this).attr("href")).select();
      document.execCommand("copy");
      temp.remove();
      alert($(this).attr("href") + "複製成功");
    });
  }

  function sendData(targetBlock, dataset, assignTo) {
    $(targetBlock).find(".plankname").text(dataset.name);
    $(targetBlock).find(".clickBoard").attr("href", "".concat(window.location.host).concat(window.location.pathname, "?sid=").concat(urlval, "&cid=1-").concat(dataset.ndx));
    $(targetBlock).find(".plankpn").text(dataset.series + "｜" + dataset.pn);
    $(targetBlock).find(".concept").text(dataset.concept);
    var content2 = "";
    var specs = dataset.spec.split(",");

    for (var i = 0; i < Math.ceil(specs.length / 2); i++) {
      content2 += "<tr>";

      for (var j = 0; j < 2; j++) {
        content2 += "<td>".concat(specs[i * 2 + j], "\n        </td>");
      }

      content2 += "</tr>";
    }

    $(targetBlock).find(".content2").html(content2);
    var levelselection = dataset.levelselection.split(",");
    var content3 = " <tr >\n    <td class=\"px-0\" colspan=\"2\">\n      <select class=\"cmz-sel\" name=\"level\" id=\"level".concat(assignTo, "\" placeholder=\"").concat(levelselection[0], "\">");

    for (var _i = 0; _i < levelselection.length; _i++) {
      content3 += "<option value=\"".concat(_i + 1, "\" selected=").concat(_i === 0, ">").concat(levelselection[_i], "</option>");
    }

    content3 += "</select>\n    </td>\n  </tr>";

    for (var k = 1; k < 6; k++) {
      var level = dataset["level" + k].split(",");

      if (level.length > 2) {
        for (var _i2 = 0; _i2 < Math.ceil(level.length / 2); _i2++) {
          content3 += "<tr class = \"levelcontent level".concat(k).concat(k > 1 ? " d-none" : "", "\">");

          for (var _j = 0; _j < 2; _j++) {
            if (level[_i2 * 2 + _j]) {
              content3 += "<td>".concat(level[_i2 * 2 + _j], "</td>");
            }
          }

          content3 += "</tr>";
        }
      }
    }

    $(targetBlock).find(".content3").html(content3);
    new CustomizedSelect($("#level".concat(assignTo)));
    $("#level".concat(assignTo)).on("change", function () {
      $(".levelcontent").addClass("d-none");
      $(".levelcontent.level".concat($(this).val())).removeClass("d-none");
    });
    var sliderImg = "";
    var indicatorsImg = "";
    var allImg = dataset.img.split(",");

    for (var _i3 = 0; _i3 < allImg.length; _i3++) {
      sliderImg += "<div class=\"carousel-item ".concat(_i3 === 0 ? "active" : "", "\">\n                    <div class=\"bg-banner mb-4\"\n                        style=\"background-image:url(").concat(dataset.imgpath, "/").concat(allImg[_i3], ")\"></div>\n                    </div>");
    }

    for (var _i4 = 0; _i4 < Math.ceil(allImg.length / 5); _i4++) {
      indicatorsImg += "<div class=\"carousel-item ".concat(_i4 === 0 ? "active" : "", "\">\n                                    <ul class=\"carousel-img-indicators\">");

      for (var _j2 = 0; _j2 < 5; _j2++) {
        if (_i4 * 5 + _j2 < allImg.length) {
          indicatorsImg += "<li>\n                                <div class=\"bg-square active\"\n                                      data-target=\"#carousel".concat(assignTo, "\"\n                                      data-slide-to=\"").concat(_i4 * 5 + _j2, "\"\n                                      style=\"background-image:url(").concat(dataset.imgpath).concat(allImg[_i4 * 5 + _j2], ")\"></div>\n                              </li>");
        }
      }

      indicatorsImg += "</ul></div>";
    }

    $(targetBlock).find("#carousel" + assignTo).find(".carousel-inner").html(sliderImg);
    $(targetBlock).find("#carousel".concat(assignTo, "-indicators")).find(".carousel-inner").html(indicatorsImg);
  }

    //function appendPlanks(planks) {
    //    var plankStr = '';
    //    planks.forEach(function (plank, index) {
    //        plankStr += "<div class=\"col-6 col-lg-3 product-hover\">\n                      <div class=\"showProductInfo d-block border-ae bg-v-rect overflow-hidden\"\n                      style=\"background-image:url('".concat(window.location.origin).concat(plank.colorimgpath).concat(plank.colorimg, "')\"\n                        id=\"product-").concat(plank.cid, "\"\n                        data-ndx=\"").concat(plank.cid, "\"\n                        data-toggle=\"collapse\"\n                        data-xlcol=\"").concat(Math.floor(index / 4) + 1, "\"\n                        data-lgcol=\"").concat(Math.floor(index / 2) + 1, "\"\n                        data-name=\"").concat(plank.name, "\"\n                        data-pn=\"").concat(plank.PN, "\"\n                        data-pn2=\"").concat(plank.PN2, "\"\n                        data-pn3=\"").concat(plank.PN3, "\"\n                        data-luster=\"").concat(plank.luster, "\"\n                        data-texture=\"").concat(plank.texture, "\"\n                        data-concept=\"").concat(plank.concept, "\"\n                        data-spec=\"").concat(plank.spec, "\"\n                        data-level1=\"").concat(plank.level1, "\"\n                        data-level2=\"").concat(plank.level2, "\"\n                        data-level3=\"").concat(plank.level3, "\"\n                        data-level4=\"").concat(plank.level4, "\"\n                        data-level5=\"").concat(plank.level5, "\"\n                        data-img=\"").concat(plank.img, "\"\n                        data-imgpath=\"").concat(plank.imgpath, "\"\n                        data-colorimg=\"").concat(plank.colorimg, "\"\n                        data-colorimgpath=\"").concat(plank.colorimgpath, "\"\n                        data-series=\"").concat(plank.series, "\"\n                        data-levelselection=\"").concat(plank.levelselection, "\">\n                        <a class=\"position-absolute svg-link cart\" href=\"#\">\n                          <span class=\"icon icon-cart\"></span>\n                        </a>\n                        <div class=\"slide-group\">\n                          <div class=\"text-right pr-2 pb-2\">\n                            <a class=\"add-fav\" href=\"#\">\n                              <span class=\"icon icon-heart-white\"></span>\n                            </a>\n                          </div>\n                          <div class=\"show-mark clearfix\">\n                            <h3 class=\"h6 font-weight-bold\">").concat(plank.name, "</h3>\n                            <span class=\"font-xs ").concat(plank.PN2 ? 'd-block' : 'float-left', " pr-2\">").concat(plank.PN, "</span>\n                            ").concat(plank.PN2 ? "<span class=\"font-xs ".concat(plank.PN3 ? 'd-block' : 'float-left', " pr-2\">").concat(plank.PN2, "</span>") : '', "\n                            ").concat(plank.PN3 ? "<span class=\"font-xs float-left pr-2\">".concat(plank.PN3, "</span>") : '', "\n                            <span class=\"font-xs float-right\">").concat(plank.nameluster, "/").concat(plank.nametexture, "</span>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"info font-xs clearfix\">\n                        <span class=\"d-block\">").concat(plank.eng, "</span>\n                        <span class=\"d-block\">").concat(plank.name, "</span>\n                        <span class=\"d-block cube cube-brown\">").concat(plank.PN, "</span>\n                        <span class=\"d-block d-md-inline-block cube cube-green\">").concat(plank.PN2, "</span>\n                        <span class=\"float-md-right\">").concat(plank.nameluster, "/").concat(plank.nametexture, "</span>\n                      </div>\n                    </div>");

    //        if ((index + 1) % 2 === 0) {
    //            var _idNum = (index - 1) / 2;

    //            plankStr += appendCollapse(_idNum);
    //        }
    //    });
    //    var remain = totalCollapse % 4;
    //    var idNum = 0;

    //    if (remain == 1) {
    //        idNum = Math.ceil(totalCollapse / 2);
    //    } else if (remain === 2 || remain === 3) {
    //        idNum = Math.floor(totalCollapse / 2);
    //    }

    //    plankStr += appendCollapse(idNum);
    //    return plankStr;
    //}

    function appendPlanks(planks) {
        var plankStr = '';
        planks.forEach(function (plank, index) {
            plankStr += "<div class=\"col-6 col-lg-3 product-hover\">\n                      <div class=\"showProductInfo d-block border-ae bg-v-rect overflow-hidden\"\n                      style=\"background-image:url('".concat(window.location.origin).concat(plank.colorimgpath).concat(plank.colorimg, "')\"\n                        id=\"product-").concat(plank.cid, "\"\n                        data-ndx=\"").concat(plank.cid, "\"\n                        data-toggle=\"collapse\"\n                        data-xlcol=\"").concat(Math.floor(index / 4) + 1, "\"\n                        data-lgcol=\"").concat(Math.floor(index / 2) + 1, "\"\n                        data-name=\"").concat(plank.name, "\"\n                        data-pn=\"").concat(plank.PN, "\"\n                        data-pn2=\"").concat(plank.PN2, "\"\n                        data-pn3=\"").concat(plank.PN3, "\"\n                        data-luster=\"").concat(plank.luster, "\"\n                        data-texture=\"").concat(plank.texture, "\"\n                        data-concept=\"").concat(plank.concept, "\"\n                        data-spec=\"").concat(plank.spec, "\"\n                        data-level1=\"").concat(plank.level1, "\"\n                        data-level2=\"").concat(plank.level2, "\"\n                        data-level3=\"").concat(plank.level3, "\"\n                        data-level4=\"").concat(plank.level4, "\"\n                        data-level5=\"").concat(plank.level5, "\"\n                        data-img=\"").concat(plank.img, "\"\n                        data-imgpath=\"").concat(plank.imgpath, "\"\n                        data-colorimg=\"").concat(plank.colorimg, "\"\n                        data-colorimgpath=\"").concat(plank.colorimgpath, "\"\n                        data-series=\"").concat(plank.series, "\"\n                        data-levelselection=\"").concat(plank.levelselection, "\">\n                        <a class=\"position-absolute svg-link cart\" href=\"#\">\n                          <span class=\"icon icon-cart\"></span>\n                        </a>\n                        <div class=\"slide-group\">\n                          <div class=\"text-right pr-2 pb-2\">\n                            <a class=\"add-fav\" href=\"#\">\n                              <span class=\"icon icon-heart-white\"></span>\n                            </a>\n                          </div>\n                          <div class=\"show-mark clearfix\">\n                            <h3 class=\"h6 font-weight-bold\">").concat(plank.name, "</h3>\n                            <span class=\"font-xs ").concat(plank.PN2 ? 'd-block' : 'float-left', " pr-2\">").concat(plank.PN, "</span>\n                            ").concat(plank.PN2 ? "<span class=\"font-xs ".concat(plank.PN3 ? 'd-block' : 'float-left', " pr-2\">").concat(plank.PN2, "</span>") : '', "\n                            ").concat(plank.PN3 ? "<span class=\"font-xs float-left pr-2\">".concat(plank.PN3, "</span>") : '', "\n                            <span class=\"font-xs float-right\">").concat(plank.nameluster, "/").concat(plank.nametexture, "</span>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"info font-xs clearfix\">\n                        <span class=\"d-block\">").concat(plank.eng, "</span>\n                        <span class=\"d-block\">").concat(plank.name, "</span>\n                        <span class=\"d-block cube cube-").concat(plank.brand == 1 ? 'green' : 'gold', "\">").concat(plank.PN, "</span>\n                        ").concat(plank.PN2 ? "<span class=\"".concat(plank.PN3 ? 'd-block' : 'float-left', " cube cube-").concat(plank.brand == 1 ? 'brown' : 'silver', "\">").concat(plank.PN2, "</span>") : '', "\n                        ").concat(plank.PN3 ? "<span class=\"float-left cube cube-black\">".concat(plank.PN3, "</span>") : '', "\n                        <span class=\"float-md-right\">").concat(plank.nameluster, "/").concat(plank.nametexture, "</span>\n                      </div>\n                    </div>");

            if ((index + 1) % 2 === 0) {
                var _idNum = (index - 1) / 2;

                plankStr += appendCollapse(_idNum);
            }
        });
        var remain = totalCollapse % 4;
        var idNum = 0;

        if (remain == 1) {
            idNum = Math.ceil(totalCollapse / 2);
        } else if (remain === 2 || remain === 3) {
            idNum = Math.floor(totalCollapse / 2);
        }

        plankStr += appendCollapse(idNum);
        return plankStr;
    }

  function appendCollapse(assignTo) {
    var collapseStr = "\n<div class=\"infoCollapse collapse col-12 mb-4 mt-9 mt-md-6 pointer\" id=\"block-".concat(assignTo, "\">\n  <div class=\"border border-db rounded p-4 p-xl-6\">\n    <div class=\"row\">\n      <div class=\"col-lg-6\">\n        <div class=\"carousel slide\" id=\"carousel").concat(assignTo, "\" data-interval=\"2000\">\n          <div class=\"carousel-inner sliderImg\">");
    collapseStr += "</div>\n                                <a class=\"carousel-control-prev\" href=\"#carousel".concat(assignTo, "\" role=\"button\" data-slide=\"prev\">\n                                  <span class=\"carousel-control-prev-icon-white\" aria-hidden=\"true\"></span>\n                                </a>\n                                <a class=\"carousel-control-next\" href=\"#carousel").concat(assignTo, "\" role=\"button\" data-slide=\"next\">\n                                  <span class=\"carousel-control-next-icon-white\" aria-hidden=\"true\"></span>\n                                </a>\n          </div>\n        <div class=\"carousel carousel-nest slide\" id=\"carousel").concat(assignTo, "-indicators\" data-interval=\"10000\">\n      <div class=\"carousel-inner indicatorsImg\">");
      collapseStr += "</div>\n                    <a class=\"carousel-control-prev\" href=\"#carousel".concat(assignTo, "-indicators\" role=\"button\" data-slide=\"prev\">\n                      <span class=\"carousel-control-prev-icon-white\" aria-hidden=\"true\"></span>\n                    </a>\n                    <a class=\"carousel-control-next\" href=\"#carousel").concat(assignTo, "-indicators\" role=\"button\" data-slide=\"next\">\n                      <span class=\"carousel-control-next-icon-white\" aria-hidden=\"true\"></span>\n                    </a>\n      </div>\n    </div>\n    <div class=\"col-lg-6 mt-4 mt-lg-0\">\n      <h4 class=\"font-weight-bold plankname\">\n        <a class=\"add-fav\" href=\"#\"><span class=\"icon icon-heart-black\"></span></a></h4><span class=\"font-sm plankpn\"></span>\n      <input class=\"tab-radio d-none\" type=\"radio\" name=\"tabs").concat(assignTo, "\" id=\"radio").concat(assignTo, "-1\" value=\".content:nth-child(1)\" checked=\"checked\"/>\n      <input class=\"tab-radio d-none\" type=\"radio\" name=\"tabs").concat(assignTo, "\" id=\"radio").concat(assignTo, "-2\" value=\".content:nth-child(2)\"/>\n      <input class=\"tab-radio d-none\" type=\"radio\" name=\"tabs").concat(assignTo, "\" id=\"radio").concat(assignTo, "-3\" value=\".content:nth-child(3)\"/>\n      <div class=\"tab-group\">\n        <label class=\"tab-0\" for=\"radio").concat(assignTo, "-1\">\u6982\u5FF5</label>\n        <label class=\"tab-1\" for=\"radio").concat(assignTo, "-2\">\u898F\u683C</label>\n        <label class=\"tab-2\" for=\"radio").concat(assignTo, "-3\">\u7B49\u7D1A</label>\n      </div>\n      <div class=\"content-group mb-2\">\n        <div class=\"content content1\">\n          <p class=\"font-xs concept\"></p>\n        </div>\n        <div class=\"content\">\n          <table class=\"table table-borderless table-sm font-xs mb-0\">\n            <tbody class=\"content2\">\n\n            </tbody>\n          </table>\n        </div>\n        <div class=\"content\">\n          <table class=\"table table-borderless table-sm font-xs mb-0\">\n            <tbody class=\"content3\">\n\n            </tbody>\n          </table>\n        </div>\n      </div><a class=\"font-xs\" href=").concat(window.location.origin, "/Series/Usage/").concat(urlval, ">\u67E5\u770B\u6848\u4F8B\u5206\u4EAB></a>\n      <ul class=\"collapse-link clearfix\">\n        <li><a class=\"shareLinks\" href=\"#\"><span class=\"icon icon-link mr-1\"></span>\u5206\u4EAB\u7522\u54C1</a></li>\n        <li><a href=\"").concat(window.location.origin, "/Support/ContactUs\"><span class=\"icon icon-calendar mr-1\"></span>\u7DDA\u4E0A\u9810\u7D04</a></li>\n        <li><a href=\"").concat(window.location.origin, "/Support/Store\"><span class=\"icon icon-location mr-1\"></span>\u5C0B\u627E\u7D93\u92B7</a></li>\n      </ul>\n      <ul class=\"d-flex social-link-icons\">\n        <li class=\"ml-2\"><a href=\"#\"><img src=\"/Content/img/icon_round_1.png\" alt=\"\"/></a></li>\n        <li class=\"ml-2\"><a href=\"#\"><img src=\"/Content/img/icon_round_2.png\" alt=\"\"/></a></li>\n        <li class=\"ml-2\"><a href=\"#\"><img src=\"/Content/img/icon_round_3.png\" alt=\"\"/></a></li>\n        <li class=\"ml-2\"><a href=\"#\"><img src=\"/Content/img/icon_round_4.png\" alt=\"\"/></a></li>\n        <li class=\"ml-2\"><a class=\"clickBoard\" href=\"").concat(window.location.host, "/").concat(window.location.pathname, "?sid=").concat(urlval, "&cid=1-").concat(assignTo, "\"><img src=\"/Content/img/icon_round_5.png\" alt=\"\"/></a></li>\n      </ul>\n    </div>\n  </div>\n</div>\n</div>");
    return collapseStr;
  }

  var filterArr = [];
  $(".filterType").on("change", function () {
    var cond = {
      texture: "",
      luster: "",
      shade: "",
      color: ""
    };
    cond.texture = +$("#texture").val() || 0;
    cond.luster = +$("#luster").val() || 0;
    cond.shade = +$("#shade").val() || 0;
    cond.color = +$("#color").val() || 0;
    var result = planks.filter(function (_ref) {
      var texture = _ref.texture;
      return cond.texture === 0 ? texture : texture === cond.texture;
    }).filter(function (_ref2) {
      var luster = _ref2.luster;
      return cond.luster === 0 ? luster : luster === cond.luster;
        }).filter(function (_ref3) {
            var shade = _ref3.shade.split(',');
            if (cond.shade === 0) {
                return shade;
            } else {
                if (shade.indexOf(''+cond.shade)>-1) {
                    return shade;
                }
            }
        
              //var shad = _ref3.shade;
              //return cond.shade === 0 ? shade : shade === cond.shade;
        }).filter(function(_ref4) {
            var color = _ref4.color.split(',');
            if (cond.color === 0) {
                return color;
            } else {
                if (color.indexOf('' + cond.color) > -1) {
                    return color;
                }
            }

              //var color = _ref4.color;
              //return cond.color === 0 ? color : color === cond.color;
    });;
    filterArr = _toConsumableArray(result);
    totalCollapse = filterArr.length;
    $(".display-planks").html(appendPlanks(filterArr));
    evtOn();
  });
  $(".sortType").on("click", function () {
    event.preventDefault();
    var cond = $(this).data("sort");
    var sortArr;

    var methods = function methods(a, b) {
      return cond === "publicdate" ? new Date(b[cond]) - new Date(a[cond]) : cond === "eng" ? b[cond] > a[cond] ? 1 : -1 : b[cond] - a[cond];
    };

    sortArr = filterArr.length ? filterArr.sort(methods) : planks.sort(methods);
    $(".display-planks").html(appendPlanks(sortArr));
    evtOn();
  });
});
//# sourceMappingURL=script.js.map
