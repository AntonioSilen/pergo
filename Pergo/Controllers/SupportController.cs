﻿using AutoMapper;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using Pergo.ViewModels.Support;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Controllers
{
    public class SupportController : BaseController
    {
        private FAQRepository faqRepository;
        private CaseRepository caseRepository;
        private DocumentationRepository documentationRepository;
        private ReservationRepository reservationRepository;
        private StoreRepository storeRepository;
        private DocumentationMailLogRepository documentationMailLogRepository;

        public SupportController() : this(null, null, null, null, null, null) { }

        public SupportController(FAQRepository repo, CaseRepository repo2, DocumentationRepository repo3, ReservationRepository repo4, StoreRepository repo5, DocumentationMailLogRepository repo6)
        {
            faqRepository = repo ?? new FAQRepository(new PergoDBEntities());
            caseRepository = repo2 ?? new CaseRepository(new PergoDBEntities());
            documentationRepository = repo3 ?? new DocumentationRepository(new PergoDBEntities());
            reservationRepository = repo4 ?? new ReservationRepository(new PergoDBEntities());
            storeRepository = repo5 ?? new StoreRepository(new PergoDBEntities());
            documentationMailLogRepository = repo6 ?? new DocumentationMailLogRepository(new PergoDBEntities());
        }

        // GET: Service
        public ActionResult Index()
        {
            SupportView model = new SupportView();
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 線上預約
        /// </summary>
        /// <returns></returns>
        public ActionResult Reservation()
        {
            return View();
        }

        /// <summary>
        /// 產品保固
        /// </summary>
        /// <returns></returns>
        public ActionResult Warranty()
        {
            return View();
        }

        /// <summary>
        /// FAQs
        /// </summary>
        /// <returns></returns>
        public ActionResult FAQ()
        {
            FAQView model = new FAQView();
            model.FAQList = Mapper.Map<List<FAQDataView>>(faqRepository.GetAll().Where(f => f.IsOnline == true).OrderBy(f => f.Sort).ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 施工流程
        /// </summary>
        /// <returns></returns>
        public ActionResult Construction()
        {
            return View();
        }

        /// <summary>
        /// 文件下載
        /// </summary>
        /// <returns></returns>
        public ActionResult Documentation()
        {
            DocumentationView model = new DocumentationView();
            model.DocumentationList = Mapper.Map<List<Pergo.Areas.Admin.ViewModels.Documentation.DocumentationView>>(documentationRepository.Query(true).OrderBy(d => d.Sort).ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 維護與清潔
        /// </summary>
        /// <returns></returns>
        public ActionResult Maintenance()
        {
            MaintenanceView model = new MaintenanceView();

            return View(model);
        }

        /// <summary>
        /// 案例分享
        /// </summary>
        /// <returns></returns>
        public ActionResult Case()
        {
            return View();
        }

        /// <summary>
        /// 搜尋門市
        /// </summary>
        /// <returns></returns>
        public ActionResult Store()
        {
            StoreView model = new StoreView();
            model.StoreList = Mapper.Map<List<StoreDataView>>(storeRepository.GetAll().Where(s => s.IsOnline == true).OrderBy(s => s.Sort).ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 支援服務共用
        /// </summary>
        /// <returns></returns>
        public ActionResult Service()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult GetFAQ()
        {
            var result = Mapper.Map<List<FAQDataView>>(faqRepository.GetAll().Where(f => f.IsOnline == true).OrderBy(f => f.Sort).ToList());

            return Json(result);
        }

        /// <summary>
        /// 聯絡表單
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult ContactUs(int type = 0)
        {
            ContactUsView model = new ContactUsView();
            model.Type = type;
            //model.ReservationTime = DateTime.Now.AddDays(1);
            //model.ReservationTimeStr = model.ReservationTime.ToString("yyyy/MM/dd HH:mm");
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);
            return View(model);
        }

        [HttpPost]
        public ActionResult ContactUs(ContactUsView model)
        {
            model.ReservationTime = Convert.ToDateTime(model.ReservationTimeStr);
            if (ModelState.IsValid)
            {
                Reservation reservation = Mapper.Map<Reservation>(model);
                reservation.CreateDate = DateTime.Now;
                model.ID = reservationRepository.Insert(reservation);

                //寄信通知管理員
                SendRemindMail(reservation, "marketing@pergo.com.tw", true);
                SendRemindMail(reservation, reservation.Email);

                ShowMessage(true, "資料已送出。");
                return Redirect("/Support/ContactResult?cid=" + model.ID + "#contact_result");
            }
            else
            {
                ShowMessage(false, "資料格式錯誤。");
                model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);
                return View(model);
            }
        }

        /// <summary>
        /// 表單結果
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ContactResult(int cid = 0)
        {
            ContactUsView model = new ContactUsView();
            var query = reservationRepository.GetById(cid);
            model = Mapper.Map<ContactUsView>(query);
            model.ReservationTimeStr = model.ReservationTime.ToString("yyyy/MM/dd HH:mm");
            if (!string.IsNullOrEmpty(model.Store)) {
                model.StoreInfo = Mapper.Map<StoreDataView>(storeRepository.GetById(Convert.ToInt32(model.Store)));
            }
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 寄信通知管理員
        /// </summary>
        /// <param name="email"></param>
        /// <param name="doclist"></param>
        /// <returns></returns>
        public string SendRemindMail(Reservation reservation, string email = "", bool isAdmin = false)
        {
            try
            {
                //email = "a842695137@gmail.com";
                string Title = $"Pergo 百力地板預約";
                string mailbody = "";
                string AdminLink = $"{Request.Url.Authority}/Admin/ReservationAdmin/Edit/{reservation.ID}";

                #region MailBody
                mailbody += @"<div>
                                        <div>" +
                                            "<h3>預約資訊 : </h3>" +
                                            $"<p><span style='font-weight: bold;'>姓名 :</span> { reservation.Name }</p>" +
                                            $"<p><span style='font-weight: bold;'>分類 : </span>{ Pergo.Utility.Cmind.EnumHelper.GetDescription((Pergo.Models.ReservationType)reservation.Type) }</p>" +
                                            $"<p><span style='font-weight: bold;'>電話 : </span>{ reservation.Phone }</p>" +
                                            $"<p><span style='font-weight: bold;'>地址 : </span>{ reservation.Address }</p>" +
                                            $"<p><span style='font-weight: bold;'>門市 : </span>{ storeRepository.GetById(Convert.ToInt32(reservation.Store)).StoreName }</p>" +
                                            $"<p><span style='font-weight: bold;'>信箱 : </span>{ reservation.Email }</p>" +
                                            $"<p><span style='font-weight: bold;'>詢問內容 : </span>{ reservation.InquiryContent }</p><br/>" +
                                            $"<p><span style='font-weight: bold;'>預約時間 : </span>{ reservation.ReservationTime.ToString("yyyy/MM/dd HH:mm") }</p>";
                                        if (isAdmin)
                                        {
                                    mailbody += "<br/>" +
                                    $"<p><a href='{AdminLink}'>後台連結</a></p>";
                                        }

                                    mailbody += @"
                                            <br/>
                                            <br/>                                            
                                            <p>Pergo 敬上</p>
                                            </div>   
                                        </div>";
                #endregion

                SendMail(email, mailbody, Title, "", "");

                return "Success";
            }
            catch (Exception ex)
            {
                return "Failed";
            }
        }

        /// <summary>
        /// 寄送文件
        /// </summary>
        /// <param name="email"></param>
        /// <param name="datalist"></param>
        /// <returns></returns>
        public string SendDocMail(string email, string doclist)
        {
            try
            {
                List<DocumentationMailLog> mailLogList = new List<DocumentationMailLog>();
                string Title = "Pergo型錄來了，快點閱讀靈感吧 !";
                string mailbody = "";

                #region MailBody
                mailbody += @"<div>
                                        <div>
                                            <h3>Pergo型錄來了，快點閱讀靈感吧</h3>
                                            <p>您好，</p>
                                            <br/>
                                            <p>感謝您下載Pergo型錄，</p>
                                            <p>我們希望他會給您帶來更多的居家靈感 !</p>
                                            <br/>
                                            <p>下方可以找到您感興趣的型錄連結按鈕。</p>
                                            <p>提供您pdf格式下載，以及在線瀏覽兩種方式。</p>
                                            <ul>
                                ";
                var docIdList = doclist.Split(',');
                List<string> docStrList = new List<string>();
                foreach (var Id in docIdList)
                {
                    if (!string.IsNullOrEmpty(Id))
                    {
                        int id = Convert.ToInt32(Id);
                        var data = documentationRepository.GetById(id);
                        mailbody += "<li>" + data.Title + "<ul>" +                                
                                        //$"<li><a style='color:#ffd800' target='_blank' href='{Request.Url.Scheme}://{Request.Url.Authority}/FileUploads/DocumentationFile/{data.FileName}' download={data.Title}>Download</a></li>" +  
                                        //$"<li><a style='color:#ffd800' target='_blank' href='{Request.Url.Scheme}://{Request.Url.Authority}/Support/DownloadFile/{id}'>Download</a></li>" +
                                        $"<li><a style='color:#ffd800' target='_blank' href='{Request.Url.Scheme}://{Request.Url.Authority}/FileUploads/DocumentationFile/{data.FileName}'>Read Online</a></li>" +
                                    "</ul></li>";

                        //附件檔案
                        //docStrList.Add(Server.MapPath($"/FileUploads/DocumentationFile/") + $"{data.FileName}");

                        //新增Log
                        mailLogList.Add(new DocumentationMailLog()
                        {
                            DocID = Convert.ToInt32(Id),
                            Email = email,
                            CreateDate = DateTime.Now
                        });
                    }                   
                }
                mailbody += @"</ul>
                                    <br/>
                                    <h3>有任何疑問嗎 ?</h3>" +
                                    $"<p>如果您對我們的產品有任何問題，請隨時與我們<a href='{Request.Url.Scheme}://{Request.Url.Authority}/Support/ContactUs'>聯繫</a><p>" +
                                    @"
                                    <br/>
                                    <br/>
                                    <p>我們祝您有個鼓舞人心的居家質感旅程 !</p>
                                    <br/>
                                    <p>Pergo 敬上</p>
                                    </div>   
                                </div>";
                #endregion

                bool res = SendMail(email, mailbody, Title, "", "", docStrList);
                foreach (var mailLog in mailLogList)
                {
                    documentationMailLogRepository.Insert(mailLog);
                }
                #region 未使用
                //if (res)
                //{
                //    //寄送型錄紀錄
                //    try
                //    {
                //        StreamReader sr = new StreamReader(Server.MapPath(@"\App_Data\sentemail.txt"));
                //        while (!sr.EndOfStream)
                //        {               // 每次讀取一行，直到檔尾
                //            string line = sr.ReadLine();            // 讀取文字到 line 變數

                //            if (line == email)
                //            {
                //                sr.Close();
                //                return "新信箱";
                //            }
                //        }
                //        sr.Close();                     // 關閉串流       

                //        using (StreamWriter outputFile = new StreamWriter(Path.Combine(Server.MapPath(@"\App_Data\"), "subscribeemail.txt"), true))
                //        {
                //            outputFile.WriteLine(email);
                //            outputFile.Close();
                //        }
                //        return "信箱已記錄";
                //    }
                //    catch (Exception ex)
                //    {
                //        return $"Message :  {ex.Message}";
                //    }
                //}
                #endregion

                return res ? "Success" : "Failed";
            }
            catch (Exception ex)
            {
                return "Failed :" + ex.Message + " , " + ex.InnerException.Message;
            }
        }

        #region 寄信
        public bool SendMail(string emailStr, string mailbody, string mailtitle, string imgPath, string imgName, List<string> attachment = null)
        {
            try
            {
                //Email
                List<string> mailList = new List<string>();
                mailList.Add(emailStr);

                bool result = MailHelper.SendEmail(mailtitle, mailbody, mailList, imgPath, imgName, attachment);

                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter sw = new StreamWriter(Server.MapPath("/App_Data/maillog.txt")))   //小寫TXT     
                {
                    // Add some text to the file.                  
                    sw.WriteLine("-------------------");
                    // Arbitrary objects can also be written to the file.
                    sw.Write("Date : ");
                    sw.WriteLine(DateTime.Now);
                    sw.WriteLine(ex.Message);
                }
                return false;
            }
        }
        #endregion
    }
}