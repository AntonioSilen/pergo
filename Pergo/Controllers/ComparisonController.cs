﻿using AutoMapper;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.ViewModels.Comparison;
using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Controllers
{
    public class ComparisonController : BaseController
    {
        private SeriesRepository seriesRepository;
        private LevelRepository levelRepository;
        private ProductRepository productRepository;

        public ComparisonController() : this(null, null, null) { }

        public ComparisonController(SeriesRepository repo, LevelRepository repo2, ProductRepository repo3)
        {
            seriesRepository = repo ?? new SeriesRepository(new PergoDBEntities());
            levelRepository = repo2 ?? new LevelRepository(new PergoDBEntities());
            productRepository = repo3 ?? new ProductRepository(new PergoDBEntities());
        }

        // GET: Comparison
        public ActionResult Index(int ch = 0, string value = "")
        {
            IndexView model = new IndexView();
            var seriesList = seriesRepository.Query(true, ch, "");
            if (ch != 0)
            {               
                model.CH = ch;
                model.CHStr = Pergo.Utility.Cmind.EnumHelper.GetDescription((Pergo.Models.BrandValue)ch);
            }
            model.BrandSeriesList = Mapper.Map<List<SeriesView>>(seriesList);
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        public ActionResult ComparisonTable(int sa = 0, int sb = 0, int ch = 0, string value = "")
        {
            if (sa == 0 || sb == 0)
            {
                return RedirectToAction("Index", new { ch = ch });
            }
            ComparisonTableView model = new ComparisonTableView();
            model.CH = ch;
            try
            {            
                model.FirstSeries = Mapper.Map<SeriesView>(seriesRepository.GetById(sa));
                int la = model.FirstSeries.Brand == 1 ? 1 : 5; 
                var levelA = levelRepository.Query(true).Where(l => l.Level == la && l.SeriesID == sa).FirstOrDefault();
                model.FirstSeriesSpec = Mapper.Map<LevelView>(levelA);
                model.FirstSeriesSpec.ColorList = Mapper.Map<List<ColorView>>(productRepository.GetAll().Where(c => c.IsOnline == true && c.SeriesID == sa).ToList());

                model.SecondSeries = Mapper.Map<SeriesView>(seriesRepository.GetById(sb));
                int lb = model.SecondSeries.Brand == 1 ? 1 : 5; 
                var levelB = levelRepository.Query(true).Where(l => l.Level == lb && l.SeriesID == sb).FirstOrDefault();
                model.SecondSeriesSpec = Mapper.Map<LevelView>(levelB);
                model.SecondSeriesSpec.ColorList = Mapper.Map<List<ColorView>>(productRepository.GetAll().Where(c => c.IsOnline == true && c.SeriesID == sb).ToList());

                model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

                return View(model);
            }
            catch (Exception ex)
            {
                return View(model);
            }
            
        }
    }
}