﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.ViewModels.Characteristic;
using Pergo.ViewModels.Home;
using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Controllers
{
    [ErrorHandleActionFilter]
    public class CharacteristicController : BaseController
    {
        private FixedBannerRepository fixedBannerRepository;
        private SeriesRepository seriesRepository;

        public CharacteristicController() : this(null, null) { }

        public CharacteristicController(FixedBannerRepository repo, SeriesRepository repo2)
        {
            fixedBannerRepository = repo ?? new FixedBannerRepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
        }

        // GET: Characteristic
        public ActionResult Index(int id = 0, string value = "")//系列分類頁/大分類頁
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                id = value.ToLower() == "vinyl" ? 2 : 1;
            }
            CharacteristicView model = new CharacteristicView();
            model.id = model.brand = id;

            var bannerList = fixedBannerRepository.GetAll().Where(b => b.IsOnline == true ).OrderBy(b => b.Sort);
            //var midList = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 4).ToList());
            //var customList = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 5).ToList());

            switch (id)
            {
                case 1: //Laminate
                    model.BannerListTop = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 4).ToList());
                    model.BannerListMid = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 5).ToList());
                    model.BannerListCustom = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 6).ToList());
                    break;
                case 2: //Vinyl
                    model.BannerListTop = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 7).ToList());
                    model.BannerListMid = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 8).ToList());
                    model.BannerListCustom = Mapper.Map<List<FixedBannerView>>(bannerList.Where(b => b.Type == 9).ToList());
                    break;
                default:
                    break;
            }

            var branserieslist = seriesRepository.GetAll().Where(s => s.IsOnline == true && s.Brand == id).OrderBy(s => s.Sort).ToList();
            model.BrandSeriesList = Mapper.Map<List<SeriesView>>(branserieslist);
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
    }
}