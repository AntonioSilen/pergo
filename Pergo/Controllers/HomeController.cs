﻿using AutoMapper;
using Pergo.ActionFilters;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Controllers
{
    [ErrorHandleActionFilter]
    public class HomeController : BaseController
    {
        private CarouselBannerRepository carouselBannerRepository;
        private FixedBannerRepository fixedBannerRepository;
        private AccessoriesRepository accessoriesRepository;
        private AccessoriesUsageRepository accessoriesUsageRepository;

        public HomeController() : this(null, null, null, null) { }

        public HomeController(CarouselBannerRepository repo, FixedBannerRepository repo2, AccessoriesRepository repo3, AccessoriesUsageRepository repo4)
        {
            carouselBannerRepository = repo ?? new CarouselBannerRepository(new PergoDBEntities());
            fixedBannerRepository = repo2 ?? new FixedBannerRepository(new PergoDBEntities());
            accessoriesRepository = repo3 ?? new AccessoriesRepository(new PergoDBEntities());
            accessoriesUsageRepository = repo4 ?? new AccessoriesUsageRepository(new PergoDBEntities());
        }

        public ActionResult Index()
        {
            HomeView model = new HomeView();
            var bannerListC = carouselBannerRepository.GetAll().Where(b => b.IsOnline == true).OrderBy(b => b.Sort);
            model.CarouselBannerList = Mapper.Map<List<CarouselBannerView>>(bannerListC);

            //var bannerListF = fixedBannerRepository.GetAll().Where(b => b.IsOnline == true);
            model.FixedBannerListTop = Mapper.Map<List<FixedBannerView>>(fixedBannerRepository.GetAll().Where(b => b.IsOnline == true && b.Type == 1).ToList());
            model.FixedBannerListMid = Mapper.Map<List<FixedBannerView>>(fixedBannerRepository.GetAll().Where(b => b.IsOnline == true && b.Type == 2).ToList());
            model.FixedBannerListButt = Mapper.Map<List<FixedBannerView>>(fixedBannerRepository.GetAll().Where(b => b.IsOnline == true && b.Type == 3).ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        public ActionResult Accessories(int ch = 1, string value = "")
        {          
            AccessoriesView model = new AccessoriesView();
            using (StreamReader sr = new StreamReader(Server.MapPath("/Models/AccessoryTitle.txt")))
            {
                String line;
                int l = 1;
                while ((line = sr.ReadLine()) != null)
                {
                    if (l == 1)
                    {
                        model.LaminateTitle = line;
                    }
                    else if (l == 2)
                    {
                        model.LaminateDesc = line;
                    }
                    else if (l == 3)
                    {
                        model.VinylTitle = line;
                    }
                    else if (l == 4)
                    {
                        model.VinylDesc = line;
                        l = 0;
                    }
                    l++;
                }
            }
            model.ch = ch;
            var query = accessoriesRepository.Query(true, ch, "");            
            model.AccessoriesItemList = Mapper.Map<List<AccessoriesItem>>(query);
            foreach (var item in model.AccessoriesItemList)
            {
                var itemquery = accessoriesUsageRepository.Query(true, item.ID, "");
                item.AccessoriesUsageList = Mapper.Map<List<AccessoriesItemUsage>>(itemquery);
            }

            var usageList = accessoriesRepository.GetJoin();
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }        

        public ActionResult About()
        {
            AboutView model = new AboutView();
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        public ActionResult Search(SearchView model)
        {
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 搜尋頁結果
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSearchResult(string searchvalue = "", string tagvalue = "")
        {
            List<SearchDataView> dataList = new List<SearchDataView>();
            if (!string.IsNullOrEmpty(searchvalue))
            {

            }
           
            else
            {
                ShowMessage(false, "請輸入欲查詢花色、系列");
            }

            return Json(dataList);
        }

        public string SubscribeNews(string email)
        {
            try
            {              
                StreamReader sr = new StreamReader(Server.MapPath(@"\App_Data\subscribeemail.txt"));
                while (!sr.EndOfStream)
                {               // 每次讀取一行，直到檔尾
                    string line = sr.ReadLine();            // 讀取文字到 line 變數

                    if (line == email)
                    {
                        sr.Close();
                        return "該信箱已訂閱。";
                    }                   
                }
                sr.Close();                     // 關閉串流       

                using (StreamWriter outputFile = new StreamWriter(Path.Combine(Server.MapPath(@"\App_Data\"), "subscribeemail.txt"), true))
                {
                    outputFile.WriteLine(email);
                    outputFile.Close();
                }
                return "訂閱成功。";
            }
            catch (Exception ex)
            {
                return $"訂閱失敗，請再試一次。 Message :  {ex.Message}";             
            }
        }       
    }
}