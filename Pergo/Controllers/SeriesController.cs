﻿using AutoMapper;
using Pergo.Areas.Admin.ViewModels.CaseSite;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.Utility.Cmind;
using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Controllers
{
    public class SeriesController : BaseController
    {
        private SeriesBannerRepository seriesBannerRepository;
        private SeriesRepository seriesRepository;
        private LevelRepository levelRepository;
        private ProductRepository productRepository;
        private ProductImageRepository productImageRepository;
        private UsageRepository usageRepository;
        private CaseSiteRepository caseSiteRepository;

        public SeriesController() : this(null, null, null, null, null, null, null) { }

        public SeriesController(SeriesBannerRepository repo, SeriesRepository repo2, LevelRepository repo3,
            ProductRepository repo4, ProductImageRepository repo5, UsageRepository repo6, CaseSiteRepository repo7)
        {
            seriesBannerRepository = repo ?? new SeriesBannerRepository(new PergoDBEntities());
            seriesRepository = repo2 ?? new SeriesRepository(new PergoDBEntities());
            levelRepository = repo3 ?? new LevelRepository(new PergoDBEntities());
            productRepository = repo4 ?? new ProductRepository(new PergoDBEntities());
            productImageRepository = repo5 ?? new ProductImageRepository(new PergoDBEntities());
            usageRepository = repo6 ?? new UsageRepository(new PergoDBEntities());
            caseSiteRepository = repo7 ?? new CaseSiteRepository(new PergoDBEntities());
        }

        /// <summary>
        /// 概覽
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Overview(int id = 0, string value = "")
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }

                int number = 0;
                bool canConvert = int.TryParse(value, out number);
                if (canConvert == true)
                {
                    id = Convert.ToInt32(value);
                }
                else
                {
                    var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                    id = series.ID;
                }               
            }
            var seriesData = seriesRepository.GetById(id);
            OverviewView model = new OverviewView();
            model.brand = seriesData.Brand;
            model.seriesId = id;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;
            var bannerList = seriesBannerRepository.GetAll().Where(s => s.IsOnline == true && s.SeriesID == id && s.Page == 1).OrderBy(s => s.Sort);
            model.SeriesBannerList = Mapper.Map<List<SeriesBannerView>>(bannerList.ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
        /// <summary>
        /// 功能特色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Feature(int id = 0, string value = "")
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                int number = 0;
                bool canConvert = int.TryParse(value, out number);
                if (canConvert == true)
                {
                    id = Convert.ToInt32(value);
                }
                else
                {
                    var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                    id = series.ID;
                }               
            }
            var seriesData = seriesRepository.GetById(id);
            OverviewView model = new OverviewView();
            model.brand = seriesData.Brand;
            model.seriesId = id;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;
            var bannerList = seriesBannerRepository.GetAll().Where(s => s.IsOnline == true && s.SeriesID == id && s.Page == 2).OrderBy(s => s.Sort);
            model.SeriesBannerList = Mapper.Map<List<SeriesBannerView>>(bannerList.ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
        /// <summary>
        /// 專利技術
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Tech(int id = 0, string value = "")
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                int number = 0;
                bool canConvert = int.TryParse(value, out number);
                if (canConvert == true)
                {
                    id = Convert.ToInt32(value);
                }
                else
                {
                    var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                    id = series.ID;
                }
            }
            var seriesData = seriesRepository.GetById(id);
            OverviewView model = new OverviewView();
            model.brand = seriesData.Brand;
            model.seriesId = id;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;
            var bannerList = seriesBannerRepository.GetAll().Where(s => s.IsOnline == true && s.SeriesID == id && s.Page == 3).OrderBy(s => s.Sort);
            model.SeriesBannerList = Mapper.Map<List<SeriesBannerView>>(bannerList.ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
        /// <summary>
        /// 產品規格
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Spec(int id = 0, string value = "")
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                int number = 0;
                bool canConvert = int.TryParse(value, out number);
                if (canConvert == true)
                {
                    id = Convert.ToInt32(value);
                }
                else
                {
                    var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                    id = series.ID;
                }
            }
            var seriesData = seriesRepository.GetById(id);
            SpecView model = new SpecView();
            model.brand = seriesData.Brand;
            model.seriesId = id;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;
            var banner = seriesBannerRepository.Query(true, id, 0).OrderByDescending(s => s.UpdateDate).FirstOrDefault();
            model.MainPic = banner == null ? "" : banner.MainPic;
            var levelList = levelRepository.GetAll().Where(l => l.IsOnline == true && l.SeriesID == id);
            if (model.brand == 1)
            {
                var Ll = levelList.Where(l => l.Level == 1).FirstOrDefault();
                var Ol = levelList.Where(l => l.Level == 2).FirstOrDefault();
                var Pl = levelList.Where(l => l.Level == 3).FirstOrDefault();
                model.LivingLevel = Ll != null ? Mapper.Map<LevelView>(Ll) : new LevelView();
                model.OriginalLevel = Ol != null ? Mapper.Map<LevelView>(Ol) : new LevelView();
                model.PublicLevel = Pl != null ? Mapper.Map<LevelView>(Pl) : new LevelView();

                var llc = model.LivingLevel.Certification;
                model.LivingLevel.Certification = string.IsNullOrEmpty(llc) ? "" : HandleCKEditorString(llc);
                var olc = model.OriginalLevel.Certification;
                model.OriginalLevel.Certification = string.IsNullOrEmpty(olc) ? "" : HandleCKEditorString(olc);
                var plc = model.PublicLevel.Certification;
                model.PublicLevel.Certification = string.IsNullOrEmpty(plc) ? "" : HandleCKEditorString(plc);

                var llf = model.LivingLevel.Features;
                model.LivingLevel.Features = string.IsNullOrEmpty(llf) ? "" : HandleCKEditorString(llf);
                var olf = model.OriginalLevel.Features;
                model.OriginalLevel.Features = string.IsNullOrEmpty(olf) ? "" : HandleCKEditorString(olf);
                var plf = model.PublicLevel.Features;
                model.PublicLevel.Features = string.IsNullOrEmpty(plf) ? "" : HandleCKEditorString(plf);

                model.LivingLevel.ColorList = model.OriginalLevel.ColorList = model.PublicLevel.ColorList =
               Mapper.Map<List<ColorView>>(productRepository.GetAll().Where(c => c.SeriesID == id).ToList());
            }
            else if (model.brand == 2)
            {
                var Optl = levelList.Where(l => l.Level == 4).FirstOrDefault();
                var Prel = levelList.Where(l => l.Level == 5).FirstOrDefault();
                model.OptimumLevel = Optl != null ? Mapper.Map<LevelView>(Optl) : new LevelView();
                model.PremiumLevel = Prel != null ? Mapper.Map<LevelView>(Prel) : new LevelView();

                var olc = model.OptimumLevel.Certification;
                model.OptimumLevel.Certification = string.IsNullOrEmpty(olc) ? "" : HandleCKEditorString(olc);
                var plc = model.PremiumLevel.Certification;
                model.PremiumLevel.Certification = string.IsNullOrEmpty(plc) ? "" : HandleCKEditorString(plc);

                var olf = model.OptimumLevel.Features;
                model.OptimumLevel.Features = string.IsNullOrEmpty(olf) ? "" : HandleCKEditorString(olf);
                var plf = model.PremiumLevel.Features;
                model.PremiumLevel.Features = string.IsNullOrEmpty(plf) ? "" : HandleCKEditorString(plf);

                model.OptimumLevel.ColorList = model.PremiumLevel.ColorList =
              Mapper.Map<List<ColorView>>(productRepository.GetAll().Where(c => c.SeriesID == id).ToList());
            }
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
        /// <summary>
        /// 應用
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Usage(int id = 0, string value = "")
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                int number = 0;
                bool canConvert = int.TryParse(value, out number);
                if (canConvert == true)
                {
                    id = Convert.ToInt32(value);
                }
                else
                {
                    var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                    id = series.ID;
                }
            }
            var seriesData = seriesRepository.GetById(id);
            UsageView model = new UsageView();
            model.brand = seriesData.Brand;
            model.seriesId = id;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;

            var result = new List<CaseSiteView>();
            result.Add(new CaseSiteView()
            {
                ID = 0,
                Name = "全部",
            });
            if (!string.IsNullOrEmpty(seriesData.UsageSite))
            {
                var casesiteList = seriesData.UsageSite.Split(',');
                foreach (var item in casesiteList)
                {
                    if (true)
                    {
                        result.Add(Mapper.Map<CaseSiteView>(caseSiteRepository.GetAll().Where(c => c.Name == item).FirstOrDefault()));
                    }
                }
            }
            model.SiteList = result;
            model.UsageList = Mapper.Map<List<Pergo.Areas.Admin.ViewModels.Usage.UsageView>>(usageRepository.Query(true).Where(u => u.SeriesID == id));
            foreach (var item in model.UsageList)
            {
                var sitetype = item.SiteType.Split(',');
                foreach (var type in sitetype)
                {
                    item.SiteTypeInt += caseSiteRepository.GetAll().Where(c => c.Name == type).FirstOrDefault().ID + ",";
                }
            }
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
        /// <summary>
        /// 花色展示
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Color(int sid = 0, string value = "")
        {
            if (sid == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                sid = series.ID;
            }
            var seriesData = seriesRepository.GetById(sid);
            ColorListView model = new ColorListView();
            model.brand = seriesData.Brand;
            model.seriesId = sid;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;
            model.BannerList = Mapper.Map<List<SeriesBannerView>>(seriesBannerRepository.GetAll().Where(s => s.IsOnline == true && s.SeriesID == sid && s.Page == 4).OrderBy(s => s.Sort).ToList());
            //model.ColorList = Mapper.Map<List<ColorView>>(productRepository.Query(true).Where(c => c.SeriesID == sid).ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }
        /// <summary>
        /// 詢價預約
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ENQ(int id = 0, string value = "")
        {
            if (id == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return RedirectToAction("Index", "Home");
                }
                int number = 0;
                bool canConvert = int.TryParse(value, out number);
                if (canConvert == true)
                {
                    id = Convert.ToInt32(value);
                }
                else
                {
                    var series = seriesRepository.GetAll().Where(s => s.SeriesEngName.Replace(" ", "") == value).FirstOrDefault();
                    id = series.ID;
                }
            }
            var seriesData = seriesRepository.GetById(id);
            ENQView model = new ENQView();
            model.brand = seriesData.Brand;
            model.seriesId = id;
            model.SeriesStr = seriesData.SeriesName;
            model.SeriesEngStr = seriesData.SeriesEngName;
            model.LevelSpec = Mapper.Map<LevelView>(levelRepository.GetAll().Where(l => l.SeriesID == id && l.Level == 1).FirstOrDefault());
            model.ColorList = Mapper.Map<List<ColorView>>(productRepository.GetAll().Where(c => c.SeriesID == id && c.IsOnline == true).ToList());
            model.SEOInfo = Mapper.Map<Pergo.Areas.Admin.ViewModels.SEO.SEOView>(GetSEO);

            return View(model);
        }

        /// <summary>
        /// 共用底部其他功能
        /// </summary>
        /// <returns></returns>
        public ActionResult Other()
        {
            return PartialView();
        }

        /// <summary>
        /// 共用系列選單
        /// </summary>
        /// <returns></returns>
        public ActionResult SeriesMenu()
        {
            //if (string.IsNullOrEmpty(series))
            //{
            //    return RedirectToAction("Index", "Home");
            //}
            //ViewBag.SeriesEngStr = series;
            return PartialView();
        }

        /// <summary>
        /// 取得花色資料
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public JsonResult GetColorList(int sid)
        {
            List<ColorResult> resultList = new List<ColorResult>();
            var query = productRepository.Query(true).Where(c => c.SeriesID == sid).ToList();

            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    //點閱數
                    int views = item.Views == null ? 0 : (int)item.Views;
                    //系列資料
                    ProductSeries seriesdata = seriesRepository.GetById(item.SeriesID);
                    if (seriesdata == null)
                    {
                        continue;
                    }
                    //應用圖片
                    string colorimg = "";
                    var imglist = productImageRepository.Query(true, item.ID).ToList();
                    for (int i = 0; i < imglist.Count(); i++)
                    {
                        colorimg += imglist[i].CarousePic;
                        if (imglist.Count() > i + 1)
                        {
                            colorimg += ",";
                        }
                    }
                    //系列規格
                    string chtype = seriesdata.Brand == 1 ? "LAMINATE" : "VINYL";
                    int level = seriesdata.Brand == 1 ? 1 : 4;

                    //等級選項
                    string levelselection = "";
                    if (seriesdata.Brand == 1)
                    {

                        levelselection += GetLevelSelection(sid, 1, "living Expression");
                        levelselection += GetLevelSelection(sid, 2, "original Excellence");
                        levelselection += GetLevelSelection(sid, 3, "public Extreme");

                        if (string.IsNullOrEmpty(levelselection))
                        {
                            levelselection += "living Expression";
                        }
                    }
                    else if (seriesdata.Brand == 2)
                    {

                        levelselection += GetLevelSelection(sid, 4, "卓越版 Optimum ");
                        levelselection += GetLevelSelection(sid, 5, "優適版 Premium");

                        if (string.IsNullOrEmpty(levelselection))
                        {
                            levelselection += " Premium";
                        }
                    }

                    resultList.Add(new ColorResult()
                    {
                        brand = seriesdata.Brand,
                        PN = item.Model,
                        PN2 = item.ModelTwo,
                        PN3 = item.ModelThree,
                        color = string.IsNullOrEmpty(item.Color) ? "0" : item.Color,
                        shade = string.IsNullOrEmpty(item.Shade) ? "0" : item.Shade,
                        eng = item.EngName + ", PLANK",
                        luster = item.Luster,
                        nameluster = Pergo.Utility.Cmind.EnumHelper.GetDescription((Pergo.Models.Luster)item.Luster).Split(' ')[0],
                        name = item.Name,
                        popular = views,
                        publicdate = item.UpdateDate.ToString("yyyy-MM-dd"),
                        texture = item.Texture,
                        nametexture = Pergo.Utility.Cmind.EnumHelper.GetDescription((Pergo.Models.ColorTextureWeb)item.Texture),
                        concept = item.Concept,
                        spec = $"類別  {chtype},{GetLevelSpec(item.SeriesID, level)}",
                        level1 = $"{GetLevelSpec(item.SeriesID, 1)}",
                        level2 = $"{GetLevelSpec(item.SeriesID, 2)}",
                        level3 = $"{GetLevelSpec(item.SeriesID, 3)}",
                        level4 = $"{GetLevelSpec(item.SeriesID, 4)}",
                        level5 = $"{GetLevelSpec(item.SeriesID, 5)}",
                        img = colorimg,
                        imgpath = "/FileUploads/ProductImagePhoto/",
                        colorimg = item.MainPic,
                        colorimgpath = "/FileUploads/ProductPhoto/",
                        series = $"{seriesdata.SeriesTitle}-{seriesdata.SeriesName}",
                        levelselection = levelselection,
                        cid = item.ID,
                    });
                }
            }

            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得等級規格
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="ltype"></param>
        /// <returns></returns>
        public string GetLevelSpec(int sid, int ltype)
        {
            try
            {
                string specStr = "";
                var spec = levelRepository.Query(true).Where(l => l.SeriesID == sid && l.Level == ltype).FirstOrDefault();
                if (!string.IsNullOrEmpty(spec.SpecDisplay))
                {
                    foreach (var s in spec.SpecDisplay.Split(','))
                    {
                        if (!string.IsNullOrEmpty(s))
                        {
                            Type t = spec.GetType();
                            var props = t.GetProperties();
                            for (int i = 3; i <= 16; i++)
                            {
                                if (i != 14)
                                {
                                    var p = props[i];
                                    var desint = (Field)Enum.Parse(typeof(Field), p.Name);
                                    var des = EnumHelper.GetDescription(desint);
                                    if (des == s)
                                    {
                                        var val = p.GetValue(spec);
                                        var returnval = "";
                                        if (i == 7 || i == 10 || i == 11)
                                        {
                                            returnval = val.ToString().Replace(",", "<br>");
                                        }
                                        else
                                        {
                                            returnval = val.ToString().Replace(",", ".");
                                        }
                                        specStr += $"{s} : {returnval},";
                                    }
                                }
                            }
                        }
                    }
                }
                return specStr;
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// 取得等級選單值
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="ltype"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public string GetLevelSelection(int sid, int ltype, string val)
        {
            try
            {
                var levelslt = levelRepository.Query(true).Where(l => l.SeriesID == sid && l.Level == ltype).FirstOrDefault() == null ? "" : val;
                return $"{val},";
            }
            catch (Exception)
            {
                return "";            
            }
        }

        /// <summary>
        /// 更新瀏覽次
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        public string UpdateViews(int cid)
        {
            try
            {
                int? views = productRepository.UpdateViews(cid);

                return $"Success {cid} Views : {views}";
            }
            catch (Exception ex)
            {
                return $"Error Message : {ex.Message}";
            }
        }

        public string HandleCKEditorString(string text)
        {
            //string text = "<p><img alt=\"\" src=\"/FileUploads/LevelPhoto/20190917103156116.png\" />法國VOC釋放標準</p><p><img alt=\"\" src=\"/FileUploads/LevelPhoto/20190917124912932.png\" />北歐天鵝環保標誌</p><p><img alt=\"\" src=\"/FileUploads/LevelPhoto/20190917124501096.png\" />芬蘭VOC釋放標準</p><p><img alt=\"\" src=\"/FileUploads/LevelPhoto/20190917124518984.png\" />歐盟生態標籤認證</p><p><img alt=\"\" src=\"/FileUploads/LevelPhoto/20190917124531677.png\" />森林認證體系認可計畫</p><p><img alt=\"\" src=\"/FileUploads/LevelPhoto/20190917124547881.png\" />&nbsp;歐盟生態環保標章</p>";
            string text_return = "";
            string[] list = Regex.Split(text, "<img");
            int i = 0;
            foreach (var item in list)
            {
                string val = item;
                if (i > 0)
                {
                    if (!(val.IndexOf("<span") > 0))
                    {
                        int start = val.IndexOf("/>");
                        var tempstr = val.Substring(start + 2, (val.IndexOf("</p>") - start)-2);
                        string tempstr_return = "<span style=\"font-size:8px;\">" + tempstr + "</span>";
                        val = val.Replace(tempstr, tempstr_return);
                    }
                }
                text_return += val;
                i++;
                if (i < list.Count())
                {
                    text_return += "<img";
                }
            }

            return text_return;
        }
    }
}