﻿using AutoMapper;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.Controllers
{
    public class BaseController : Controller
    {
        //private SeriesRepository seriesRepository;

        //public BaseController() : this(null) { }

        //public BaseController(SeriesRepository repo)
        //{
        //    seriesRepository = repo ?? new SeriesRepository(new PergoDBEntities());
        //}
        //public List<SeriesView> SeriesList
        //{
        //    get
        //    {
        //        var seriesmodelList = seriesRepository.GetAll().Where(s => s.IsOnline == true && s.Brand == 1);
        //        var seriesList = Mapper.Map<List<SeriesView>>(seriesmodelList);
        //        return seriesList;
        //    }
        //}
        // GET: Base
        //public ActionResult Index()
        //{
        //    return View();
        //}

        /// <summary>
        /// 顯示訊息
        /// 成功時為綠色訊息
        /// 失敗時為紅色訊息
        /// </summary>        
        /// <param name="success">
        /// 是否成功 成功:true 失敗:false
        /// </param>
        /// <param name="message">
        /// 要顯示的訊息，若帶入空字串則預設成[success==true]為"成功"，[success==false]為"失敗"
        /// </param>
        public void ShowMessage(bool success, string message)
        {
            string tempDataKey = success ? "Result" : "Error";

            if (string.IsNullOrEmpty(message))
                message = success ? "Success" : "Failed";

            this.TempData[tempDataKey] = message;
        }

        public string ControllerName
        {
            get
            {
                return this.ControllerContext.RouteData.Values["controller"].ToString();
            }
        }

        public string ActionName
        {
            get
            {
                return this.ControllerContext.RouteData.Values["action"].ToString();
            }
        }

        public string ParameterName
        {
            get
            {
                var res = this.ControllerContext.RouteData.Values["value"];
                return res == null ? "" : res.ToString();
            }
        }

        SEORepository seoRepository = new SEORepository(new PergoDBEntities());

        /// <summary>
        /// 判斷網址頁面
        /// </summary>
        public SEO GetSEO
        {
            get
            {
                SEO SEOInfo = new SEO();
                string thisController = ControllerName;
                string thisAction = ControllerName == "Series" ? "Overview" : ActionName;
                if (thisController == "Comparison" && thisAction == "ComparisonTable")
                {
                    thisAction = "Index";
                }
                var thisParametere = ParameterName == null ? "": ParameterName;
                var data = seoRepository.Query(thisController, thisAction, thisParametere).FirstOrDefault();
                if (data != null)
                {                  
                    SEOInfo.Title = data.Title;
                    SEOInfo.Description = data.Description;
                    SEOInfo.Keywords = data.Keywords;
                    SEOInfo.Url = data.Url;
                    SEOInfo.Image = string.IsNullOrEmpty(data.Image) ? "defaultSEO.jpg" : data.Image;
                    if (ControllerName == "Series")
                    {
                        switch (ActionName)
                        {
                            case "Overview" :
                                SEOInfo.Title = SEOInfo.Title + " | 概覽";
                                break;
                            case "Feature":
                                SEOInfo.Title = SEOInfo.Title + " | 功能特色";
                                break;
                            case "Tech":
                                SEOInfo.Title = SEOInfo.Title + " | 專利技術";
                                break;
                            case "Spec":
                                SEOInfo.Title = SEOInfo.Title + " | 產品規格";
                                break;
                            case "Usage":
                                SEOInfo.Title = SEOInfo.Title + " | 應用";
                                break;
                            case "Color":
                                SEOInfo.Title = SEOInfo.Title + " | 花色展示";
                                break;
                            case "ENQ":
                                SEOInfo.Title = SEOInfo.Title + " | 詢價預約";
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    SEOInfo.Title = "PERGO百力地板官方網站｜Floors for Real Life";
                    SEOInfo.Description = "PERGO百力地板為比利時原裝進口超耐磨地板，是強化複合地板的發明者，擁有全球獨家抗水技術，並擁有超強抗刮擦抗衝擊特性。全球家用與商業空間指定使用的地板品牌。超耐磨地板首選推薦PERGO!";
                    SEOInfo.Keywords = "超耐磨地板、強化複合地板、防水地板、寵物用地板";
                    SEOInfo.Url = "http://pergo.com.tw/";
                    SEOInfo.Image = "defaultSEO.jpg";
                }
              
                return SEOInfo;
            }
        }

        public bool IsMobile()
        {
            //var agent = Request.Headers["user-agent"];
            //var check = Request.Headers["user-agent"] != null && Request.Headers["user-agent"].ToLower().ToString().IndexOf("windows") != -1;
            bool flag = false;

            string agent = System.Web.HttpContext.Current.Request.UserAgent;
            string[] keywords = { "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser" };

            //排除 Windows 桌面系统
            if (!agent.Contains("Windows NT") || (agent.Contains("Windows NT") && agent.Contains("compatible; MSIE 9.0;")))
            {
                //排除 苹果桌面系统
                if (!agent.Contains("Windows NT") && !agent.Contains("Macintosh"))
                {
                    foreach (string item in keywords)
                    {
                        if (agent.Contains(item))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
            }

            return flag;
            //return !check;           
        }
    }
}