﻿using AutoMapper;
using Pergo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台
                cfg.CreateMap<CarouselBanner, Areas.Admin.ViewModels.CarouselBanner.BannerView>();
                cfg.CreateMap<Areas.Admin.ViewModels.CarouselBanner.BannerView, CarouselBanner>();

                cfg.CreateMap<FixedBanner, Areas.Admin.ViewModels.FixedBanner.BannerView>();
                cfg.CreateMap<Areas.Admin.ViewModels.FixedBanner.BannerView, FixedBanner>();

                cfg.CreateMap<ItemType, Areas.Admin.ViewModels.ItemType.ItemTypeView>();
                cfg.CreateMap<Areas.Admin.ViewModels.ItemType.ItemTypeView, ItemType>();

                cfg.CreateMap<ProductLevel, Areas.Admin.ViewModels.Level.LevelView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Level.LevelView, ProductLevel>();               

                cfg.CreateMap<Product, Areas.Admin.ViewModels.Product.ProductView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Product.ProductView, Product>();

                cfg.CreateMap<SeriesBanner, Areas.Admin.ViewModels.SeriesBanner.BannerView>();
                cfg.CreateMap<Areas.Admin.ViewModels.SeriesBanner.BannerView, SeriesBanner>();

                cfg.CreateMap<ProductImage, Areas.Admin.ViewModels.ProductImage.ProductImageView>();
                cfg.CreateMap<Areas.Admin.ViewModels.ProductImage.ProductImageView, ProductImage>();

                cfg.CreateMap<Reservation, Areas.Admin.ViewModels.Reservation.ReservationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Reservation.ReservationView, Reservation>();

                cfg.CreateMap<Sample, Areas.Admin.ViewModels.Sample.SampleView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Sample.SampleView, Sample>();

                cfg.CreateMap<SampleProduct, Areas.Admin.ViewModels.SampleProduct.SampleProductView>();
                cfg.CreateMap<Areas.Admin.ViewModels.SampleProduct.SampleProductView, SampleProduct>();

                cfg.CreateMap<ProductSeries, Areas.Admin.ViewModels.Series.SeriesView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Series.SeriesView, ProductSeries>();

                cfg.CreateMap<Store, Areas.Admin.ViewModels.Store.StoreView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Store.StoreView, Store>();

                cfg.CreateMap<Case, Areas.Admin.ViewModels.Case.CaseView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Case.CaseView, Case>();

                cfg.CreateMap<CaseImage, Areas.Admin.ViewModels.CaseImage.CaseImageView>();
                cfg.CreateMap<Areas.Admin.ViewModels.CaseImage.CaseImageView, ColorShade>();

                cfg.CreateMap<ColorShade, Areas.Admin.ViewModels.ColorShade.ColorShadeView>();
                cfg.CreateMap<Areas.Admin.ViewModels.ColorShade.ColorShadeView, ColorShade>();

                cfg.CreateMap<CaseSite, Areas.Admin.ViewModels.CaseSite.CaseSiteView>();
                cfg.CreateMap<Areas.Admin.ViewModels.CaseSite.CaseSiteView, CaseSite>();

                cfg.CreateMap<Style, Areas.Admin.ViewModels.Style.StyleView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Style.StyleView, Style>();

                cfg.CreateMap<SeriesUsage, Areas.Admin.ViewModels.Usage.UsageView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Usage.UsageView, SeriesUsage>();

                cfg.CreateMap<Documentation, Areas.Admin.ViewModels.Documentation.DocumentationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Documentation.DocumentationView, Documentation>();
                #endregion

                #region 前台
                cfg.CreateMap<CarouselBanner, ViewModels.Home.CarouselBannerView>();
                cfg.CreateMap<ViewModels.Home.CarouselBannerView, CarouselBanner>();

                cfg.CreateMap<FixedBanner, ViewModels.Home.FixedBannerView>();
                cfg.CreateMap<ViewModels.Home.FixedBannerView, FixedBanner>();

                cfg.CreateMap<SeriesBanner, ViewModels.Series.SeriesBannerView>();
                cfg.CreateMap<ViewModels.Series.SeriesBannerView, SeriesBanner>();

                cfg.CreateMap<ProductLevel, ViewModels.Series.LevelView>();
                cfg.CreateMap<ViewModels.Series.LevelView, ProductLevel>();

                cfg.CreateMap<Product, ViewModels.Series.ColorView>();
                cfg.CreateMap<ViewModels.Series.ColorView, Product>();

                cfg.CreateMap<FAQ, ViewModels.Support.FAQDataView>();
                cfg.CreateMap<ViewModels.Support.FAQDataView, FAQ>();

                cfg.CreateMap<Reservation, ViewModels.Support.ContactUsView>();
                cfg.CreateMap<ViewModels.Support.ContactUsView, Reservation>();

                cfg.CreateMap<Accessories, ViewModels.Home.AccessoriesItem>();
                cfg.CreateMap<ViewModels.Home.AccessoriesItem, Accessories>();

                cfg.CreateMap<AccessoriesUsage, ViewModels.Home.AccessoriesItemUsage>();
                cfg.CreateMap<ViewModels.Home.AccessoriesItemUsage, AccessoriesUsage>();

                cfg.CreateMap<SEO, Areas.Admin.ViewModels.SEO.SEOView>();
                cfg.CreateMap<Areas.Admin.ViewModels.SEO.SEOView, SEO>();
                #endregion
            });
        }
    }
}