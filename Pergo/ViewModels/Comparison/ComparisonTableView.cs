﻿using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Comparison
{
    public class ComparisonTableView : FooterView
    {
        public SeriesView FirstSeries { get; set; }
        public SeriesView SecondSeries { get; set; }
        public LevelView FirstSeriesSpec { get; set; }
        public LevelView SecondSeriesSpec { get; set; }

        public int CH { get; set; }
    }
}