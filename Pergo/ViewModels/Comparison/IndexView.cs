﻿using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Comparison
{
    public class IndexView : FooterView
    {
        public int CH { get; set; }
        public string CHStr { get; set; }
        public List<SeriesView> BrandSeriesList { get; set; }
    }
}