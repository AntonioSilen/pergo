﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Series
{
    public class SpecView : FooterView
    { 
        public string SeriesStr { get; set;}
        public string SeriesEngStr { get; set; }
        public string MainPic { get; set; }
        public LevelView LivingLevel { get; set; }
        public LevelView OriginalLevel { get; set; }
        public LevelView PublicLevel { get; set; }
        public LevelView OptimumLevel { get; set; }
        public LevelView PremiumLevel { get; set; }
    }
}