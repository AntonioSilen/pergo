﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Series
{
    public class SeriesView
    {
        [Required]
        [Display(Name = "Sort")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "系列名稱")]
        public string SeriesName { get; set; }

        [Required]
        [Display(Name = "系列英文名稱")]
        public string SeriesEngName { get; set; }

        [Display(Name = "系列標題")]
        public string SeriesTitle { get; set; }

        [Required]
        [Display(Name = "品牌")]
        public int Brand { get; set; }

        [Required]
        [Display(Name = "Icon原色")]
        public string Icon { get; set; }

        [Required]
        [Display(Name = "Icon黃色")]
        public string IconHover { get; set; }

        [Display(Name = "樣圖")]
        public string SamplePic { get; set; }

        [Display(Name = "案例圖")]
        public string CasePic { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}