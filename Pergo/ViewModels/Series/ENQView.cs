﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Series
{
    public class ENQView : FooterView
    {
        public string SeriesStr { get; set; }
        public string SeriesEngStr { get; set; }
        public LevelView LevelSpec { get; set; }
        public List<ColorView> ColorList { get; set; }
    }
}