﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Series
{
    public class OverviewView : FooterView
    {
        public string SeriesStr { get; set; }
        public string SeriesEngStr { get; set; }
        public List<SeriesBannerView> SeriesBannerList { get; set; }
        //public List<SeriesBannerView> SeriesBannerListTop { get; set; }
        //public List<SeriesBannerView> SeriesBannerListCenter { get; set; }
        //public List<SeriesBannerView> SeriesBannerListVideo { get; set; }
        //public List<SeriesBannerView> SeriesBannerListBottom { get; set; }
    }

    public class SeriesBannerView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "系列")]
        public int SeriesID { get; set; }
        public string SeriesStr { get; set; }

        [Display(Name = "主標題")]
        public string Title { get; set; }

        [Display(Name = "英文標題")]
        public string EngTitle { get; set; }

        [Display(Name = "副標題")]
        public string SubTitle { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "手機圖片")]
        public string MobileMainPic { get; set; }

        [Display(Name = "連結文字")]
        public string LinkText { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Display(Name = "頁面")]
        public int Page { get; set; }

        [Display(Name = "類別")]
        public int Type { get; set; }

        [Display(Name = "白字")]
        public bool TextChange { get; set; }

        [Display(Name = "左右留白")]
        public bool WhiteSpace { get; set; }

        [Display(Name = "文字位置")]
        public int Position { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}