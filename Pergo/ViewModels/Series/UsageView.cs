﻿using Pergo.Areas.Admin.ViewModels.CaseSite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Series
{
    public class UsageView : FooterView
    {
        public string SeriesStr { get; set; }
        public string SeriesEngStr { get; set; }

        public List<CaseSiteView> SiteList { get; set; }
        public List<Pergo.Areas.Admin.ViewModels.Usage.UsageView> UsageList { get; set; }
    }
}