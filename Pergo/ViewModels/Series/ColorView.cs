﻿using Pergo.Models;
using Pergo.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.ViewModels.Series
{
    public class ColorListView : FooterView
    {
        public string SeriesID { get; set; }
        public string SeriesStr { get; set; }
        public string SeriesEngStr { get; set; }

        public List<SeriesBannerView> BannerList { get; set; }
        public List<ColorView> ColorList { get; set; }

        public List<SelectListItem> TextureOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ColorTexture>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> LusterOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Luster>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ColorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ColorColor>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ShadeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Shade>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }

    public class ColorView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "系列")]
        public int SeriesID { get; set; }
        public string SeriesStr { get; set; }
        public string SeriesEngStr { get; set; }

        [Display(Name = "紋理")]
        public int Texture { get; set; }

        [Display(Name = "亮澤")]
        public int Luster { get; set; }

        [Display(Name = "色系")]
        public string Color { get; set; }

        [Display(Name = "風格")]
        public string Shade { get; set; }

        [Display(Name = "產品名稱")]
        public string Name { get; set; }

        [Display(Name = "產品英文名稱")]
        public string EngName { get; set; }

        [Display(Name = "型號")]
        public string Model { get; set; }
        public string ModelTwo { get; set; }
        public string ModelThree { get; set; }

        [Required]
        [Display(Name = "概念")]
        public string Concept { get; set; }

        [Required]
        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "點閱數")]
        public int Views { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        public DateTime UpdateDate { get; set; }        
    }

    public class ColorResult
    {
        public int cid { get; set; }
        public int brand { get; set; }
        public string PN { get; set; }
        public string PN2 { get; set; }
        public string PN3 { get; set; }
        public string color { get; set; }
        public string shade { get; set; }
        public string eng { get; set; }
        public int luster { get; set; }
        public string nameluster { get; set; }
        public string name { get; set; }
        public int popular { get; set; }
        public string publicdate { get; set; }
        public int texture { get; set; }
        public string nametexture { get; set; }
        public string concept { get; set; }
        public string spec { get; set; }
        public string level1 { get; set; }
        public string level2 { get; set; }
        public string level3 { get; set; }
        public string level4 { get; set; }
        public string level5 { get; set; }
        public string img { get; set; }
        public string imgpath { get; set; }
        public string colorimg { get; set; }
        public string colorimgpath { get; set; }
        public string series { get; set; }
        public string levelselection { get; set; }
    }
}