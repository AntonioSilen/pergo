﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Series
{
    public class LevelView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "系列編號")]
        public int SeriesID { get; set; }

        [Required]
        [Display(Name = "等級")]
        public int Level { get; set; }

        [Display(Name = "長度")]
        public string Length { get; set; }

        [Display(Name = "厚度")]
        public string Thickness { get; set; }

        [Display(Name = "密度")]
        public string Density { get; set; }

        [Display(Name = "倒角")]
        public string Chamfer { get; set; }

        [Display(Name = "工藝技術")]
        public string Tech { get; set; }

        [Display(Name = "包裝")]
        public string Package { get; set; }

        [Display(Name = "基材標準")]
        public string Standard { get; set; }

        [Display(Name = "表層壓紋")]
        public string Texture { get; set; }

        [Display(Name = "亮澤")]
        public string Luster { get; set; }

        [Display(Name = "耐熱性")]
        public string HeatResistance { get; set; }

        [Display(Name = "鎖扣")]
        public string Lock { get; set; }

        [Display(Name = "環保認證")]
        public string Certification { get; set; }

        [Display(Name = "耐磨等級")]
        public string WearRating { get; set; }

        [Display(Name = "使用等級")]
        public string UseLevel { get; set; }

        [Display(Name = "產品特性")]
        public string Features { get; set; }

        [Display(Name = "產品花色")]
        public string Color { get; set; }

        [Required]
        [Display(Name = "保固與服務")]
        public string Guarantee { get; set; }

        [Display(Name = "規格顯示欄位")]
        public string SpecDisplay { get; set; }

        [Display(Name = "等級顯示欄位")]
        public string LevelDisplay { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        public List<ColorView> ColorList { get; set; }
    }
}