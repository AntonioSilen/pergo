﻿using AutoMapper;
using Pergo.Areas.Admin.ViewModels.SEO;
using Pergo.Models;
using Pergo.Repositories;
using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels
{
    public class FooterView
    {
        public string SearchValue { get; set; }
        public int brand { get; set; }
        public int seriesId { get; set; }

        private SeriesRepository seriesRepository;

        public FooterView() : this(null) { }

        public FooterView(SeriesRepository repo)
        {
            seriesRepository = repo ?? new SeriesRepository(new PergoDBEntities());
        }
        public List<SeriesView> SeriesList
        {
            get
            {
                var seriesmodelList = seriesRepository.GetAll().Where(s => s.IsOnline == true && s.Brand == 1);
                var seriesList = Mapper.Map<List<SeriesView>>(seriesmodelList);
                return seriesList;
            }
        }

        public SEOView SEOInfo { get; set; }
    }
}