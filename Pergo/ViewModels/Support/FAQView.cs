﻿using Pergo.Models;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.ViewModels.Support
{
    public class FAQView : FooterView
    {
       public List<FAQDataView> FAQList { get; set; }

        FAQTypeRepository faqTypeRepository = new FAQTypeRepository(new PergoDBEntities());
        public List<SelectListItem> QuestionTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = faqTypeRepository.GetAll().Where(s => s.IsOnline == true).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Name,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
    }

    public class FAQDataView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "問題")]
        public string Question { get; set; }

        [Display(Name = "解答")]
        public string Answer { get; set; }

        [Display(Name = "地板類型")]
        public int BrandType { get; set; }

        [Display(Name = "問題類型")]
        public int QuestionType { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }       
    }
}