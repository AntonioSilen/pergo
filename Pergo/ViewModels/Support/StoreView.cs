﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Support
{
    public class StoreView : FooterView
    {
        public List<StoreDataView> StoreList { get; set; }
    }
    public class StoreDataView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "門市名稱")]
        public string StoreName { get; set; }

        [Required]
        [Display(Name = "連絡電話")]
        public string Tel { get; set; }

        [Required]
        [Display(Name = "門市地址")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "營業時間")]
        public string Opening { get; set; }

        [Required]
        [Display(Name = "門市照片")]
        public string StorePic { get; set; }

        [Display(Name = "是否上線")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}