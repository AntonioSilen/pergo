﻿using Pergo.Models;
using Pergo.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pergo.ViewModels.Support
{
    public class ContactUsView : FooterView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "分類")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Phone(ErrorMessage ="電話格式錯誤")]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "地址")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "門市")]
        public string Store { get; set; }

        public StoreDataView StoreInfo { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "信箱格式錯誤")]
        [Display(Name = "信箱")]
        public string Email { get; set; }

        [Display(Name = "詢問內容")]
        public string InquiryContent { get; set; }

        [Display(Name = "回覆狀態")]
        public bool IsReply { get; set; }

        [Required]
        [Display(Name = "預約時間")]
        public string ReservationTimeStr { get; set; }
        public DateTime ReservationTime { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        private static StoreRepository storeRepository = new StoreRepository(new PergoDBEntities());
        public List<SelectListItem> StoreOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = storeRepository.Query(true);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.StoreName,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
    }
}