﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Support
{
    public class DocumentationView : FooterView
    {
        public string Email { get; set; }
        public DateTime CreateDate {get; set;}
        public List<Pergo.Areas.Admin.ViewModels.Documentation.DocumentationView> DocumentationList { get; set; }
    }
}