﻿using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Home
{
    public class SearchView : FooterView
    {
      public SearchDataView SearchData { get; set; }
    }

    public class SearchDataView
    {
        public List<SeriesView> SeriesList { get; set; }

        public List<ColorView> ColorList { get; set; }
    }
}