﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Home
{
    public class AccessoriesView : FooterView
    {
        public int ch { get; set; }
        public List<AccessoriesItem> AccessoriesItemList { get; set; }
        [Display(Name = "Laminate標題")]
        public string LaminateTitle { get; set; }

        [Display(Name = "Laminate描述")]
        public string LaminateDesc { get; set; }

        [Display(Name = "Vinyl標題")]
        public string VinylTitle { get; set; }

        [Display(Name = "Vinyl描述")]
        public string VinylDesc { get; set; }
    }

    public class AccessoriesItem
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "概念")]
        public string Concept { get; set; }

        [Required]
        [Display(Name = "品牌材質")]
        public int Brand { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        public List<AccessoriesItemUsage> AccessoriesUsageList { get; set; }
    }
    public class AccessoriesItemUsage
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "介紹")]
        public string Intro { get; set; }

        [Required]
        [Display(Name = "配件項目")]
        public int AccessoriesID { get; set; }
        public string AccessoriesStr { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }
    }
}