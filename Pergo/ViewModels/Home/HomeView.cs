﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Home
{
    public class HomeView : FooterView
    {
        public List<CarouselBannerView> CarouselBannerList { get; set; }

        public List<FixedBannerView> FixedBannerListTop { get; set; }
        public List<FixedBannerView> FixedBannerListMid { get; set; }
        public List<FixedBannerView> FixedBannerListButt { get; set; }
    }
}