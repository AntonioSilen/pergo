﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Home
{
    public class FixedBannerView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "英文標題")]
        public string EngTitle { get; set; }

        [Display(Name = "副標題")]
        public string SubTitle { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "手機圖片")]
        public string MobileMainPic { get; set; }

        [Display(Name = "連結文字")]
        public string LinkText { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Display(Name = "類別")]
        public int Type { get; set; }

        [Display(Name = "白字")]
        public bool TextChange { get; set; }

        [Display(Name = "左右留白")]
        public bool WhiteSpace { get; set; }

        [Display(Name = "文字位置")]
        public int Position { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "上線狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }
    }
}