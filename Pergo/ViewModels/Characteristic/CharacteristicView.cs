﻿using Pergo.ViewModels.Home;
using Pergo.ViewModels.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pergo.ViewModels.Characteristic
{
    public class CharacteristicView : FooterView
    {
        public int id { get; set; }
        public List<FixedBannerView> BannerListTop { get; set; }
        public List<FixedBannerView> BannerListMid { get; set; }
        public List<FixedBannerView> BannerListCustom { get; set; }
        public List<SeriesView> BrandSeriesList { get; set; }
    }
}